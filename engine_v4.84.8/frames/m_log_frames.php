<?php
include_once(_DIR_TO_ENGINE."/admin/c_editor_obj.php") ;
include_once(_DIR_TO_ENGINE."/admin/c_fra_tree.php") ;
include_once(_DIR_TO_ENGINE."/admin/c_admin_page.php") ;
include_once(_DIR_TO_ENGINE."/admin/i_clss_func.php") ;
include_once(_DIR_TO_ENGINE.'/admin/c_menu_tabs.php') ;
include_once(_DIR_TO_ENGINE.'/admin/i_stats.php') ;

class c_log extends c_editor_obj
{
  function body($options=array()) { global $tkey ; $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/log_tree.php?tkey='.$tkey,'title'=>$options['title'])) ; }
}

class c_log_tree extends c_fra_tree
{
  function body($options=array())
  {?><body id=tree_objects><div id=tree_menu><?$this->top_menu() ;?></div><?
      $this->generate_time_tree('Журнал',$this->tkey,array('on_click_script'=>'log_viewer.php','no_show_item'=>1));
   ?></body><?
  }
}

// просмотр журнала событий
class c_log_viewer extends c_admin_page
{

 function body($options=array()) {$this->body_frame($options) ; }

 function block_main()
    { $table=_DOT($this->tkey) ;
      $clss=$table->jur_clss ;
      $usl='' ;
      $_usl=array() ;
      $recs_info=execSQL_van('select min(c_data) as min,max(c_data) as max,count(pkey) as cnt from '.$table->table_name) ;
      if (!$recs_info['cnt']) { ?><div class=alert>Журнал пуст</div><? return ; }
      if (!$this->start_time) $this->start_time=$recs_info['min'] ;
      if (!$this->end_time) $this->end_time=$recs_info['max'] ;

      $_usl[]=$this->get_time_usl($this->start_time,$this->end_time) ;
      if ($_POST['select_IP']) 					        $_usl[]=' ip like \'%'.addslashes($_POST['select_IP']).'%\'' ;
      if ($_POST['select_event']) 				        $_usl[]=' obj_name=\''.addslashes($_POST['select_event']).'\'' ;
      if (sizeof($_usl)) $usl=implode(' and ',$_usl) ;

      ?><br>
        <form name="form_log" method="post">
            От: <? generate_datepicker('start_time',date('d.m.Y H:i:s',$this->start_time)) ?>
            До: <? generate_datepicker('end_time',date('d.m.Y H:i:s',$this->end_time)) ?>
            IP: <input name="select_IP" type="text" class=text value="<?echo $_POST['select_IP']?>">
            <?if ($clss==40)
             {  $list_events=execSQL('select distinct(obj_name) from '.$table->table_name.' where '.$usl) ;
                ?>Событие: <select size="1" name="select_event"><option value=""></option><?if (sizeof($list_events)) foreach($list_events as $rec){?><option value="<?echo $rec['obj_name']?>" <?if ($rec['obj_name']==$_POST['select_event']) echo 'selected'?>><?echo $rec['obj_name']?></option> <?}?></select><?

             }?>
            <input type="submit" value="Показать"><br><br>
        </form>
      <?
	  $title="Журнал '".$table->name."' " ;
      $title.=$this->get_time_title($this->start_time,$this->end_time) ;
      if ($_POST['select_IP']) $title.=' IP "'.$_POST['select_IP'].'"' ;
      if ($_POST['select_event']) $title.=' " событие "'.$_POST['select_event'].'" ' ;
	  //set_reffer_values($list_recs,$clss) ;
      //echo 'usl='.$usl.'<br>' ;
      //echo 'clss='.$clss.'<br>' ;
      _CLSS($clss)->show_list_items($this->tkey,$usl,array('debug'=>0,'default_order'=>'pkey desc','edit_by_click'=>1,'read_only'=>1,'no_check'=>1,'no_icon_edit'=>1,'title'=>$title,'all_enabled'=>1,'no_htmlspecialchars'=>1)) ;
    }

 }

//------------------------------------------------------------------------------------------------------------------------------------------------
//
// ПРОСМОТОРЩИК ЗАГРУЖЕННЫХ СТРАНИЦ
//
//------------------------------------------------------------------------------------------------------------------------------------------------


class c_log_pages extends c_editor_obj
{
  function body($options=array())
  { global $cmd,$show ;
    if (!$show) $this->body_frame_2x(array('fra_tree'=>_PATH_TO_ADMIN.'/log_tree_pages.php?cmd='.$cmd,'title'=>$options['title'])) ;
    else        $this->body_frame_1x(array('fra_view'=>_PATH_TO_ADMIN.'/log_viewer_pages.php?cmd='.$cmd,'title'=>$options['title'])) ;
  }
}


class c_log_tree_pages extends c_fra_tree
{
  function body($options=array())
  {?><body id=tree_objects>
         <ul class="tree_root" on_click_script="log_viewer_pages.php">
            <li clss=1>Заходы на сайт</li>
            <ul>
                <li clss=1 cmd=20>Посетители</li>
                   <ul>
                    <li cmd=20>Сейчас на сайте</li>
                    <li cmd=21>Сегодня</li>
                    <li cmd=22>Все</li>
                   </ul>
                <li clss=1>Роботы</li>
                   <ul>
                    <li cmd=30>Yandex</li>
                    <li cmd=34>YandexMarket</li>
                    <li cmd=35>Yandex.Commerce.Pinger</li>
                    <li cmd=36>YandexSomething</li>
                    <li cmd=31>Google</li>
                    <li cmd=32>Rambler</li>
                    <li cmd=33>Yahoo</li>
                    <li cmd=39>Все роботы</li>
                   </ul>
                <li clss=1>Журнал страниц</li>
                    <ul>
                     <li cmd=40>Сейчас на сайте</li>
                     <li cmd=41>Сегодня</li>
                     <li cmd=42>Все</li>
                    </ul>
                <li clss=1>Журнал нагрузки</li>
                    <ul>
                     <li cmd=50>Сейчас на сайте</li>
                     <li cmd=51>Сегодня</li>
                     <li cmd=52>Все</li>
                    </ul>
                <li clss=1>Журнал мета-тегов</li>
                    <ul>
                     <li cmd=60>Сейчас на сайте</li>
                     <li cmd=61>Сегодня</li>
                     <li cmd=62>Все</li>
                    </ul>
         </ul>
         </body>
      <?
  }
}


class c_log_viewer_pages extends c_admin_page
{

 function body($options=array()) {$this->body_frame($options) ; }

 function block_main()
    { //$start_day=mktime(0,0,0,$now['mon'],$now['mday'],$now['year']); // c
      //$start_act=mktime($now['hours'],$now['minutes']-$last_minut,$now['seconds'],$now['mon'],$now['mday'],$now['year']);
      ?><form name="form_main" id="form_main" method="post" action=''>
         <input type="hidden" name="cmd" value="<?echo $this->cmd?>">
      <?

      switch ($this->cmd)
      { case 1:
        case 2:
        case 20: $this->show_vhod_pages('logon','active','Активность клиентов на сайте за последние 10 мин') ; break ;
        case 21: $this->show_vhod_pages('logon','segodna','Входы на сайт за текущие сутки') ; break ;
        case 22: $this->show_vhod_pages('logon','all','Все входы на сайт из журнала') ; break ;

        case 3:
        case 30: $this->show_vhod_pages('robot','yandex','Входы робота "yandex" на сайт за последние 24 часа') ; break ;
        case 31: $this->show_vhod_pages('robot','Google','Входы робота "Google" на сайт за последние 24 часа') ; break ;
        case 32: $this->show_vhod_pages('robot','rambler','Входы робота "rambler" на сайт за последние 24 часа') ; break ;
        case 33: $this->show_vhod_pages('robot','yahoo','Входы робота "yahoo" на сайт за последние 24 часа') ; break ;
        case 34: $this->show_vhod_pages('robot','YandexMarket','Входы робота "YandexMarket" на сайт за последние 24 часа') ; break ;
        case 35: $this->show_vhod_pages('robot','Yandex.Commerce.Pinger','Входы робота "Yandex.Commerce.Pinger" на сайт за последние 24 часа') ; break ;
        case 36: $this->show_vhod_pages('robot','YandexSomething','Входы робота "YandexSomething" на сайт за последние 24 часа') ; break ;
        case 37: $this->show_vhod_pages('robot','dynamik','Роботы, определенные по методу динамической фильтрации') ; break ;
        case 39: $this->show_vhod_pages('robot','all','Входы всех роботов на сайт за последние 24 часа') ; break ;

        case 4:
        case 40: $this->show_vhod_pages('jurnal','active','Просмотр журнала страниц - последние 10 мин') ; break ;
        case 41: $this->show_vhod_pages('jurnal','sutki','Просмотр журнала страниц - последние сутки') ; break ;
        case 42: $this->show_vhod_pages('jurnal','all','Просмотр журнала страниц - все записи') ; break ;

        case 5:
        case 50: $this->show_debug_pages('jurnal','active','Просмотр журнала нагрузки - последние 10 мин') ; break ;
        case 51: $this->show_debug_pages('jurnal','sutki','Просмотр журнала нагрузки - последние сутки') ; break ;
        case 52: $this->show_debug_pages('jurnal','all','Просмотр журнала нагрузки - все записи') ; break ;

        case 6:
        case 60: $this->show_metatags_pages('jurnal','active','Просмотр журнала мета-тегов - последние 10 мин') ; break ;
        case 61: $this->show_metatags_pages('jurnal','sutki','Просмотр журнала мета-тегов - последние сутки') ; break ;

        default: echo 'режим '.$this->cmd.' - не определен<br>' ;
      }
      ?></form><?
    }

  // time_int = active,segodna,all

  static function show_vhod_pages($mode,$time_int,$title='')
  { global $select_IP,$cmd ;
      $ip_info=array() ;
      //print_r($_POST) ;
      //echo 'select_IP='.$select_IP.'<br>' ;
      check_input_var($select_IP,'ip') ;
      //echo 'select_IP='.$select_IP.'<br>' ;
      //echo 'time_int='.$time_int.'<br>';
      //echo 'mode='.$mode.'<br>';

	  switch ($mode)
	  { case 'jurnal': $list_session_pages=get_pages_stat_info_jurnal($time_int,$select_IP) ; break ;
        case 'robot':  $list_session_pages=get_pages_stat_info('sutki','array',$time_int) ; break ;
        case 'logon':  $list_session_pages=get_pages_stat_info($time_int,'array') ; break ;
      }

      echo '<h1>'.$title.' - '.sizeof($list_session_pages).'</h1>' ;

      $arr_from=array() ;
      if (sizeof($list_session_pages)) foreach($list_session_pages as $rec_page)
      { $arr=parse_url($rec_page['url_from']) ;
        $arr_from[$arr['host']]++ ;
      }
      arsort($arr_from,SORT_NUMERIC) ;
      //damp_array($arr_from,1,-1) ;

      //damp_array($list_session_pages) ;
      ?>IP: <input type="text" class="text _send_by_enter" name="select_IP" value="<?echo $select_IP?>">&nbsp;&nbsp;&nbsp;
        Откуда: <select class="text send_by_enter" name="select_from"><option></option>
           <? if (sizeof($arr_from)) foreach($arr_from as $from=>$cnt)
                { if (!$from) $from='Прямые переходы' ;
                  ?><option value="<?echo $from?>" <?if ($_POST['select_from']==$from) echo 'selected'?>><?echo $from.' ['.$cnt.']'?></option><?}
           ?>
         </select>&nbsp;&nbsp;&nbsp;
         <input type="submit" value="Показать">
         <br><br>
      <script type="text/javascript">$j('input.send_by_enter').keydown(function(e){if(e.keyCode==13) $j('form#form_main').submit();});</script>
      <?
      //damp_array($_POST) ;
      ?><table><tr class=clss_header><td>Время</td><td>Сколько</td><td>ip</td><td>Страна</td><td>Город</td><td>Откуда</td><td>Что искали</td><td>Просмотрено<br>страниц</td><td>Код</td><td>Куда</td><td>Заголовок</td><td>Робот</td><td>Информация агента</td></tr><?
      if (sizeof($list_session_pages)) foreach($list_session_pages as $rec_page)
         if (!$_POST['select_from'] or ($_POST['select_from'] and strpos($rec_page['url_from'],$_POST['select_from'])!==false))
      { $time=$rec_page['last_data']-$rec_page['first_data'] ;
        $_time=($time)? show_time($time):'' ;
        $now=getdate() ;
        $first_data=getdate($rec_page['first_data']) ;
        $str_data=($now['mday']!=$first_data['mday'])? date('d.m.y H:i:s',$rec_page['first_data']):date('H:i:s ',$rec_page['first_data']) ;
        $reffer_link=check_yandex_link($rec_page['url_from']) ;
        //$style=($mode=='active')? 'class="select_top_border"':'' ;
        ?><tr><td><? echo $str_data ; ?></td>
              <td><? echo $_time ; ?></td>
              <td class=center><a href='<?echo _PATH_TO_ADMIN.'/log_viewer_pages.php?$cmd='.$cmd.'&select_IP='.$rec_page['ip']?>'><? echo $rec_page['ip']?></a></td>
              <? if (_GEO_IP_METHOD_) { echo '<td>'.$rec_page['country'].'</td><td><span class=blau>'.$rec_page['city'].'</span></td>' ; } ?>
              <td class=left><?if ($rec_page['url_from']){?><a target=_blank href="<? echo $reffer_link ?>"><? echo mb_substr($rec_page['url_from'],0,60); ?></a><?}?></td>
              <td class="green bold"><? echo $rec_page['search_text'] ; ?></td>
              <td><? echo $rec_page['cnt'] ; ?></td>
              <td><? if ($rec_page['use_cache']) echo $rec_page['use_cache'] ; ?></td>
              <td class=left><a target=_blank href="<? echo 'http://'.$rec_page['url'] ?>"><? echo mb_substr($rec_page['url'],0,40); ?></a></td>
              <td class=left><? echo $rec_page['obj_name'] ?></td>
              <td 			><? echo ($rec_page['is_robot'])? 'Да':''  ?></td>
              <td class=left><? echo ($rec_page['is_robot'])? robot_name($rec_page['user_agent']):defines_vers($rec_page['user_agent']) ?></td>
          </tr><?
        if ($time_int=='active')
        { $list_pages=execSQL('select * from '.TM_LOG_PAGES.' where session_id="'.$rec_page['session_id'].'" and is_robot=0 order by c_data') ;
          $last_data=$rec_page['first_data'] ;
          if (sizeof($list_pages)) foreach($list_pages as $rec) if ($rec['pkey']!=$rec_page['pkey'])
	      { $time=$rec['c_data']-$last_data ;
            $last_data=$rec['c_data'] ;
            $_time=($time)? show_time($time):'' ;
            $colspan=(_USE_GEOIP_)? 8:6 ;
	        ?><tr><td colspan=<?echo $colspan?> class="left back_white"><?/*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+<? echo $_time ; ?>*/?></td>
	              <td class="back_white"><? if ($rec_page['use_cache']) echo  $rec['use_cache']; ?></td>
	              <td class="left back_white"><a target=_blank href="<? echo 'http://'.$rec['url'] ?>"><? echo mb_substr($rec['url'],0,40); ?></a></td>
	              <td class="left back_white"><? echo $rec['obj_name'] ?></td>
	              <td class="back_white" colspan=2></td>
	          </tr><?
          }
        }
      }
      ?></table><?

  }

  static function show_debug_pages($mode,$time_int,$title='')
  {
	  $list_recs=get_pages_stat_info_jurnal($time_int) ;
      echo '<h1>'.$title.' - '.sizeof($list_recs).'</h1>' ;
      //set_reffer_values($list_recs,$clss) ;
	  //_CLSS(50)->show_based_obj_list($list_recs,array('read_only'=>1,'no_check'=>1,'title'=>$title,'all_enabled'=>1,'no_htmlspecialchars'=>1)) ;
     //return ;

      ?><table><tr class=clss_header><td>id</td><td>Время</td><td>ip</td><td>Код</td><td>Страница</td><td>Заголовок</td><td>Код объекта</td><td>Время генерации, с.</td><td>Задействовано памяти</td><td>Запросов к БД</td><td>Время на запросы к БД</td><td>Робот</td><td>Информация агента</td><td>Описание агента</td></tr><?
      if (sizeof($list_recs)) foreach($list_recs as $rec_page)
      { $now=getdate() ;
        $first_data=getdate($rec_page['first_data']) ;
        $str_data=($now['mday']!=$first_data['mday'])? date('d.m.y H:i:s',$rec_page['first_data']):date('H:i:s ',$rec_page['first_data']) ;
        $class_sql=($rec_page['sql_time']>1)? 'class=red':'' ;
        $class_gen=($rec_page['time_gen']>1)? 'class=red':'' ;
        ?><tr><td><? echo $rec_page['pkey'] ; ?></td>
              <td><? echo $str_data ; ?></td>
              <td class=left><? echo $rec_page['ip'] ; ?></td>
              <td 			><? if ($rec_page['use_cache']) echo $rec_page['use_cache'] ; ?></td>
              <td class=left><a target=_blank href="http://<? echo $rec_page['url'] ?>"><? echo mb_substr($rec_page['url'],0,40); ?></a></td>
              <td class=left><? echo $rec_page['obj_name'] ?></td>
              <td class=left><? echo $rec_page['reffer'] ?></td>
              <td <?echo $class_gen?>><? echo $rec_page['time_gen'] ?></td>
              <td 			><? echo $rec_page['mem_size'] ?></td>
              <td 			><? echo $rec_page['sql_cnt'] ?></td>
              <td <?echo $class_sql?>><? echo $rec_page['sql_time'] ?></td>
              <td 			><? echo ($rec_page['is_robot'])? 'Да':''  ?></td>
              <td class=left><? echo ($rec_page['is_robot'])? robot_name($rec_page['user_agent']):defines_vers($rec_page['user_agent']) ?></td>
              <td class=left><? echo $rec_page['user_agent'];?></td><?
        ?></tr><?
      }
      ?></table><?

  }

  function show_metatags_pages($mode,$time_int,$title='')
  {
	  $list_recs=get_pages_stat_info_jurnal($time_int) ;
      echo '<h1>'.$title.' - '.sizeof($list_recs).'</h1>' ;
      //set_reffer_values($list_recs,$clss) ;
	  //_CLSS(50)->show_based_obj_list($list_recs,array('read_only'=>1,'no_check'=>1,'title'=>$title,'all_enabled'=>1,'no_htmlspecialchars'=>1)) ;
      //return ;
      //print_2x_arr($list_recs) ;

      ?><table><tr class=clss_header><td>id</td><td>Время</td><td>ip</td><td>Код</td><td>Страница</td><td>Код объекта</td><td>Заголовок</td><td>Keywords</td><td>Description</td></tr><?
      if (sizeof($list_recs)) foreach($list_recs as $rec_page)
      { $now=getdate() ;
        $first_data=getdate($rec_page['first_data']) ;
        $str_data=($now['mday']!=$first_data['mday'])? date('d.m.y H:i:s',$rec_page['first_data']):date('H:i:s ',$rec_page['first_data']) ;

        ?><tr><td><? echo $rec_page['pkey'] ; ?></td>
              <td><? echo $str_data ; ?></td>
              <td class=left><? echo $rec_page['ip'] ; ?></td>
              <td 			><? if ($rec_page['use_cache']) echo $rec_page['use_cache'] ; ?></td>
              <td class=left><a target=_blank href="http://<? echo $rec_page['url'] ?>"><? echo mb_substr($rec_page['url'],0,40); ?></a></td>
              <td class=left><? echo $rec_page['reffer'] ?></td>
              <td class=left><? echo $rec_page['obj_name'] ?></td>
              <td class=left><? echo $rec_page['page_keywords'] ?></td>
              <td class=left><? echo $rec_page['page_description'] ?></td>
         </tr><?
      }
      ?></table><?

  }


 }

?>