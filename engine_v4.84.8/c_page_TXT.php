<?php
//
class c_page_TXT
{ // конструктор

  function c_page_TXT(&$options=array())
  {

  }

  function init(&$options=array())
  {
    $this->on_send_header_before() ;
    // обработка входных данных до определения текущего объекта
    $this->command_execute($options) ; set_time_point('command_execute - OK') ;
    $this->select_obj_info($options) ; set_time_point('select_obj_info - OK') ;
  }

  function on_send_header_before()
  {
     header("Content-type: text/html;charset=UTF-8");
  }

  // отработка команд ДО определения текущего объекта
  // вызывается для всех скриптов текущего модуля, если в скрипте не объявлена собственная функция command_execute
  function command_execute()
  { global $cur_system ;
    if (is_object($cur_system) and method_exists($cur_system,'command_execute'))  $cur_system->command_execute() ;
  }

  function select_obj_info()
  { global $cur_system ;
    if (is_object($cur_system) and method_exists($cur_system,'select_obj_info'))  $cur_system->select_obj_info() ;
  }

    function check_autorize() { return(1);}

  // стандартная функция для вызова страницы
  function show(&$options=array()) { $this->body($options) ;}
  // вывод txt-контента
  function body(&$options=array()) {}
}

?>