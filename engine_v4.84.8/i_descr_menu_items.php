<?
// скрипты, работающие через стандартные tree и viewers ---------------------------------------------------------------

// index.php
$file_items['index.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['index.php']['options']['reboot_all']='1' ;

// sync.php
//$file_items['sync.php']['include'][]='_DIR_TO_ENGINE."/admin/c_sync.php"' ;

// ajax.php
$file_items['ajax.php']['include'][]='_DIR_TO_ENGINE."/admin/c_ajax.php"' ;

// frame.php
//$file_items['frame_goods.php'][]=array() ;

// fancyupload.php
//$file_items['fancyupload.php']['include'][]='_DIR_TO_ENGINE."/frames/m_fancyupload_frames.php"' ;
//$file_items['fancyupload.php']['options']['title']='"Загрузчик изображений"' ;

// fra_top_menu.php
$file_items['fra_top_menu.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;

// fra_tree.php
$file_items['fra_tree.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;

// fra_viewer.php
$file_items['fra_viewer.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;

// fra_viewer_search.php
$file_items['fra_viewer_search.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;

// show_obj.php
$file_items['show_obj.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['show_obj.php']['options']['use_class']='"c_fra_viewer"' ;

// getimage.php
$file_items['getimage.php']['include'][]='_DIR_TO_ENGINE."/admin/i_getimage.php"' ;

// reboot.php
$file_items['reboot.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['reboot.php']['options']['reboot_all']=1 ;
$file_items['reboot.php']['options']['use_class']='"c_index"' ;

// back_file_upload.php
//$file_items['back_file_upload.php']['text'][]='upload_file_background();' ;

// editor_info.php
$file_items['editor_info.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_info.php']['options']['use_table']='TM_INFO' ;
$file_items['editor_info.php']['options']['title']='"Блоки информации"' ;
$file_items['editor_info.php']['options']['use_class']='"c_editor_obj"' ;

// editor_mail_templates.php
$file_items['editor_mail_templates.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_mail_templates.php']['options']['use_table']='TM_MAILS' ;
$file_items['editor_mail_templates.php']['options']['title']='"Шаблоны писем"' ;
$file_items['editor_mail_templates.php']['options']['use_class']='"c_editor_obj"' ;

// editor_meta_tags.php - временно отключен
//$file_items['editor_meta_tags.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
//$file_items['editor_meta_tags.php']['options']['use_table']='TM_KEYWORDS' ;
//$file_items['editor_meta_tags.php']['options']['title']='"Редактор мета-тегов"' ;
//$file_items['editor_meta_tags.php']['options']['use_class']='"c_editor_obj"' ;

//editor_personal.php
$file_items['editor_personal.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_personal.php']['options']['use_table']='TM_PERSONAL' ;
$file_items['editor_personal.php']['options']['title']='"Персонал сайта"' ;
$file_items['editor_personal.php']['options']['use_class']='"c_editor_obj"' ;

//editor_site_menu.php
$file_items['editor_site_menu.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_site_menu.php']['options']['use_table']='TM_SITE_MENU' ;
$file_items['editor_site_menu.php']['options']['title']='"Меню сайта"' ;
$file_items['editor_site_menu.php']['options']['use_class']='"c_editor_obj"' ;

//editor_site_setting.php
$file_items['editor_site_setting.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_site_setting.php']['options']['use_table_code']='"TM_setting"' ;
$file_items['editor_site_setting.php']['options']['title']='"Настройки сайта"' ;
$file_items['editor_site_setting.php']['options']['use_class']='"c_editor_obj"' ;

//editor_trash.php
$file_items['editor_trash.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_trash.php']['options']['use_table_code']='"TM_admin_trash"' ;
$file_items['editor_trash.php']['options']['title']='"Мусорная корзина"' ;
$file_items['editor_trash.php']['options']['use_class']='"c_editor_obj"' ;

// editor_banners.php
$file_items['editor_banners.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_banners.php']['options']['use_table']='TM_BANNERS' ;
$file_items['editor_banners.php']['options']['title']='"Баннеры"' ;
$file_items['editor_banners.php']['options']['use_class']='"c_editor_obj"' ;

// editor_list.php
$file_items['editor_list.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_list.php']['options']['use_table']='TM_LIST' ;
$file_items['editor_list.php']['options']['title']='"Списки"' ;
$file_items['editor_list.php']['options']['use_class']='"c_editor_obj"' ;


// скрипты, работающие через внешние tree и viewers ---------------------------------------------------------------

// give_file.php
$file_items['give_file.php']['include'][]						='_DIR_TO_ENGINE."/admin/c_give_file.php"' ;

// ch_ext.php
$file_items['sh_ext.php']['include'][]						    ='_DIR_TO_ENGINE."/admin/c_sh_ext.php"' ;

// fast_edit.php
$file_items['fastedit.php']['text_content']						=1 ;
$file_items['fastedit.php']['text'][]						    ='<? define("_ENGINE_MODE","site") ;' ;
$file_items['fastedit.php']['text'][]						    ='include ("../ini/patch.php");' ;
$file_items['fastedit.php']['text'][]						    ='include (_DIR_TO_ENGINE."/i_system.php") ;' ;
$file_items['fastedit.php']['text'][]						    ='session_start() ;' ;
$file_items['fastedit.php']['text'][]						    ='$_SESSION["__fast_edit_mode"]=$_GET["mode"] ;' ;
$file_items['fastedit.php']['text'][]						    ='session_commit() ;' ;
$file_items['fastedit.php']['text'][]						    ='$goto="http://"._MAIN_DOMAIN.$_GET["to"] ;' ;
$file_items['fastedit.php']['text'][]						    ='header("Location: ".$goto);' ;
$file_items['fastedit.php']['text'][]						    ='?>' ;

// editor_pages.php
$file_items['editor_pages.php']['include'][]                    ='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_pages.php']['options']['use_table']         ='TM_PAGES' ;
$file_items['editor_pages.php']['options']['title']             ='"Страницы сайта"' ;
$file_items['editor_pages.php']['options']['use_class']         ='"c_editor_obj"' ;

// editor_metatags.php
$file_items['editor_metatags.php']['include'][]					='_DIR_TO_ENGINE."/frames/m_editor_metatags_frames.php"' ;
$file_items['editor_metatags.php']['options']['title']          ='"Редактор метатегов"' ;
$file_items['editor_metatags_tree.php']['include'][]				    ='_DIR_TO_ENGINE."/frames/m_editor_metatags_frames.php"' ;
$file_items['editor_metatags_viewer.php']['include'][]		            ='_DIR_TO_ENGINE."/frames/m_editor_metatags_frames.php"' ;


// file_explorer.php
$file_items['file_explorer.php']['include'][]                    ='_DIR_TO_ENGINE."/frames/m_file_explorer.php"' ;
$file_items['file_explorer.php']['options']['title']             ='"Файловый проводник"' ;
$file_items['file_explorer_tree.php']['include'][]				 ='_DIR_TO_ENGINE."/frames/m_file_explorer.php"' ;
$file_items['file_explorer_viewer.php']['include'][]		     ='_DIR_TO_ENGINE."/frames/m_file_explorer.php"' ;

// image_uploader.php
$file_items['image_uploader.php']['include'][]					='_DIR_TO_ENGINE."/frames/m_image_uploader_frames.php"' ;
$file_items['image_uploader.php']['options']['title']			='"Загрузчик изображений"' ;

// file_uploader.php
$file_items['file_uploader.php']['include'][]					='_DIR_TO_ENGINE."/frames/m_file_uploader_frames.php"' ;
$file_items['file_uploader.php']['options']['title']			='"Загрузчик файлов"' ;

// ck_uploader.php
$file_items['ck_uploader.php']['include'][]					    ='_DIR_TO_ENGINE."/frames/m_ck_uploader.php"' ;

// engine_support.php
$file_items['engine_support.php']['include'][]					='_DIR_TO_ENGINE."/frames/m_engine_support_frames.php"' ;
$file_items['engine_support.php']['options']['title']			='"Техническая поддержка"' ;
$file_items['engine_support_frame.php']['include'][]		    ='_DIR_TO_ENGINE."/frames/m_engine_support_frames.php"' ;
$file_items['engine_support_token_request.php']['include'][]	='_DIR_TO_ENGINE."/frames/m_engine_support_frames.php"' ;

// sys_info.php
$file_items['sys_info.php']['include'][]						='_DIR_TO_ENGINE."/frames/m_sys_info_frames.php"' ;
$file_items['sys_info.php']['options']['title']					='"Информация по настройкам движка"' ;

// все журналы
$file_items['log_tree.php']['include'][]						='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_viewer.php']['include'][]						='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;

$file_items['log_tree_pages.php']['include'][]				 	='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_viewer_pages.php']['include'][]				='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;

// log_ban_stat.php
$file_items['log_ban_stat.php']['include'][]					='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_ban_stat.php']['options']['use_table']	        ='TM_LOG_BANNERS' ;
$file_items['log_ban_stat.php']['options']['use_class']			='"c_log"' ;
$file_items['log_ban_stat.php']['options']['title']				='"Журнал статистики банеров"' ;

// log_events.php
$file_items['log_events.php']['include'][]						='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_events.php']['options']['use_table']		    ='TM_LOG_EVENTS' ;
$file_items['log_events.php']['options']['use_class']			='"c_log"' ;
$file_items['log_events.php']['options']['title']				='"Журнал событий на сайте"' ;

// log_pages.php
$file_items['log_pages.php']['include'][]						='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_pages.php']['options']['use_table']		    ='TM_LOG_PAGES' ;
$file_items['log_pages.php']['options']['use_class']			='"c_log_pages"' ;
$file_items['log_pages.php']['options']['title']				='"Журнал страниц"' ;

// log_errors.php
$file_items['log_errors.php']['include'][]						='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_errors.php']['options']['use_table']		    ='TM_LOG_ERRORS' ;
$file_items['log_errors.php']['options']['use_class']			='"c_log"' ;
$file_items['log_errors.php']['options']['title']				='"Журнал ошибок"' ;

// log_searches.php
$file_items['log_searches.php']['include'][]					='_DIR_TO_ENGINE."/frames/m_log_frames.php"' ;
$file_items['log_searches.php']['options']['use_table']	        ='TM_LOG_SEARCH' ;
$file_items['log_searches.php']['options']['use_class']			='"c_log"' ;
$file_items['log_searches.php']['options']['title']				='"Журнал поисковых запросов"' ;
//damp_array($file_items) ;

// editor_tables.php
$file_items['editor_tables.php']['include'][]                       ='_DIR_TO_ENGINE."/admin/c_site.php"' ;
$file_items['editor_tables.php']['options']['use_table']            ='"obj_descr_obj_tables"' ;
$file_items['editor_tables.php']['options']['use_class']            ='"c_editor_obj"' ;

// sys_util.php
$file_items['sys_util.php']['include'][]                            ='_DIR_TO_ENGINE."/frames/m_admin_util_frames.php"' ;
$file_items['sys_util.php']['options']['title']                     ='"Системные утилиты"' ;
$file_items['sys_util_tree.php']['include'][]                       ='_DIR_TO_ENGINE."/frames/m_admin_util_frames.php"' ;
$file_items['sys_util_viewer.php']['include'][]                     ='_DIR_TO_ENGINE."/frames/m_admin_util_frames.php"' ;

// editor_pages.php
$file_items['php_pages_editor.php']['include'][]                    ='_DIR_TO_ENGINE."/frames/m_php_pages_editor.php"' ;
$file_items['php_pages_editor.php']['options']['title']             ='"Редактор PHP cтраниц сайта"' ;
$file_items['php_pages_editor_tree.php']['include'][]				='_DIR_TO_ENGINE."/frames/m_php_pages_editor.php"' ;
$file_items['php_pages_editor_viewer.php']['include'][]				='_DIR_TO_ENGINE."/frames/m_php_pages_editor.php"' ;
?>