<?

function convert_XML_to_TXT(&$XML_source)
{ $dom = new DomDocument();
  if ($dom->loadXML($XML_source))
{ $objs_xml=$dom->getElementsByTagName("obj");
  if (sizeof($objs_xml))
       foreach ($objs_xml as $obj_xml)
    {    $obj=array() ;
		//foreach($obj_xml->childNodes as $prop_xml) $obj[$prop_xml->nodeName]=trim(iconv('utf-8','windows-1251',$prop_xml->nodeValue)) ;
		foreach($obj_xml->childNodes as $prop_xml) $obj[$prop_xml->nodeName]=trim($prop_xml->nodeValue) ;
        $list=array(&$obj) ;
           _CLSS($obj['clss'])->show_list_items($list,array('read_only'=>1,'no_check'=>1,'all_enabled'=>1)) ;
  }
   }
}

 // проверяет и записывает во все таблицы в поле use_clss режим таблицы - main,out,ext
 function check_table_mode()
 {

 	?><table><?
 	if (sizeof($_SESSION['descr_obj_tables'])) foreach($_SESSION['descr_obj_tables'] as $table_obj)
 	{  unset($type) ;
 	   if (isset($_SESSION['descr_obj_tables'][$table_obj->parent]))
 	   { $list_fields=execSQL('describe '.$table_obj->table_name) ;
 	     if (!sizeof($list_fields['clss'])) $type='ext' ; else $type='out' ;
 	   }
 	   else
 	   { switch ($table_obj->clss)
 	     {
           case 101: $type='main' ; break;
           case 106: $type='indx' ; break;
           case 108: $type='jurn' ; break;
         }
 	   }

       switch ($type)
        { case 'main': $style='style="background:#D7EBFF;"' ; break;
		  case 'out':  $style='style="background:#FFD7EB;"' ; break;

          default:     $style='' ;
        }

       ?><tr <?echo $style?>><td><?echo $table_obj->pkey?></td><td class=left><? echo $table_obj->table_name ?></td><td><? echo $type?></td></tr><?
       if ($type) { $sql='update obj_descr_obj_tables set use_clss="'.$type.'" where pkey='.$table_obj->pkey ;
        		    if (!$result = mysql_query($sql)) SQL_error_message($sql,'Не удалось обновить запись в таблице',1) ;
        		  }
 	}
 	?></table><?

 }

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
// i_img_working
//-----------------------------------------------------------------------------------------------------------------------------------------------------------

// загружает файл из временного файла, делает превьюшку, дописывает размер файла в превьщку, сохраняет превьшку и оригинал в сесиии
function upload_image_to_session()
{
	// Get the session Id passed from SWFUpload. We have to do this to work-around the Flash Player Cookie Bug
	if (isset($_POST["PHPSESSID"])) session_id($_POST["PHPSESSID"]);

    session_start();
	//ini_set("html_errors", "0");

	// Check the upload
	if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0) {
		echo "ERROR:invalid upload";
		exit(0);
	}
    $params['resize']=$_SESSION['img_pattern']['Форум']['site']['resize'] ; // array('width'=>200,'fields'=>1,'resampled'=>1,'quality'=>100,'back_color'=>'#FFFFFF');
    $params['text']=$_SESSION['img_pattern']['Форум']['site']['text'] ; //array('color'=>'#ffffff','back_color'=>'#000000','font_name'=>'images/swisse.ttf','font_size'=>10) ;

    list($source,$type)=open_image($_FILES["Filedata"]["tmp_name"]) ;
    $img_info=getimagesize($_FILES["Filedata"]["tmp_name"]) ;
    $fsize=byte_to_kbyte(filesize($_FILES["Filedata"]["tmp_name"])) ;

  	resize_image($source,$params['resize']) ;
    $source=image_add_info($source,'('.$img_info[0].'x'.$img_info[1].')     '.$fsize.' kb',$params['text']) ;

	if (!isset($_SESSION["file_info"])) $_SESSION["file_info"] = array();

	// Use a output buffering to load the image into a variable
	ob_start();
	imagejpeg($source);
	$imagevariable = ob_get_contents();
	ob_end_clean();

	$file_id = md5($_FILES["Filedata"]["tmp_name"] + rand()*100000);

	$_SESSION["file_info"][$file_id]['small'] = $imagevariable;
	$_SESSION["file_info"][$file_id]['source'] = file_get_contents($_FILES["Filedata"]["tmp_name"]);
	$_SESSION["file_info"][$file_id]['type'] = $type;


	echo "FILEID:" . $file_id;	// Return the file id to the script
}

// добавляет к изображению рамку и информацию по изображению
 function image_add_info(&$source,$text,$options=array())
 { $w1=imagesx($source)  ; $h1=imagesy($source) ;
   $im_convas  = imagecreatetruecolor($w1+4,$h1+4+20);
   imagefill($im_convas,0,0,hexdec($options['back_color']));
   if (!imagecopymerge($im_convas,$source,2,2,0,0,$w1,$h1,100)) echo 'Не удалось наложить картинку<br>';
   ImageDestroy($source);
   $options['angle']=0 ;

   imagefttext($im_convas,$options['font_size'],$options['angle'],10,$h1+18,hexdec($options['color']),_DIR_TO_ROOT.'/'.$options['font_name'],$text) ;
   return($im_convas)  ;

 }

 function show_img_thumbnail_by_id()
 {
	// This script accepts an ID and looks in the user's session for stored thumbnail data.
	// It then streams the data to the browser as an image

	// Work around the Flash Player Cookie Bug
	if (isset($_POST["PHPSESSID"])) session_id($_POST["PHPSESSID"]);

    session_start();

	$image_id = isset($_GET["id"]) ? $_GET["id"] : false;
	$mode     = isset($_GET["mode"]) ? $_GET["mode"] : 'small';

	if ($image_id === false) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "No ID";
		_exit();
	}

	if (!is_array($_SESSION["file_info"]) || !isset($_SESSION["file_info"][$image_id][$mode])) {
		header("HTTP/1.1 404 Not found");
		_exit();
	}

	header("Content-type: image/jpeg") ;
	header("Content-Length: ".strlen($_SESSION["file_info"][$image_id][$mode]));
	echo $_SESSION["file_info"][$image_id][$mode];

 }

   // возвращает правильную сслыку на фото для показа в админке
  // отключена 4.06.12 - более не используется
  function get_img_href($tkey,$pkey,$name,$set_width=100,$set_height=100,$no_img_preview=0)
  {
  	$small_image_src=_DOT($tkey)->patch_to_file.'small/'.$name ;
  	$small_image_dir=_DOT($tkey)->dir_to_file.'small/'.$name ;
    $big_image_src=_DOT($tkey)->patch_to_file.'source/'.$name ;
    $text_width='width='.$set_width ;
    $text_height='height='.$set_height ;

    $small_src=(file_exists($small_image_dir))? $small_image_src:$big_image_src ;

    if ($no_img_preview) $text='<img src="'.$small_src.'" border="0" '.$text_width.' '.$text_height.'/>';
    else                 $text='<a href="'.$big_image_src.'" id=img_'.$pkey.'  target=_clear onclick="return hs.expand(this,config1)"><img src="'.$small_src.'" border="0" width=100 height=100/></a>';
    return($text) ;
  }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>