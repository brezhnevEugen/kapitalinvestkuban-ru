<?
include_once('clss_0.php') ;
class clss_26 extends clss_0
{
  // изменение имени db страницы
  // ДОРАБОТАТЬ: при создании страницы в подразделе, с наличием htaccess и правил в нем разрешить создание любых страниц, файл не создавать
  function on_change_event_href($tkey,$pkey,&$fvalue)
  {    $alt_info=select_db_obj_info($tkey,$pkey) ;
       $parent_info=get_obj_info($alt_info['_parent_reffer']) ;
       if ($parent_info['clss']==13) $test_value='/'.$parent_info['dir'].$fvalue.'.html' ;
       else                          $test_value='/'.$fvalue.'.html' ;
       if ($alt_info['href']==$test_value) return(0) ; // возвращаем, что значение не изменилось

       // виртуальные страницы в каталоге на сайте через modrewrite
       if ($parent_info['clss']==13)
       {  $arr=explode('/',$fvalue) ;
          $checked_arr=array() ;
          if (sizeof($arr)) foreach($arr as $dir) $checked_arr[]=safe_text_to_url($dir,$tkey) ;
          $fvalue='/'.$parent_info['dir'].'/'.implode('/',$checked_arr).'.html' ;
          $fvalue=str_replace('//','/',$fvalue) ;
          return(1) ;
       }

       // переименование страницы из корня сложнее - надо изменить имена существующего скрипта
       //echo '$alt_info[href]='.$alt_info['href'].'<br>' ;
       //echo '$fvalue='.$fvalue.'<br>' ;
       //echo '$test_value='.$test_value.'<br>' ;

       $fvalue=safe_file_names($fvalue) ;
       $fvalue='/'.$fvalue.'.html' ;
       //echo '$fvalue='.$fvalue.'<br>' ;
       if (is_file(_DIR_TO_ROOT.$alt_info['href'])) { ?>Производим <strong>переименование</strong> скрипта страницы сайта<?
                                                         $res=rename(_DIR_TO_ROOT.$alt_info['href'],_DIR_TO_ROOT.$fvalue) ;
                                                         if ($res==1)
                                                         { ?>- <span class=green>OK</span><br><?
                                                           $alt_page_url=execSQL_value('select url from '.TM_SITEMAP.' where obj_reffer="'.$alt_info['_reffer'].'"') ;
                                                           if ($alt_page_url)
                                                           { $new_page_url=str_replace($alt_info['href'],$fvalue,$alt_page_url) ;
                                                             echo $new_page_url.'<br>' ;
                                                             ?>Производим <strong>изменение адреса</strong>  страницы страницы в карте сайта - <span class=green>OK</span><br><?
                                                             update_rec_in_table(TM_SITEMAP,array('url'=>$new_page_url),'obj_reffer="'.$alt_info['_reffer'].'"') ;
                                                           }

                                                         } else {?>- <span class=red>ERROR</span> - не удалось переиновать скрипт<br><?}

                                                       }
       else                                            $res=1 ;
       return($res) ;
 }

  // диалог создания страницы
  function obj_create_dialog($options=array())
     {  include_once(_DIR_TO_ENGINE.'/admin/i_clss_func.php') ;
        $form_id='form_obj_create_dialog_clss_'.$this->clss ;
        ?><form method="POST" action="ajax.php" id=<?echo $form_id?>>
            <div class="black bold">Укажите название (h1) страницы и заполните текст.</div><br>
            Название: <input  type="text" class=text size=100 name="new_page_name" value="" data-required><br><br>
            <h2>Содержание</h2><? show_window_ckeditor_v3($GLOBALS['obj_info'],'',200,'new_page_content') ; ?>
            <br><input type=submit id=submit class=button_green mode=replace_viewer_main cmd=create_obj_after_dialog clss=26 parent_reffer="<?echo $_POST['parent_reffer']?>" value="Создать страницу">
            и <? show_select_action('after_clss_26_create_action',array('go_list'=>'перейти к списку страниц','go_this'=>'перейти к созданной странице','go_create'=>'создать еще одну страницу')) ; ?>
            <script type="text/javascript">new mForm.Submit({form:'<?echo $form_id?>',validateOnBlur:true,blinkErrors:true,shakeSubmitButton: true,submitButton:$$('#submit'),ajax: true, showLoader: true, responseSuccess:'any',onSuccess:function(responseText, responseXML){if (responseXML!=null) mForm_Submit_AJAX_onSuccess(responseXML);}});</script>
          </form>
        <?
     }

  // выполнение создания объекта по результатам диалога
  function create_obj_after_dialog()
  { $finfo['obj_name']=$_POST['new_page_name'];
    $finfo['value']=$_POST['new_page_content'] ;
    ob_start() ;
    $new_obj_rec=$this->obj_create($finfo) ;
    $text=ob_get_clean()  ;
    echo $text ;
    return($new_obj_rec) ;
  }

  // создаем объект нужного класса
  function obj_create($finfo,$options=array())
  { if (!$finfo['obj_name']) $finfo['obj_name']='Новая страница' ;
    // вызываем базовую функцию создания объекта
    $page_info=parent::obj_create($finfo,$options) ;
    $parent_info=get_obj_info($page_info['_parent_reffer']) ;
    if ($parent_info['clss']==13) // страница создается в директории с разбором имен через rewrite engine on
    {  $page_url='/'.$parent_info['dir'].'/'.safe_text_to_url($page_info['obj_name'],$page_info['tkey']).'.html' ;
       update_rec_in_table($page_info['tkey'],array('href'=>$page_url),'pkey='.$page_info['pkey']) ;
    }
    else
    {  $url=($page_info['obj_name'])? $page_info['obj_name']:$page_info['pkey'] ;
       $url=safe_file_names($url) ;
       $url='/'.$url.'.html' ;
       if (file_exists(_DIR_TO_ROOT.$url))
       { ?><div class=alert>Страница "<?echo $url?>" уже существует. Будет использовано произвольное имя файла</div><?
         $url=str_replace('.html','_'.rand(1,1000).'.html',$url) ;
       }
       $ini_patch='ini/patch.php' ;
       // прописываем стандарный скрипт в корне
       $cont="<? // создано системой автоматического создания шаблонов страниц ".get_month_year(0,'day month year time')."\ninclude ('".$ini_patch."')  ;\ninclude (_DIR_TO_ENGINE.'/i_system.php') ;\n\$options=array();\nshow_page(\$options) ;\n?>" ;

       $page_name=basename($url) ;
       $dir=dirname($url) ;

       $cnt=create_page($dir,$page_name,$cont) ;
       if ($cnt) update_rec_in_table($page_info['tkey'],array('href'=>$url),'pkey='.$page_info['pkey']) ;
    }
    return($page_info) ;
  }

    // возвращает значение поля fname массива объекта класса. Отдельная функция необходима, т.к. иногда есть необходимость корректировать значения поля перед отдачей в вывод
    function get_rec_field_to_view($rec,$fname)
     { $value=$rec[$fname] ;
       switch($fname)
       {case 'href': $arr_1=explode('.',$value) ;   // удаляем .html
                     $arr_2=explode('/',$arr_1[0]) ; // удаляем директорию, если есть
                     unset($arr_2[0]) ;
                     if (sizeof($arr_2)>1) unset($arr_2[1]) ;
                     //unset($arr_2[2]) ;
                     //$value=$arr_2[sizeof($arr_2)-1] ; // возвращаем только имя страницы
                     $value=implode('/',$arr_2) ; // возвращаем только имя страницы
                     break ;
        default:    $value=parent::get_rec_field_to_view($rec,$fname) ;
       }
       return($value) ;
     }


  // возвращает текст html ссылки для создания объекта
  function get_create_a_html($parent_reffer)
    { $text='<a href="#" class=button mode=replace_viewer_main cmd=create_obj_dialog clss="'.$this->clss.'" parent_reffer="'.$parent_reffer.'">'.$this->name($parent_reffer).'</a>'  ;
      return($text) ;
    }

  // возвращает текст html кнопки для создания объекта
  function get_create_button_html($parent_reffer)
    { $text='<button mode=replace_viewer_main cmd=create_obj_dialog clss="'.$this->clss.'" parent_reffer="'.$parent_reffer.'">Создать еще '.$this->name($parent_reffer).'</button>'  ;
      return($text) ;
    }

 // реакция на событие удаления объекта - у каждого класса должно выражаться по своему
 function on_delete_event($obj_info,$options=array())
  { if (!$options['debug'] and is_file(_DIR_TO_ROOT.$obj_info['href'])) unlink(_DIR_TO_ROOT.$obj_info['href']) ;
    echo 'Удален скрипт страницы: <strong>'._PATH_TO_SITE.$obj_info['href'].'</strong><br>' ;
    $options['obj_title']='<span class=green>'.$obj_info['obj_name'].'</span> - <strong>'.$obj_info['href'].'</strong>' ;
    parent::on_delete_event($obj_info,$options) ;
  }

}

 // отображение виртуального поля url_view
 function clss_26_url_view($rec,$tkey,$pkey,$options,$cur_fname)
 { $text='<a href="'._PATH_TO_SITE.$rec['href'].'" target="_blank">'.$rec['href'].'</a>' ;
   return($text) ;
 }



?>