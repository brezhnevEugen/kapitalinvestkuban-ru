<?exit();?>
versions 4.82
1.  Прямое указание скрипта с функцией команды у кнопки
    <button class="v1" cmd="engine_moduls/category/i_props.php->import_props_from_list">Импортировать свойства товаров из списков сайта</button>
    <button class="v1" cmd="engine_moduls|import_props_from_list">Импортировать свойства товаров из списков сайта</button>

2.  Добавление в clss_1 поля code=varchar(32) - для указание кодов команд

3.

==========================================================================================================================================================================================

versions 4.74
1. Удалены поля title,list_clss,use_pattern у класса 101
2. Функция create_DOT_object_table - удален параметр mode, передача параметров переделана через options

==========================================================================================================================================================================================
versions 4.63
1. создано описание классов
2. $descr_clss[...]['in_tree_name'] => clss_0::prepare_public_info(&$rec)
3. $arr_tree_clss_names[...]        => clss_0::prepare_public_info(&$rec)

==========================================================================================================================================================================================
version_4.62.2
1. проверить таблицы obj_descr_obj_tables - будет добавлено новое поле "setting"
2. добавить в obj_site_pages новый класс 13 - директория

==========================================================================================================================================================================================
version_4.62
1. проверять, что после всех _DIR_TO_ROOT и _DIR_TO_ENGINE стоит слеш
2. замена $SEO_text => $this->SEO_info
3. в obj_info удалены поля __childs и __childs_pkeys. Первое при подсчете кол-ва объектов не использовало условие отбора объектов.
   Провавильно использовать поле all_cnt_clss
   Второе используется редко, если необходим перечень id дочерних объектов, надо использовать $this->tree[$section_id]->get_list_child() ;

==========================================================================================================================================================================================
version_4.58
1. parent_enabled - проверить, что это поле по умолчнию = 1, и установлено в 1 для начала. �?наче не будет выводься вновь добавленный товар и существующий товар
2. c_yandex.php - вывод  url страниц с русскими буквами необходимо производить через функцию rawurlencode: $offer->add_child('url',$patch_to_site_NS.str_replace('%2F','/',rawurlencode($rec['__href']))) ;// перекодировка русских букв
3. form_cart.php::cart_panel_goods_full Проверять, чтобы не было формы, она вынесена в  c_orders_cart.php
4. функция session_declare удалена окончательно, проверять, чтобы не использовалась в модулях, заменять вызовы на $_SESSION
5. /style_emails.css - в этом файле должны быть определеы стили писем, отправляемых с сайта
6. стили с шаблонов писем должны быть удалены, отправка писем из модулей должны происходить через функцию mail_system->send_html_mail, там автоматически подключается файл /style_emails.css
7. Выполнить функцию "Создаем скрипты для пунктов меню админки" - будет создае скрипт give_file.php
8. save_obj_in_table теперь возвращает ссылку на объект _reffer: pkey.tkey
   проверять модуль orders: c_orders_admin:cmd_exe_order_add_comment()
9. в on_create_event теперь передается не массив, а reffer pkey.tkey нового объекта
10. clss_0_obj_create теперь возвращает не массив, а reffer pkey.tkey нового объекта
11. проверять таблицу "карта сайта" - там новое поле status
12. проверять on_change_event - теперь не надо вносить в описание класса название функции обработчика, достаточно создать функцию с именем clss_XXX_on_change_event()
13. ВНИМАНИЕ! для старых сайтов обязательно переносить переносить href в url_name - чтобы не потерялись ссылки
14. функция  c_system::get_patch_item переименована c_system::get_url_item. Проверить, если функция get_patch_item объявлена в mc_ext_moduls, переименовать в  get_url_item
15. проверить m_goods, удалить  get_patch_item - она полностью реализована в c_system
16. проверять, загружен ли при апдейте движка  PHPMailer_v5.1
17. проверять, существует ли файл style_emails.css

Ошибка при отработке SQL запроса:
'Table './linetest/log_site_pages' is marked as crashed and should be repaired
select session_id,count(pkey) as cnt,user_agent,min(c_data) as first_data from log_site_pages where is_robot=0 group by session_id having first_data>=1337544000'
Показать дамп отладки
Ошибка при отработке SQL запроса:
'Table './linetest/log_site_pages' is marked as crashed and should be repaired
select session_id,count(pkey) as cnt,user_agent from log_site_pages where is_robot=0 and c_data>=1337588895 group by session_id order by c_data'
Показать дамп отладки


REPAIR TABLE  `log_site_pages`




