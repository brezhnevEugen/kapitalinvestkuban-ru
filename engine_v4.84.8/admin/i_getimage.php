<? 	// скрипт для вывода изображений, если к исходникам заблокирован доступ
	// может работать как через использование заранее подготовленных клонов, так и через
	// резайс исходного файла на нужный размер
	//  входные параметры
	//   tkey   - код таблицы
	//   pkey	- код фото
	//	 mode	- название клона который подо показать
	//   name	- путь к фото
	//  если при обращении к клонам изображений их нет, они генерируются здесь
 	include_once(_DIR_TO_ENGINE.'/c_objects.php');
 	connect_database() ;
    session_start('adminfabrikapereezdaru') ; // подключаем текущую сессиию


 	include_once('i_img_working.php');

 //print_r(get_included_files()) ;
	$fname='' ;
 	if ($_GET['tkey'] and $_GET['pkey'])
 	{ 	if (!sizeof(_DOT($_GET['tkey']))) return ;
 	  	if (!sizeof(_DOT($_GET['tkey'])->list_clss_ext[3])) return ;

	 	$img_dir=_DOT($_GET['tkey'])->dir_to_file ;
	 	$table_name=_DOT($_GET['tkey'])->table_name ;
	 	$clone_info=$_SESSION['img_pattern'][_DOT($_GET['tkey'])->img_pattern] ;

	 	if ($_GET['pkey']) { $img_info=execSQL_van('select * from '.$table_name.' where pkey="'.$_GET['pkey'].'"') ;
	 						 $fname=$img_info['obj_name'] ;
						   }

	 	if (!$fname) return ;

	 	if ($_GET['mode']!='small' and !isset($clone_info[$_GET['mode']])) unset($_GET['mode']) ;

	 	if ($_GET['mode'] and !file_exists($img_dir.$_GET['mode'].'/'.$fname)) // если надо показать клон, а клона нет
	 	{ // генерим клон
	 	  if ($_GET['mode']=='small') create_preview_image($img_dir,$fname) ;
	 	   				 else create_clone_image($_GET['tkey'],$fname,array('clone'=>$_GET['mode'])) ;
	 	}

	 	working_image($img_dir.$_GET['mode'].'/'.$fname) ;
    }

    if ($_GET['name'])
    {
       if ($_GET['clone'] and $_GET['tkey']) working_image($_GET['name'],_DOT($_GET['tkey'])->list_clone_img($_GET['clone'])) ;
       else if ($_GET['name'] and $_GET['mode']=='small')
         { $options['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);
           working_image($_GET['name'],$options);
         }
        else working_image($_GET['name']) ;
    }

?>


