<?
//-----------------------------------------------------------------------------------------------------------------------------
// базовый фрейм для отработки команд
//-----------------------------------------------------------------------------------------------------------------------------

include_once('c_admin_page.php') ;
class c_fra_based extends c_admin_page
{ public $trace_exec_cmd ;
  public $no_inner_form=0 ;

  function local_page_head()
  {
    /*?><script type="text/javascript">$j(document).ready(function (){$j(document).inline_editor() ;});</script><?*/
  }

 function body($options=array()) {$this->body_frame($options) ; }

 // выполняем системные команды до получения текущего объекта - они должны быть выполнены до получения текущего объекта
 function cmd_exec_before_select_obj_info($options=array())
 { //damp_array($this) ;
   //damp_array($_POST) ;
   // если в форме перед отправкой производилось подклбчение расширения или класса, подклбчаем его снова
   if (sizeof($_POST['include_extension'])) foreach($_POST['include_extension'] as $ext_name=>$value) include_extension($ext_name,array('no_hidden_var'=>1)) ;
   if (sizeof($_POST['class_file']))     foreach($_POST['class_file'] as $inc_clss=>$value)     include_class($inc_clss,array('no_hidden_var'=>1)) ;
   //damp_array(get_included_files());
   //damp_array($this) ;
   //damp_array($_POST) ;
    include_once (_DIR_TO_ENGINE.'/admin/i_admin_system.php') ;
   ob_start() ;
   if ($this->cmd) switch($this->cmd)
   { case 'save_list':          $this->cmd=save_list_obj($_POST['obj']) ; echo $this->cmd ; break ;
     case 'delete_obj':         $this->cmd=delete_obj($_POST['del_obj']) ;             break ;
     /*default:                   if ($this->cmd_mode=='before_select_obj_info') $this->cmd=$this->panel_function($this->cmd,$this->file) ;*/
     default:                   if ($this->cmd_mode=='before_select_obj_info') list($this->cmd,$cmd_exec)=exec_script_cmd($this->cmd,$this->cmd_script,array('cur_page'=>$this)) ;
   }
   $this->trace_exec_cmd=ob_get_clean() ;
 }

 function select_obj_info() {}

 function cmd_exec_after_select_obj_info($options=array())
 { global $debug_frame_viewer ;
   // отрабатываем действие команды
   // dвесь вывод - в буфер, потом будет показано после менюю
   // this->cmd - метод, функция, расширение переданное через $_GET или $_POST
   // this->file - файл функции (если еще не подключен), передается через $_GET или $_POST

   // проверяем, нет ли для создаваемго класса диалого создания объекта
   // если есть, вместо создания объекта показываем дилог создания
   if ($this->cmd=='create_obj' and $_POST['select_clss'])
   {
     include_class($_POST['select_clss']) ;
     if (function_exists('clss_'.$_POST['select_clss'].'_create_dialog')) $this->cmd='clss_'.$_POST['select_clss'].'_create_dialog' ;
   }

   if (/*$this->cmd=='create_obj' or */$this->cmd=='add_obj_to_cart' or $this->cmd=='cart_copy_as_obj' or $this->cmd=='cart_move_as_obj' or $this->cmd=='cart_copy_as_refs')
     { ob_start() ;
         if ($debug_frame_viewer) echo 'cmd=<span class="bold black">'.$this->cmd.'</span><br>' ;
         // если после отработки команды необходимо обновить инфу по объекту - делать это в самой команде
         // если необходимо выполнить определнную команду по выполнения $this->cmd - вписать в  $cur_menu_item[cmd] и вернуть ok - это каманда будет выполена вместо выполнения
         // пункта меню
         if ($this->cmd) list($this->cmd,$cmd_exec)=exec_script_cmd($this->cmd,$this->cmd_script,array('file'=>$this->cmd_file,'cur_page'=>$this)) ; else  $this->cmd='ok' ;
         if ($debug_frame_viewer) echo '$this->cmd=<span class="bold black">'.$this->cmd.'</span><br>' ;
       $this->trace_exec_cmd.=ob_get_clean() ;

     }
 }

 function block_main() // выводит список разделов
 { global $find_source,$filter,$debug_frame_viewer ; // исходные данные по объекту
   global $obj_info ;
   if (isset($obj_info['tkey'])) $tkey=$obj_info['tkey'] ; elseif (isset($_POST['tkey'])) $tkey=$_POST['tkey'] ; elseif (isset($_GET['tkey'])) $tkey=$_GET['tkey'] ;
   if (isset($obj_info['pkey'])) $pkey=$obj_info['pkey'] ; elseif (isset($_POST['pkey'])) $pkey=$_POST['pkey'] ; elseif (isset($_GET['pkey'])) $pkey=$_GET['pkey'] ;

      // показываем панель текущего объекта
       //$this->show_page_obj_header($obj_info) ;

     if (!$this->no_inner_form)
     {?><form name="form" id=form method="post" action='' ENCTYPE="multipart/form-data" >
                    <input name="space" type="hidden" value="765" />
                    <input name="tkey" type="hidden" value="<?echo $tkey?>"/>
                    <input name="pkey" type="hidden" value="<?echo $pkey ?>"/>
                    <input name="clss" type="hidden" value="<?echo $obj_info['clss'] ?>"/>

                    <input name="find_source" type="hidden" value="<?echo $find_source?>"  id='find_source' />
                    <input name="filter" type="hidden" value="<?echo $filter?>"  id='filter' />
                    <input name="menu_id"    type="hidden" value="<?/*echo $_POST['menu_id']*/?>"  id='menu_id' />
            <?
     }
           // если в форме перед отправкой производилось подклбчение расширения или класса, подклбчаем его снова
           if (sizeof($_POST['include_extension'])) foreach($_POST['include_extension'] as $ext_name=>$value) include_extension($ext_name) ;
           if (sizeof($_POST['class_file']))     foreach($_POST['class_file'] as $inc_clss=>$value) include_class($inc_clss) ;
           if ($obj_info['clss'])     include_class($obj_info['clss']) ;

           if ($debug_frame_viewer) {?><h2>URL</h2><?echo _CUR_PAGE_PATH?><h2>массив GET</h2><? damp_array($_GET,1,-1) ; ?><h2>массив POST</h2><? damp_array($_POST,1,-1) ;}


           ?><div id="fixed_top_panel"><?
           // выводим заголовок страницы
           $this->show_page_obj_header($obj_info) ;

           // выводим меню страницы
           $this->cur_menu_item=$this->show_page_obj_menu($obj_info) ; // этот метод будет переопределяться в фремах модулей, когда необходимо вывести собственноме меню
           ?></div><?

           $this->cur_menu_item['clss']=$obj_info['clss'] ;
           if ($debug_frame_viewer) { echo 'cur_menu_item' ; damp_array($this->cur_menu_item,1,-1) ;  }
           $this->panel_fast_search() ;
           ?><div id=viewer_main <?if ($obj_info['_reffer']) echo 'reffer="'.$obj_info['_reffer'].'"'?>><?

           // показываем лог выполнения команды
           echo $this->trace_exec_cmd ;
           //damp_array($_POST) ;
           //damp_array($this) ;
           // отрабатываем действие команды
           // cmd - метод класса объекта, метод класса страницы, объявления функция, расширение переданное через $_GET или $_POST
           // file - файл функции (если еще не подключен), передается через $_GET или $_POST
           if ($debug_frame_viewer) echo 'this->cmd=<span class="bold black">'.$this->cmd.'</span><br>' ;
           $options=array_merge($this->cur_menu_item,$_POST) ;
           $options['cur_page']=$this ;
           $view_menu_panel=1 ;
           if ($this->cmd and $this->cmd!='ok') list($view_menu_panel,$cmd_exec)=exec_script_cmd($this->cmd,$this->cmd_script,$options) ; // если после отработки команды необходимо обновить инфу по объекту - делать это в самой команде

           if ($debug_frame_viewer) echo '$view_menu_panel=<span class="bold black">'.$view_menu_panel.'</span><br>' ;

           // отрабатываем действие пункта меню
           // выполняем действие, предписанное, пунктом меню
           // $this->cur_menu_item['file'] - имя файла в расширениях сайта, где находиться функция
           // $this->cur_menu_item['cmd'] или
           // $this->cur_menu_item['function'] - функция, которую необходимо выполнить - может быть как в подключенном выше файле, так и в уже подклбченных файлах
           //                                  - метод класса объекта (cmd_xxx), метод класса страницы(xxx), объявления функция, расширение переданное через $_GET или $_POST
           // внимание!!! использована операция === так как при $res_cmd=0 результат ($res_cmd=='ok')==true

           if ($view_menu_panel and $this->cur_menu_item['cmd'])
           {   if ($debug_frame_viewer) echo 'Выполняем функцию меню <strong class=green>'.$this->cur_menu_item['cmd'].'</strong><br>' ;
               $options=$this->cur_menu_item ;
               $options['cur_page']=$this ;
               list($result,$cmd_exec)=exec_script_cmd($this->cur_menu_item['cmd'],$this->cur_menu_item['script'],$options) ;
           }
           else if ($debug_frame_viewer) echo 'Выполнение функции меню <strong class=green>'.$this->cur_menu_item['cmd'].'<strong> заблокировано выполнением cmd <strong class=red>'.$this->cmd.'</strong><br>' ;


           ?></div><?
     if (!$this->no_inner_form) {?></form><?}

     ?><br><br><?

   }


  function show_page_obj_header($cur_obj_info=array())
  { ?><div id="panel_top_menu" reffer="<?echo $cur_obj_info['_reffer']?>">
       <div id="panel_cur_obj">
          <?$this->panel_page_title($cur_obj_info)?>
       </div>
       <!--
       <div id="panel_menu_items">
          <div class="item">Свойства
               <div class="list_subitems">
                    <div class="subitem">Карточка объекта</div>
                    <div class="subitem html">Описание</div>
                    <div class="subitem html">Нижний текст</div>
               </div>
          </div>
          <div class="item">Состав
               <div class="list_subitems">
                    <div class="subitem v2" cmd="get_list_items" clss="3">Фото</div>
                    <div class="subitem v2" cmd="get_list_items" clss="200">Товары</div>
               </div>
          </div>
          <div class="item">Добавить
               <div class="list_subitems">
                    <div class="subitem">Фото</div>
                    <div class="subitem">Товары</div>
               </div>
          </div>
          <div class="item">Операции
               <div class="list_subitems">
                    <div class="subitem">Импорт из XLS</div>
                    <div class="subitem">Экспорт в XLS</div>
               </div>
          </div>
          <div class="item">Корзина</div>
          <div class="clear"></div>
       </div>
       <div class="clear"></div>
       <div id="panel_menu_icons">
         <div class="item button v2" cmd="get_list_items" clss="200">Товар</div>
         <div class="item button v2" cmd="get_list_items" clss="3">Фото</div>
         <div class="item new">
             <div class="list_subitems">
                  <div class="title">Добавить</div>
                  <div class="subitem">Товар</div>
                  <div class="subitem">Фото</div>
             </div>
         </div>
         <div class="item save"></div>
         <div class="item delete"></div>
         <div class="item copy"></div>
         <div class="item seo"></div>
         <div id="panel_fast_search"><input type="text"><div class="item search"></div></div>
         <div class="clear"></div>
       </div>
       -->
     </div>
    <?
  }

// панель вывода название объекта текущей страницы
  function panel_page_title($obj_info)
  {  $obj_clss=_CLSS($obj_info["clss"]);
     if (!$obj_info['__name']) $obj_info['__name']=$obj_info['obj_name'] ;
     if ($obj_info['pkey']>0)
        {  if ($obj_info['_image_name']) $img_src=img_clone($obj_info,'small') ;
           else                          $img_src=$obj_clss->icons ;

           $name='<span class=clss_name>'.$obj_clss->name($obj_info['_parent_reffer'])."</span> '".mb_substr(strip_tags($obj_info['__name']),0,40).((strlen($obj_info['__name'])>40)? '...':'')."'" ;
           $title_func_name='clss_'.$obj_info['clss'].'_frame_title' ;
           if (function_exists($title_func_name)) $name=$title_func_name($obj_info,$name) ;
        }
        else $name=$obj_info['obj_name'] ;

     ?><div id="panel_page_title"><? echo $name ?></div><?
  }

  // панель свойст текущего объекта
  public function cur_obj_show_props($menu_item=array())
  {
     if (sizeof($this->obj_info)) _CLSS($this->obj_info['clss'])->show_props($this->obj_info,$menu_item)  ;
  }
  // панель дочерних объектов текущего объекта
  public function cur_obj_show_list_children($menu_item=array())
  {
    if (sizeof($this->obj_info)) _CLSS($this->obj_info['clss'])->show_list_children($this->obj_info,$menu_item)  ;
  }

 // вывод блока с вариантами создания возможных дочерних объектов
 public function cur_obj_child_create_select($options=array())
 { $use_obj_info=$options['obj_info'] ;
   $show_img=($options['show_img'])? $options['show_img']:1 ;
   $cnt_create_over_list=0 ;
   $use_tkey=($use_obj_info['tkey'])? $use_obj_info['tkey']:$this->obj_info['tkey'] ;
   $use_pkey=($use_obj_info['pkey'])? $use_obj_info['pkey']:$this->obj_info['pkey'] ;
   $use_clss=($use_obj_info['clss'])? $use_obj_info['clss']:$this->obj_info['clss'] ;

   if ($use_pkey>0)
   	{ $list_adding_clss=_CLSS($use_clss)->get_adding_clss($use_tkey,$use_pkey) ;
   	  ?><input id=select_clss name="select_clss" type="hidden" value="" /><?
   	    if (sizeof($list_adding_clss))
        {  ?><h1>Вы можете:</h1><ul><?
          foreach ($list_adding_clss as $clss=>$clss_name) if (!_CLSS($clss)->no_show_dialog_to_create)
           {  if ($show_img or (!$show_img and $clss!=3))
              {   $obj_clss=_CLSS($clss);
                  $obj_clss->name=$clss_name ;  // перезадаем имя класса
                  $text=$obj_clss->get_create_a_html($use_pkey.'.'.$use_tkey) ;
                  if ($text) echo '<li><img class=in_text src="'._PATH_TO_ENGINE.'/admin/images/add_icon.png" width="16" height="17" alt="" border="0"> Добавить '.$text.'<div id=div_child_clss_'.$clss.'></div></li>' ;
                  if (!CLSS($clss)->no_create_by_list) $cnt_create_over_list++ ;
              }
           }
           ?></ul><?
           if ($cnt_create_over_list and !$options['no_create_from_list']){?><br><ul><li><img width="16" height="17" border="0" alt="" src="<?echo _PATH_TO_ENGINE?>/admin/images/add_icon.png" class="in_text">&nbsp;Добавить объекты из <button mode=append_viewer_main cmd=dialog_create_obj_by_list parent_reffer="<?echo $this->obj_info['_reffer']?>" clss="<?echo $this->obj_info['clss']?>" disabled_alter_click=1>списка</button></li></ul><?}
        }

   	}
 }


  // панель HTML-редактора для текущего объекта
  function cur_obj_show_HTML_editor($menu_item)
  { $fname=$menu_item['fname'] ; // выбранные поле передается в текущем пункте меню
    show_window_ckeditor_v3($this->obj_info,$fname) ;

    ?><br><input name="show_field" type="hidden" value="<?echo $fname?>"><button class=button cmd=save_list>Сохранить</button><br><br><br><?

    $_img_name=pc_image_extractor($this->obj_info[$fname]) ; //damp_array($_img_name) ;
    $not_found_img=array() ; $ext_img=array() ;
    if (sizeof($_img_name)) foreach($_img_name as $i=>$rec)
    {  $arr=parse_url($rec[0]) ; //damp_array($arr) ;
       // если изображение находиться на текущем сайте
       if (!$arr['host'] or $arr['host']==_MAIN_DOMAIN or $arr['host']==_BASE_DOMAIN) { if (!file_exists(_DIR_TO_ROOT.$arr['path'])) $not_found_img[]=$rec[0] ; }
       // изображение находиться на врешнем хосте
       else $ext_img[]=$rec[0] ;

    }
    if (sizeof($not_found_img))
    { ?><h2>В тексте найдены ссылки на несуществующие изображения. Если текст был взят с другого сайта, укажите адрес сайта - изображения будут автоматически загруже на сервер:</h2>Адрес сайта: <input name=outer_site class="text big" type=text placeholder="http://имя сайта.ru" value=""><ul><?
      foreach($not_found_img as $name) {?><li><?echo urldecode($name)?></li><?}
      ?></ul><br><button class=button_green cmd=export_not_found_outer_images mode=replace_viewer_main show_fname="<?echo $fname?>">Перенести</button><br><br><?
    }
    if (sizeof($ext_img))
    {  ?><h2>В тексте найдены ссылки на изображения с внешних ресурсов. Рекомендуется перенести изображения на свой сервер</h2><ul><?
       foreach($ext_img as $name) {?><li><?echo urldecode($name)?></li><?}
       ?></ul><br><button class=button_green cmd=export_outer_images mode=replace_viewer_main show_fname="<?echo $fname?>">Перенести</button><br><br><?
    }
  }


   function panel_fast_search() { _CLSS($this->cur_menu_item['show_clss'])->panel_fast_search($this->tkey) ; }






     // выполняет функцию $func_name, которая может быть или в файле расширения, или в методе или в уже подключенных файлах или в классе текущего объекта
    /*
    function panel_function($func_name,$extension_file,&$menu_item=array())
    { //if ($GLOBALS['debug_frame_viewer'])
      $debug=$GLOBALS['debug_search_panel_function'] ; $site_ext='' ;
      if ($debug) echo '<strong>c_fra_based::panel_function</strong>("'.$func_name.'","'.$extension_file.'",'.print_r($menu_item,true).')<br>' ;
      // 30.10.13 - добавляем возможность указания функций из расширений сайта, команда - courer/order_courer_working
      // 19.07.14 - добавляем возможность указания функций из модулей сайта, команда - courer|order_courer_working
      // 19.07.14 - добавляем возможность указания функций из произвольных скриптов сатй, команды:
      //  - engine_moduls/category/i_props.php/import_props_from_list
      //  - ext/catalog/i_cataog.php/func_demo
      if (strpos($func_name,'->')!==false) $func_name=include_script_cmd($func_name,array('debug'=>$debug)) ;
      else
      { $arr=explode('/',$func_name) ;
      if (sizeof($arr)==2) { $func_name=$arr[1] ; $site_ext=$arr[0] ;}

          if ($debug) { damp_array($menu_item,1,-1) ; echo 'Ищем функцию <strong class=green>'.$func_name.'</strong><br>' ; }
      $res='ok' ;
      if ($site_ext) include_site_extension($site_ext,$func_name,array('debug'=>$debug)) ;
      if ($extension_file) { include_extension($extension_file) ; if ($debug) echo 'Подключено расширение админки <span class=green>'.$extension_file.'</span><br>' ; }
      if (isset($menu_item['clss']))
      { $obj_clss=_CLSS($menu_item['clss']) ;
        //damp_array($obj_clss) ;
        if (method_exists($obj_clss,$func_name)) { if ($debug) echo 'Найдена в классе <span class=green>clss_'.$menu_item['clss'].'</span><br><br>' ; $res=$obj_clss->$func_name($menu_item) ; return($res) ; }
      }
      }
      if ($func_name)
      { if (method_exists($this,$func_name))  { if ($debug) echo 'Найдена в классе страницы <span class=black>'.hide_server_dir(__FILE__).'</span>::<span class=green>'.__CLASS__.'</span><br><br>' ; $res=$this->$func_name($menu_item) ; }
       elseif (function_exists($func_name)) { if ($debug) echo 'Функция <strong>'.$func_name.'</strong> <span class=green>найдена</span><br>'; $res=$func_name($menu_item) ; }
        elseif (include_extension($func_name,array('no_hidden_var'=>$this->no_hidden_var)) and function_exists($func_name)) { if ($debug) echo 'Найдена в расширении <span class=green>'.$func_name.'</span><br><br>' ; $res=$func_name($menu_item) ; }
      }
      return($res) ;
    }  */

    function button_demo_1() { ?>Метод demo_1 внутри фрейма. Возвращаем ok для отработки функции меню<? return('ok') ; }

    function _show_page_obj_header($obj_info)  {}

    function show_page_obj_menu($obj_info) { return(array());}

    function cmd_exe_edit() { save_list_obj($_POST['obj']) ;}

}
?>