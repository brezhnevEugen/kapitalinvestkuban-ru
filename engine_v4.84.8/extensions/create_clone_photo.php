<?
// создаем отсутствующие клоны изображений
function create_clone_photo($tkey=0,$clone='',$count=0)
{
  if (!$tkey) $tkey=$_POST['table_id'] ;
  if (!$tkey) $tkey=$_GET['table_id'] ;
  if (!$clone) $clone=$_POST['clone'] ;
  if (!$clone) $clone=$_GET['clone'] ;
  if (!$count) $count=$_POST['count'] ;
  if (!$count) $count=$_GET['count'] ;
  if (!$count) $count=100000000000000 ;


  //global $obj_info ;
  //$arr[$tkey][$obj_info['clss']][$obj_info['pkey']]=$obj_info ;
  //update_clone_photo($arr,0) ;
  echo '<h1>Создаем отсутствующие клоны (№ '.$clone.'), таблица '._DOT($tkey)->table_name.'.</h1>' ;
  echo 'Параметры создания клона "'.$clone.'":<br>' ; damp_array(_DOT($tkey)->list_clone_img($clone),1,-1) ;


  //$db_list_images=execSQL('select obj_name,pkey,parent from '._DOT($tkey_images)->table_name.' where clss=3 and obj_name>""') ;
  //echo 'Всего в базе '.sizeof($db_list_images).' изображений<br>' ;
  $dir_image=_DOT($tkey)->dir_to_file.'source/' ;
  $dir_clone=_DOT($tkey)->dir_to_file.$clone.'/' ;
  $dir_list_images=scandir($dir_image) ;
  //damp_array($dir_list_images) ;
  $dir_list_clones=scandir($dir_clone) ;


  // чистим массив исходников от не-фоток
  if (sizeof($dir_list_images)) foreach ($dir_list_images as $indx=>$fname)
    if (!(stripos($fname,'.jpg')!==false or stripos($fname,'.jpeg')!==false or stripos($fname,'.gif')!==false or stripos($fname,'.png')!==false or stripos($fname,'.bmp')!==false)) unset($dir_list_images[$indx]) ;
  // чистим массив клонов от не-фоток
  if (sizeof($dir_list_clones)) foreach ($dir_list_clones as $indx=>$fname)
    if (!(stripos($fname,'.jpg')!==false or stripos($fname,'.jpeg')!==false  or stripos($fname,'.gif')!==false or stripos($fname,'.png')!==false or stripos($fname,'.bmp')!==false)) unset($dir_list_clones[$indx]) ;

  $res_array=array_diff($dir_list_images,$dir_list_clones) ;

  echo 'Получено содержимое каталога '.hide_server_dir($dir_image).' - <strong>'.sizeof($dir_list_images).'</strong> фото<br>' ;
  echo 'Получено содержимое каталога '.hide_server_dir($dir_clone).' - <strong>'.sizeof($dir_list_clones).'</strong> фото<br>' ;
  echo 'Требуется создать клонов - <strong>'.sizeof($res_array).'</strong> фото<br>' ;

  //return ;
  include_once(_DIR_TO_ENGINE.'/admin/i_img_working.php') ;

  $i=1 ;
  if (sizeof($res_array)) foreach ($res_array as $fname)
   { echo $i.'. ' ;

     if ($clone=='small')
            create_clone_image($tkey,$fname,array('clone'=>'small')) ;
       else create_clone_image($tkey,$fname,array('clone'=>$clone)) ;

     $i++ ; if ($i>$count) break ;
   }
}

?>