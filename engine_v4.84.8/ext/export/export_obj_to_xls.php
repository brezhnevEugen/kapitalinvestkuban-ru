<?
//---------------------------------------------------------------------------------------------------------------------------------------------------
//
// экспорт объектов в EXCEL
//
//---------------------------------------------------------------------------------------------------------------------------------------------------

 function export_obj_to_xls()
 {  global $goods_clss,$obj_info ;
    if ($_GET['pkey'] and $_GET['tkey']) { export_obj_to_xls_file($_GET['tkey'],$_GET['pkey']) ; return ; }
     $goods_clss=200 ;
     ?><h1>Экспорт товаров в файл XLS</h1>
       <h2>Выбирите экпортируемые поля</h2>
         <input name="export_fileds[pkey]" type="checkbox" value="Код" checked=""> Код<br>
         <input name="export_fileds[parent]" type="checkbox" value="Подраздел" checked> Подраздел<br>
         <input name="export_fileds[brand]" type="checkbox" value="Бренд" checked> Бренд<br>
         <input name="export_fileds[art]" type="checkbox" value="Артикул" checked> Артикул<br>
         <input name="export_fileds[obj_name]" type="checkbox" value="Наименование" checked> Наименование<br>
         <input name="export_fileds[enabled]" type="checkbox" value="Состояние" checked> Состояние<br>
         <input name="export_fileds[indx]" type="checkbox" value="Позиция" checked> Позиция<br>
         <input name="export_fileds[price]" type="checkbox" value="Цена" checked> Цена<br>
         <!--
         <input name="export_fileds[koof]" type="checkbox" value="Наценка" checked> Наценка<br>
         <input name="export_fileds[val]" type="checkbox" value="Валюта" checked> Валюта<br>
         -->
         <input name="export_fileds[stock]" type="checkbox" value="Склад" checked> Склад<br>
         <!--
         <input name="export_fileds[sales_type]" type="checkbox" value="sales_type" checked> sales_type<br>
         <input name="export_fileds[sales_value]" type="checkbox" value="sales_value" checked> sales_value<br>
         -->
         <input name="export_fileds[top]" type="checkbox" value="Хит" checked> Хит<br>
         <input name="export_fileds[new]" type="checkbox" value="Новинка" checked=> Новинка<br>
         <input name="export_fileds[yandex]" type="checkbox" value="Выгрузка в яндекс" checked> Выгрузка в яндекс<br>
         <input name="export_fileds[reestr]" type="checkbox" value="Номер сертификата" checked> Номер сертификата<br>
         <input name="export_fileds[reestr_status]" type="checkbox" value="Cертификация" checked> Cертификация<br>
         <input name="export_fileds[demo]" type="checkbox" value="Демо" checked=""> Демо<br>
         <input name="export_fileds[image]" type="checkbox" value="URL изображения" checked> URL изображения<br>
         <input name="export_fileds[intro]" type="checkbox" value="Краткое описание"> Краткое описание<br>
         <input name="export_fileds[manual]" type="checkbox" value="Описание"> Описание<br>
         <input name="export_fileds[files]" type="checkbox" value="Файлы"> Файлы<br>
         <br><br><button  class=button mode=give_file cmd=export_obj_to_xls_file script="ext/export/export_obj_to_xls.php">Получить файл</button>
     <?
 }

   function export_obj_to_xls_file()
   { $tkey=$_POST['tkey'] ;
     $pkey=$_POST['pkey'] ;
     //damp_array($_POST) ;
     $obj_info=select_db_obj_info($tkey,$pkey) ;
     $parents=$_SESSION['goods_system']->tree[$pkey]->get_list_child() ;
     
     //echo $parents ; return ;
     //echo 'Требуется заменить выгрузку на execSQL' ; return ;
     global $arr,$arr2,$aSheet,$row ;
     $arr=select_objs(_DOT($tkey)->table_name,'','parent in ('.$parents.')',array('order_by'=>'parent,indx','no_group_clss'=>1)) ;
     //damp_array(array_keys($arr[_DOT($tkey)])) ;
     //$files=execSQL('select pkey,parent,file_name from obj_site_goods_out_5 where clss=5 and parent in ('.implode(',',array_keys($arr[67])).')') ;
     $arr2=group_by_field('parent',$arr[$tkey]) ;
     //$arr2_files=group_by_field('parent',$files) ;
     //damp_array($arr2) ;
     //damp_array($arr2_files) ; return ;

     set_include_path(_DIR_EXT.'/PHPExcel/');
     if (!file_exists(_DIR_EXT.'/PHPExcel/')) {echo 'Расширение PHPExcel не установлено '; return ; } ;
     include_once 'PHPExcel/IOFactory.php';

     $pExcel = new PHPExcel();
     $pExcel->setActiveSheetIndex(0);
     $aSheet = $pExcel->getActiveSheet();
     $aSheet->setTitle($obj_info['obj_name']);
     //настройки для шрифтов
     $baseFont = array('font'=>array('name'=>'Arial Cyr','size'=>'10','bold'=>false));
     $boldFont = array('font'=>array('name'=>'Arial Cyr','size'=>'10','bold'=>true));
     $center = array('alignment'=>array('horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP));

     $col_ord=65 ;
     $row=1 ;
     if (sizeof($_POST['export_fileds'])) foreach($_POST['export_fileds'] as $fname)
     { $col_char=iconv('windows-1251','UTF-8',chr($col_ord)) ; //echo $col_char.'-' ;
       $aSheet->setCellValue($col_char.$row,$fname); $col_ord++ ;
       $aSheet->getStyle($col_char.$row)->applyFromArray($boldFont)->applyFromArray($center);
     }
     $row++ ;

     $_SESSION['goods_system']->tree[$pkey]->exec_func_recursive('export_rec_to_xls') ;
     // damp_array($arr2) ; return ;
     //отдаем пользователю в браузер
     include("PHPExcel/Writer/Excel5.php");
     $objWriter = new PHPExcel_Writer_Excel5($pExcel);
     header('Content-Type: application/vnd.ms-excel;name="export.xls"');
     header('Content-Disposition: attachment;filename="export.xls"');
     header('Cache-Control: max-age=0');
     $objWriter->save('php://output');
   }

   function export_rec_to_xls($pkey)
   { global $arr2,$arr2_files,$aSheet,$row ;
     if (sizeof($arr2[$pkey])) foreach($arr2[$pkey] as $id=>$rec)
     {  $col_ord=65 ;
        $_SESSION['goods_system']->prepare_public_info($rec) ;
        $col=0 ;  //echo chr(65).'-' ; //echo ord(iconv('UTF-8','windows-1251','Z')).'-' ;
        if (sizeof($_POST['export_fileds'])) foreach($_POST['export_fileds'] as $fname=>$title)
        {  $value='' ;
           $no_set_value=0 ;
           $col_char=iconv('windows-1251','UTF-8',chr($col_ord)) ; //echo $col_char.'-' ;

           switch($fname)
           { case 'parent': $parent_id_obj=$_SESSION['goods_system']->tree[$rec['parent']]->get_parent_by_clss(10) ;
                            $value=strip_tags($_SESSION['goods_system']->tree[$parent_id_obj]->name) ;
                            $aSheet->getColumnDimension($col_char)->setWidth(40); //
                            break ;
             case 'brand':  $brand_id_obj=$_SESSION['goods_system']->tree[$rec['parent']]->get_parent_by_clss(201) ;
                            $value=strip_tags($_SESSION['goods_system']->tree[$brand_id_obj]->name) ;
                            $aSheet->getColumnDimension($col_char)->setWidth(40);
                            break ;
             case 'image':  $url=img_clone($rec,'400') ;
                            $Hyperlink = new PHPExcel_Cell_Hyperlink($url,$url);
                            $Hyperlink->setUrl($url) ;
                            $Hyperlink->setTooltip($url);
                            $aSheet->setCellValueByColumnAndRow($col,$row,$url)->setHyperlink($Hyperlink) ;
                            $no_set_value=1 ;
                            $aSheet->getColumnDimension($col_char)->setWidth(80);
                            break ;
             case 'val':    $value=$_SESSION['VL_arr'][$rec[$fname]]['YA']; break ;
             case 'price':  $value=($rec[$fname]>0)? $rec['__price_5']:''; break ;
             case 'stock':  $value=($rec[$fname])? 'Есть':''; break ;
             case 'enabled':$value=($rec[$fname])? 'Вкл.':'Выкл.'; break ;
             case 'obj_name':
                            $value=$rec[$fname]  ;
                            $aSheet->getColumnDimension($col_char)->setWidth(40);
                            break ;
             case 'demo':
             case 'ya':     $value=($rec[$fname])? 'Да':'';
                            break ;
             default:       $value=$rec[$fname] ;
           }
           //if (!$no_set_value) $aSheet->setCellValue($col_char.$row,$value);
           if (!$no_set_value) $aSheet->setCellValueByColumnAndRow($col,$row,$value);
           $col_ord++ ;
           $col++ ;
           if ($col_ord>90) break ;
        }

        if ($_POST['export_fileds']['files'] and sizeof($arr2_files[$rec['pkey']]))
        foreach($arr2_files[$rec['pkey']] as $rec_file)
        {  $col_char=iconv('windows-1251','UTF-8',chr($col_ord)) ; //echo $col_char.'-' ;
           $url=_DOT(83)->patch_to_file.$rec_file['file_name'] ;
           $Hyperlink = new PHPExcel_Cell_Hyperlink($url,$url);
           $Hyperlink->setUrl($url) ;
           $Hyperlink->setTooltip($url);
           //$aSheet->getCell($col_char.$row)->setValue($url)->setHyperlink($Hyperlink) ;
           $aSheet->setCellValueByColumnAndRow($col,$row,$url)->setHyperlink($Hyperlink) ;
           $aSheet->getColumnDimension($col_char)->setWidth(80);
           $col_ord++ ;
           $col++ ;
           if ($col_ord>90) break ;
        }

        unset($arr2[$pkey][$id]) ;
        $row++ ; //echo '<br>' ;
     }
   }

?>