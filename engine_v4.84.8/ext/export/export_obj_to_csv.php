<?
//---------------------------------------------------------------------------------------------------------------------------------------------------
//
// экспорт объектов в CSV
//
//---------------------------------------------------------------------------------------------------------------------------------------------------

 function export_obj_to_csv()
 {   ?><h1>Экспорт товаров в файл CSV</h1>
       <h2>Выбирите экпортируемые поля</h2>
         <input name="export_fileds[pkey]" type="checkbox" value="Код" checked=""> Код<br>
         <input name="export_fileds[parent]" type="checkbox" value="Подраздел" checked> Подраздел<br>
         <input name="export_fileds[brand]" type="checkbox" value="Бренд" checked> Бренд<br>
         <input name="export_fileds[art]" type="checkbox" value="Артикул" checked> Артикул<br>
         <input name="export_fileds[obj_name]" type="checkbox" value="Наименование" checked> Наименование<br>
         <input name="export_fileds[enabled]" type="checkbox" value="Состояние" checked> Состояние<br>
         <input name="export_fileds[indx]" type="checkbox" value="Позиция" checked> Позиция<br>
         <input name="export_fileds[price]" type="checkbox" value="Цена" checked> Цена<br>
         <!--
         <input name="export_fileds[koof]" type="checkbox" value="Наценка" checked> Наценка<br>
         <input name="export_fileds[val]" type="checkbox" value="Валюта" checked> Валюта<br>
         -->
         <input name="export_fileds[stock]" type="checkbox" value="Склад" checked> Склад<br>
         <!--
         <input name="export_fileds[sales_type]" type="checkbox" value="sales_type" checked> sales_type<br>
         <input name="export_fileds[sales_value]" type="checkbox" value="sales_value" checked> sales_value<br>
         -->
         <input name="export_fileds[top]" type="checkbox" value="Хит" checked> Хит<br>
         <input name="export_fileds[new]" type="checkbox" value="Новинка" checked=> Новинка<br>
         <input name="export_fileds[yandex]" type="checkbox" value="Выгрузка в яндекс" checked> Выгрузка в яндекс<br>
         <input name="export_fileds[reestr]" type="checkbox" value="Номер сертификата" checked> Номер сертификата<br>
         <input name="export_fileds[reestr_status]" type="checkbox" value="Cертификация" checked> Cертификация<br>
         <input name="export_fileds[demo]" type="checkbox" value="Демо" checked=""> Демо<br>
         <input name="export_fileds[image]" type="checkbox" value="URL изображения" checked> URL изображения<br>
         <input name="export_fileds[intro]" type="checkbox" value="Краткое описание"> Краткое описание<br>
         <input name="export_fileds[manual]" type="checkbox" value="Описание"> Описание<br>
         <br><br><button  class=button mode=give_file  cmd=export_obj_to_csv_file script="ext/export/export_obj_to_csv.php" >Получить файл</button>
     <?
 }

   function export_obj_to_csv_file()
   { global $TM_goods,$arr2 ;
     $parents=$_SESSION['goods_system']->tree[$_POST['pkey']]->get_list_child() ;
     $arr=select_objs($TM_goods,'','parent in ('.$parents.')',array('order_by'=>'parent,indx','no_group_clss'=>1)) ;
     $arr2=group_by_field('parent',$arr[$_SESSION['goods_system']->tkey]) ;

       header('Content-Type: application/vnd.ms-excel;name="export.csv"');
       header('Content-Disposition: attachment;filename="export.csv"');
       header('Cache-Control: max-age=0');
       header('Pragma: public');

       ob_start() ;
       //if (sizeof($_POST['export_fileds'])) foreach($_POST['export_fileds'] as $fname=>$title) $arr_titles[]=strip_tags(get_obj_field_title(200,$fname)) ;
       //$arr_titles[]=$_POST['export_fileds'] ;
       echo implode(';',$_POST['export_fileds'])."\n" ;
       $_SESSION['goods_system']->tree[$_POST['pkey']]->exec_func_recursive('export_rec_to_csv') ;
       $text=ob_get_clean() ;
       $text=iconv('utf-8','windows-1251',$text) ;
       //echo nl2br($text) ;
       echo $text ;

   }

   function export_rec_to_csv($pkey)
   { global $arr2 ;
     //echo strip_tags($_SESSION['goods_system']->tree[$pkey]->name)."\n" ;
     //print_2x_arr($_POST['export_fileds']) ;
     //print_2x_arr($arr2) ;
     if (sizeof($arr2[$pkey])) foreach($arr2[$pkey] as $rec)
     {  $arr_output=array() ;
        $_SESSION['goods_system']->prepare_public_info($rec,array('no_dop_koof'=>0)) ;
        if (sizeof($_POST['export_fileds'])) foreach($_POST['export_fileds'] as $fname=>$title)
        {  $value='' ;

           switch($fname)
           { case 'parent': $parent_id_obj=$_SESSION['goods_system']->tree[$rec['parent']]->get_parent_by_clss(10) ;
                            $value=$_SESSION['goods_system']->tree[$parent_id_obj]->name ;
                            break ;
             case 'brand':  $brand_id_obj=$_SESSION['goods_system']->tree[$rec['parent']]->get_parent_by_clss(201) ;
                            $value=$_SESSION['goods_system']->tree[$brand_id_obj]->name ;
                            break ;
             case 'image':  $value=img_clone($rec,'400') ;
                            break ;
             case 'val':    $value=$_SESSION['VL_arr'][$rec[$fname]]['YA']; break ;
             case 'price':  $value=($rec[$fname]>0)? $rec['__price_4']:''; break ;
             case 'stock':  $value=($rec[$fname])? 'Есть':''; break ;
             case 'enabled':$value=($rec[$fname])? 'Вкл.':'Выкл.'; break ;
             case 'demo':
             case 'ya':     $value=($rec[$fname])? 'Да':'';
                            break ;
             default:       $value=$rec[$fname] ;
           }
           $arr_output[]='"'.str_replace(array("\n"),array(' '),strip_tags($value)).'"' ;
        }
        echo implode(';',$arr_output)."\n" ;
     }
   }
?>