<?

function test_include_script_cmd()
{
    ?><h1>test_include_script_cmd</h1><?

 //
 // V1:
 // cmd=test_panel                              - текущий класс страницы
 // cmd=demo/test_panel                         - /class/ext/demo/test_panel.php
 // cmd=test_panel from=ext/demo                - /engine/ext/demo/test_panel.php
 // cmd=test_panel from=ext/demo/i_demo.php     - /engine/ext/demo/i_demo.php
 // cmd=test_panel from=mod/demo                - /class/engine_modules/demo/test_panel.php
 // cmd=test_panel from=mod/demo/i_demo.php     - /class/engine_modules/demo/i_demo.php
 //
 // V2:
 // cmd=test_panel                              - /class/c_ajax.php для сайта и /engine/admin/c_ajax.php для админки
 // cmd=demo/test_panel                         - /class/ext/demo/c_ajax.php
 // cmd=test_panel from=ext/demo                - /engine/ext/demo/c_ajax.php
 // cmd=test_panel from=mod/demo                - /class/engine_modules/demo/c_ajax.php
  ?><h2>cmd=test_panel - текущий класс страницы</h2><?
  include_script_cmd('test_panel')  ;
  ?><h2>cmd=demo/test_panel - /class/ext/demo/test_panel.php</h2><?
  include_script_cmd('test/test_panel')  ;
  ?><h2>cmd=demo/test_panel - /class/ext/demo/test_panel.php</h2><?
  include_script_cmd('test/test_panel')  ;

}
?>