<?

 // панель свойств типа товара
 // $_SESSION['descr_clss'][60]['menu'][]=array("name" => "Свойства",'script'=>'mod/category/i_menu_panel.php',"cmd" =>'panel_props_of_type') ;
 function panel_template_code()
  { global $obj_info ;
    //damp_array($obj_info) ;
    if (!$obj_info['script']) {?><div class="alert">Не указан скрипт шаблона</div><? return ;}
    list($dir,$fname)=explode('/',$obj_info['script']) ;
    if (!$fname) {$fname=$dir ; $dir='templates' ; }
    else $dir='ext/'.$dir ;
    $script_dir=_DIR_TO_CLASS.'/'.$dir.'/'.$fname.'.php' ;
    if (!file_exists($script_dir)) {?><div class="alert">Не найден скрипт шаблона <strong><?echo hide_server_dir($script_dir)?></strong></div><? return ;}
    $text=file_get_contents($script_dir) ;
    ?><!-- Include required JS files -->
      <script type="text/javascript" src="<?echo _PATH_TO_ENGINE?>/AddOns/syntaxhighlighter_3.0.83/scripts/shCore.js"></script>
      <!-- At least one brush, here we choose JS. You need to include a brush for every language you want to highlight-->
      <script type="text/javascript" src="<?echo _PATH_TO_ENGINE?>/AddOns/syntaxhighlighter_3.0.83/scripts/shBrushPhp.js"></script>
      <!-- Include *at least* the core style and default theme -->
      <link href="<?echo _PATH_TO_ENGINE?>/AddOns/syntaxhighlighter_3.0.83/styles/shCore.css" rel="stylesheet" type="text/css" />
      <link href="<?echo _PATH_TO_ENGINE?>/AddOns/syntaxhighlighter_3.0.83/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />
      <pre class="brush: php">
      <?echo htmlspecialchars($text) ;?>
      </pre>
      <!-- Finally, to actually run the highlighter, you need to include this JS on your page -->
      <script type="text/javascript">SyntaxHighlighter.all()</script>
      <?

      //damp_array($_SESSION['list_created_sybsystems']) ;

  }

  function panel_template_view()
  { global $obj_info ;
    panel_template_info($obj_info) ;
    /*?>[<?echo $obj_info['obj_name']?>]<?*/
    ?><iframe id=demo_frame src="/blank.php?template_id=<?echo $obj_info['pkey']?>" style="width:100%;height:10000px;border:none;margin-top:20px;"></iframe><?
  }

  function panel_template_info($obj_info)
  { $links=get_arr_links($obj_info['pkey'],$obj_info['tkey']) ;   $link_obj_rec=array() ;
    if (sizeof($links))
      {  list($link_obj_reffer,$temp)=each($links) ;
         $link_obj_rec=get_obj_info($link_obj_reffer) ;
      }

    if ($obj_info['system'])
    {  $system_name=$_SESSION['list_created_sybsystems'][$obj_info['system']] ;
       echo 'Шаблон будет выводить объекты из подсистемы <strong>'.$system_name.'</strong><br>' ;
    }

    switch($obj_info['template_obj'])
    {  case 0: if ($link_obj_rec['pkey']) echo 'Пример показан с использованием объекта "<strong>'.$link_obj_rec['obj_name'].'</strong>"<br>' ;
               //else  echo '<span class="red">Слинкуйте объект, если он необходим  для предварительного просмотра шаблона</span>' ;
               echo '<p><strong>Код для подстановки шаблона:</strong><input type="text" style="width:300px;" value="['.$obj_info['obj_name'].']"></p>' ;
               break ;
       case 1: echo 'Шаблон использует текущий объект страницы' ;
               if ($link_obj_rec['pkey']) echo ', пример показан с использованием объекта "<strong>'.$link_obj_rec['obj_name'].'</strong>"<br>' ;
               //else  echo '<span class="red">Слинкуйте объект, если он необходим  для предварительного просмотра шаблона</span>' ;
               echo '<p><strong>Код для подстановки шаблона:</strong><input type="text" style="width:300px;" value="['.$obj_info['obj_name'].']"></p>' ;
               break ;
       case 2: echo 'Шаблон использует заданный объект:' ;
               if ($link_obj_rec['pkey']) echo '"<strong>'.$link_obj_rec['obj_name'].'</strong>"' ;
               else  echo '<span class="red">Слинкуйте необходимый объект для показа в шаблоне</span>' ;
               echo '<p><strong>Код для подстановки шаблона:</strong><input type="text" style="width:300px;" value="['.$obj_info['obj_name'].']"></p>' ;
               break ;
       case 3: echo 'Шаблон использует объект, код которого передан вторым параметром шаблона' ;
               if ($link_obj_rec['pkey']) echo ', пример показан с использованием объекта "<strong>'.$link_obj_rec['obj_name'].'</strong>"<br>' ;
               //else  echo '<span class="red">Слинкуйте объект, если он необходим для предварительного просмотра шаблона</span>' ;
               echo '<p><strong>Код для подстановки шаблона:</strong><input type="text" style="width:300px;" value="['.$obj_info['obj_name'].',XXX]"><br> XXX - код отображаемого объекта</p>' ;
               echo '<p><strong>Код шаблона в текущем примере:</strong><input type="text" style="width:300px;" value="['.$obj_info['obj_name'].','.$link_obj_rec['pkey'].']"></p>' ;
               break ;
    }

  }

  function panel_template_props($rec)
  {  ob_start() ;
     ?><h1><?echo $rec['obj_name']?></h1><?
      _CLSS($rec['clss'])->show_props($rec,array('no_header'=>1,'no_auto_check'=>1)) ;echo '<br><br>' ;
     panel_template_info($rec) ;
     $text=ob_get_clean() ;
     return($text) ;
  }

  function panel_template_preview($rec)
  {  ob_start() ;
     /*?><link rel="stylesheet" type="text/css" href="http://new.formulapereezda.ru/style.css" charset="utf-8"><?*/
     /*?>[<?echo $rec['obj_name']?>]<?*/
     ?><iframe id=demo_frame src="/blank.php?template_id=<?echo $rec['pkey']?>" style="width:1000px;height:500px;border:none;"></iframe><?
     $text=ob_get_clean() ;
     //include_once(_DIR_ENGINE_EXT.'/macros/macros.php') ;
     //macros_apply($text) ;
     return($text) ;
  }


//[template=formulapereezda/panel_responses_list_and_form_create]


?>