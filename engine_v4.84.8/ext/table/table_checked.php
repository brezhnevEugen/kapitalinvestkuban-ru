<?
function table_checked($tkey,$options=array())
{
  global $pkey ;
  if (!$tkey or is_array($tkey)) $tkey=$pkey ;
  // обновляем модель таблиц по текущей таблице
  if (!$options['no_update_descr_tables']) create_model_obj_tables(array('use_tkey_table'=>$tkey)) ;
  $cnt_sql_cmd=0 ;

  if (isset($_SESSION['descr_obj_tables'][$tkey]))
  { $tname=_DOT($tkey)->table_name ;
    //if ($tname) $table_fields=execSQL("DESCRIBE ".$tname) ; // поля проверяемой таблицы
    //if (sizeof($table_fields)) foreach($table_fields as $indx=>$rec) $table_fields[$indx]['tkey']=$tkey ;
    //damp_array($table_fields) ;

    if (!$options['show_in_outer_table']) {?><table class=table_checked><?}
    ?><tr class=clss_header><td colspan=2><?echo "Проверка '"._DOT($tkey)->name."'</strong> ('$tname', "._DOT($tkey)->mode." mode):" ; /*damp_array(_DOT($tkey)->list_clss_ext) ; */?></td></tr><?

      // для каждого объекта, который хранится в таблице, проверяем наличие его перечня полей
      if (sizeof(_DOT($tkey)->list_clss_ext)) foreach(_DOT($tkey)->list_clss_ext as $clss=>$clss_info)
      { //print_r($clss_info) ; echo '<br>' ;
        // перед проверкой каждого класса заново загружаем описание таблицы - чтобы исколчить ошибки повторного добавления полей
        if ($tname) $table_fields=execSQL("DESCRIBE ".$tname) ; // поля проверяемой таблицы
        $obj_clss=_CLSS($clss);
        $clss_fields=$obj_clss->fields ; // поля проверяемого класса
        $clss_default=$obj_clss->default ; // значения по умолчанию класса
        $clss_default['tkey']=$tkey ;
        if($clss_info['main']==$tkey)
          { // формируем перечень полей объекта $clss с учетом  режима хранения объектов или полей класса
            ?><tr class=td_header><td colspan=2><? echo "Проверяем поля для класса <strong>'".$obj_clss->name."'</strong> (clss=".$clss.")&nbsp;" ; ?></td></tr><?

            if (sizeof($clss_fields))
            { // если текущий класс поддерживает мультиязычность, дополняем указанные поля класса соответствующими мультиязычномы полями
              $lang_clss_fields=$clss_fields ;
              if (sizeof($clss_info['multilang'])) foreach($clss_info['multilang'] as $fname=>$value)
                  if (sizeof($GLOBALS['lang_arr'])) foreach($GLOBALS['lang_arr'] as $lang_code=>$lang_info) if ($lang_code) $lang_clss_fields[$fname.$lang_info['suff']]=$clss_fields[$fname] ;

              //damp_array($lang_clss_fields) ;
              foreach ($lang_clss_fields as $fname=>$virt_type)
                { $real_type=convert_virt_type_to_db_type($virt_type) ;
                  // сравниваем тип поля
                  if ($table_fields[$fname]['Type']==$real_type) echo "" ;
                     else if (!isset($table_fields[$fname])) // такого поля нет вооще   - - ВЫПОЛНЯЕТСЯ СРАЗУ
                       { ?><tr><td><span class=fname><? echo $fname?></span>: <strong class=red>отсутствует</strong></td>
                               <td><?  execSQL_update("ALTER TABLE `$tname` ADD `$fname` $real_type") ; ?> <span class="green bold">ИСПРАВЛЕНО</span></td>
                           </tr>
                         <?
                       }
                      else  // поле есть но тип  не совпадает
                       { ?><tr><td><span class=fname><? echo $fname?></span>: различные типы полей: "<? echo $table_fields[$fname]['Type']." ~ ".$real_type?> "</td>
                               <td><input name="sql_cmd[]" type="checkbox" value="<?echo "ALTER TABLE `$tname` CHANGE $fname $fname $real_type"?>"> Исправить</td>
                           </tr>
                         <?
                         $cnt_sql_cmd++ ;
                       }
                  // сравниваем значение по умолчанию - ВЫПОЛНЯЕТСЯ СРАЗУ
                  $def_value_by_clss=$clss_default[$fname] ;
                  $def_value_by_table=$table_fields[$fname]['Default'] ;
                  if (isset($clss_default[$fname]) and !isset($table_fields[$fname]['Default']))
                  {
                    ?><tr><td><span class=fname><? echo $fname?></span>: в таблице <strong class=red>не задано</strong> значение по умолчанию: "<? echo $def_value_by_clss?>"</td>
                          <td><? execSQL_update("ALTER TABLE `$tname` CHANGE $fname $fname $real_type NULL DEFAULT  '$def_value_by_clss'") ;
                                 execSQL_update("UPDATE `$tname` SET $fname='$def_value_by_clss' WHERE $fname IS NULL") ;
                              ?> <span class="green bold">ИСПРАВЛЕНО</span></td>
                      </tr>
                    <?
                  }
                  if (!isset($clss_default[$fname]) and isset($table_fields[$fname]['Default']))
                  { ?><tr><td><span class=fname><? echo $fname?></span>: в базе <strong>задано</strong> значение по умолчанию: "<? echo $def_value_by_table?>"</td>
                          <td><?  execSQL_update("ALTER TABLE `$tname` CHANGE $fname $fname $real_type NULL") ; ?> <span class="green bold">ИСПРАВЛЕНО</span></td>
                      </tr>
                    <?
                  }
                  if (isset($clss_default[$fname]) and isset($table_fields[$fname]['Default']) and $clss_default[$fname]!=$table_fields[$fname]['Default'])
                  { ?><tr><td><span class=fname><? echo $fname?></span>: в базе задано значение по умолчанию: "<? echo $def_value_by_table?>" <strong>не соответствует</strong> значению в описании класса: <?echo $def_value_by_clss?></td>
                          <td><?  execSQL_update("ALTER TABLE `$tname` CHANGE $fname $fname $real_type NULL DEFAULT  '$def_value_by_clss'") ; ?> <span class="green bold">ИСПРАВЛЕНО</span></td>
                      </tr>
                    <?
                  }
                }
            }
          }
        // если у таблицы есть дочерняя таблица с фотками, проверяем, что у таблицы есть поле _img_name
        /*
        if ($clss=3 and $clss_info['out'])
        { if (!isset($table_fields['_image_name']))
            { ?><tr><?
              echo "<tr><td>"."<span class=fname>_image_name</span>: <strong class=red>отсутствует</strong></td>" ;
              "ALTER TABLE $tname ADD _image_name varchar(255)" ;
              ?><td><input name="sql_cmd[]" type="checkbox" value="1" checked> Исправить</td><?
              
              ?></tr><?
            }
        }
        */
        // если у тaблицы включен режим parent_enabled (php or sql) проверяем наличие поля "_enabled" - ВЫПОЛНЯЕТСЯ СРАЗУ
      }
       ?><tr class=td_header><td colspan=2><? echo "Проверяем служебные поля таблицы" ; ?></td></tr><?

      if ($tname) $table_fields=execSQL("DESCRIBE ".$tname) ; // поля проверяемой таблицы
       if (_DOT($tkey)->setting['update_parent_enabled'] and !isset($table_fields['_enabled']))
       {  ?><tr><td><span class=fname>_enabled</span>: <strong class=red>отсутствует</strong></td>
                <td><?  execSQL_update("ALTER TABLE `$tname` ADD _enabled int(1) default 1") ; ?> <span class="green bold">ИСПРАВЛЕНО</span></td>
               </tr>
          <?
       }

     // теперь делаем обратную проверку - для каждого из полей объектной таблицы должен существовать хотябы один класс,
     // отвечающий за данное поле
     ?><tr class=td_header><td colspan=2><? echo "Ищем неиспользуемые поля" ; ?></td></tr><?
     if ($tname) $table_fields=execSQL("DESCRIBE ".$tname) ; // поля проверяемой таблицы
     if (sizeof($table_fields)) foreach ($table_fields as $fname=>$finfo)
     {  $cnt=0 ;

       // смотрим описания полей для тех объектов, которые состоят в списке поддерживаемых классов
       //damp_array(_DOT($tkey)->list_clss_ext) ;
       if (sizeof(_DOT($tkey)->list_clss_ext)) foreach (_DOT($tkey)->list_clss_ext as $clss=>$clss_info) if ($clss_info['main'])
       {  $obj_clss=_CLSS($clss);
          $lang_clss_fields=$obj_clss->fields ;

          if (sizeof($clss_info['multilang'])) foreach($clss_info['multilang'] as $fname2=>$value)
             if (sizeof($GLOBALS['lang_arr'])) foreach($GLOBALS['lang_arr'] as $lang_code=>$lang_info)  $lang_clss_fields[$fname2.$lang_info['suff']]=$lang_clss_fields[$fname2] ;

          if (isset($lang_clss_fields[$fname])) $cnt++ ;

       }

       if (!$cnt and $fname!='temp' and $fname!="_enabled" and $fname!="_image_name")
          { ?><tr><td><span class=fname><? echo $fname?></span>: поле не используется.</td>
                  <td><input name="sql_cmd[]" type="checkbox" value="<?echo "ALTER TABLE `$tname` DROP $fname "?>"> Исправить</td>
              </tr>
            <?
            $cnt_sql_cmd++ ;
          }
     }
     //damp_array($table_fields) ;
     /*
     ?><tr class=td_header><td colspan=2><? echo "Проверяем корректность системных полей таблицы" ; ?></td></tr><?
     $tkeys=execSQL_line('select distinct(tkey) from '.$tname) ;
     if (sizeof($tkeys)>1 or $tkeys[0]!=$tkey)
        { ?><tr><td><span class=fname>tkey</span>: некорректное значение.</td>
                <td><?  execSQL_update("UPDATE ".$tname." SET tkey=".$tkey) ;
                        execSQL_update("ALTER TABLE ".$tname." ADD tkey default ".$tkey) ;
                    ?> <span class="green bold">ИСПРАВЛЕНО</span></td>
            </tr>
          <?
        }
     */
     // если в таблице
     if (!$options['show_in_outer_table']) {?></table><?}

    $table_fields=execSQL("DESCRIBE ".$tname) ; // поля проверяемой таблицы
    update_rec_in_table(TM_DOT,array('fields_info'=>serialize($table_fields)),'pkey='.$tkey,array('debug'=>0)) ;


  } else echo "<span>Объектная таблица pkey=$tkey не существует</span><br>" ;

  if (!$options['no_show_button'])
  { if ($cnt_sql_cmd) { ?><button class=button cmd=exec_sql_list_cmd mode=dialog>Выполнить выбранные операции</button><? }
    else              { ?><div class=info>Структура полей таблицы в порядке</div><? }
  }

  return($cnt_sql_cmd) ;
}
?>