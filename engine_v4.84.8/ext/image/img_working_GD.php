<?php

//*************************************************************************************************************************************/
//
// фукции обработки изображений
//
//*************************************************************************************************************************************/

// создать клон $clone фото $fname из таблицы $tkey
 // или создать все клоны

 function create_clone_image($tkey,$fname,$options=array())
 {
    $dir=_DOT($tkey)->dir_to_file ;
    $options['tkey']=$tkey ;
    if (!$options['view_upload']) $options['no_comment']=1 ;

    if (!$options['clone'])
    { $list_clone=_DOT($tkey)->list_clone_img() ;
      $list_clone['small']['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);
      foreach($list_clone as $clone_name=>$rec)
	  { $options_res=array_merge($rec,$options) ;
	    $options_res['output_fname']=$dir.$clone_name.'/'.$fname ;
        if ($options_res['output_fname'] and !$options['no_comment'] and $options['view_upload']) echo 'Создаем клон '.$options_res['resize']['width'].'x'.$options_res['resize']['height'].' ' ;
        if ($fname) $res=working_image($dir.'source/'.$fname,$options_res) ;
        if (!$options['no_comment']) echo ($res)? ', <span class=green><strong>'.$res['put'][3].'</strong></span>качество '.$options_res['resize']['quality'].'<br>':'<span class="red bold"> Не удалось создать клон (несовместимый формат, файл поврежден, нет директрии или места на диске, нет прав) </span><br>' ;
	  }
	}
	else
	{   if ($options['clone']=='small') $options_res['resize']=array('width'=>100,'height'=>100,'fields'=>1,'resampled'=>1,'back_color'=>'FFFFFF','quality'=>100);
	    						   else $options_res=array_merge(_DOT($tkey)->list_clone_img($options['clone']),$options) ;
	    $options_res['output_fname']=$dir.$options['clone'].'/'.$fname ;
        if (!$options['no_comment']) echo 'Создаем клон '.$options_res['resize']['width'].'x'.$options_res['resize']['height'].' для '.$options_res['output_fname'] ;
        if ($fname) $res=working_image($dir.'source/'.$fname,$options_res) ;
        if (!$options['no_comment']) echo (($res)? ', <span class=green> размер=<strong>'.$res['put'][3].'</strong> kb </span>качество '.$options_res['resize']['quality'].'<br>':'<span class="red bold"> Не удалось создать клон (несовместимый формат,файл поврежден, нет директрии или места на диске, нет прав) </span><br>') ;
	}
	return($res) ;
 }

 // функция обработки изображений

// *** опции масштабирования изображения ****
// $options['resize']['width']   	- ширина нового изображения
// $options['resize']['height']  	- высота нового изображения
// $options['resize']['fields']  	- масштабировать с полями (1) или без (0)
// $options['resize']['resampled']- ресамп изображения при резайсе

// *** опции наложения логотипа ****
// логотипов может быть несколько

// $options['logo'][]['fname'] 		- имя накладываемого изображения (с путем)
// или
// $options['logo'][]['text'] 		- имя накладываемого текста
// $options['logo'][]['x']  	 	- смещение по X
// $options['logo'][]['y']	 		- смещение по Y
// $options['logo'][]['transparent']- прозрачность, 1...100
// $options['logo'][]['moment'] 	- на что накладывать логотип - на исходник ('before_resize') или на ресайс ('after_resize')

// *** опции вывода ****
// $options['output_fname']			- имя, куда сохранить изображение (с путем)

// при использовании функции необходимо обращать внимание на то, что

function working_image($fname,$options=array())
 {
    $res=0 ;   $trace=array() ;
    list($source,$type)=open_image($fname) ;
    //if (!$gd_info['GIF Create Support']) { echo '<span class=red>Внимание! Сервер не поддерживает создание GIF-изображений! Сохранение будет произведено в формате JPG</span>'; $type='jpg' ;}
	if ($source)
	{
         if (sizeof($options['logo'])) foreach($options['logo'] as $logo_inf) if ($logo_inf['moment']=='before_resize') image_add_logo($source,$logo_inf) ;

         if ($options['resize']) $trace['resize']=resize_image($source,$options['resize']) ;


         //damp_array($options) ;
         if ($options['filter']) $res=imagefilter($source,$options['filter']['name']) ;
         //if (!$res) echo 'Не удалось превратить в серый' ;

         if (sizeof($options['logo'])) foreach($options['logo'] as $logo_inf) if ($logo_inf['moment']!='before_resize') image_add_logo($source,$logo_inf) ;

  		 //damp_array($options) ;
  		 $res=put_image($source,$type,$options) ;
         if ($options['debug']) $trace['put']=$res ;

  		//ImageDestroy($im_convas);
  		ImageDestroy($source);

  		 if ($options['is_default']) { $dir=_DOT($options['tkey'])->dir_to_file ;
  		 							   $patch=_DOT($options['tkey'])->patch_to_file ;
  		 							   $fname=$options['output_fname'] ;
  		 							   $def_image=str_replace($dir,$patch,$fname) ;
  		 							   $str='ErrorDocument 404 '.$def_image ;
  		 							   $cnt=file_put_contents(dirname($fname).'/.htaccess',$str) ;
  		 							   if ($cnt) echo 'Изображение назначено используемым по умолчанию<br>' ;
  		 							   else echo 'Не удалось назначить изображение используемым по умолчанию<br>' ;
  		 							 }
    }
    return(($options['debug'])? $trace:$res) ;
 }

 // преобразуем изображение в заданный тип
 function convert_image($file_dir,$to_type)
 {
 	list($source,$type)=open_image($file_dir) ;
    if ($source)
    { $res=put_image($source,strtoupper($to_type),array('output_fname'=>$file_dir.'.'.$to_type)) ;
      if ($res) return($file_dir.'.'.$to_type) ; else return($file_dir) ;
    }


 }


 function open_image($fname)
 { 	//$size=round(filesize($fname)/1024) ;
    //if ($size>250) { echo '<strong>Превышен лимит размера файла:'.$fname.' = '.$size.' Кб </strong><br>' ;
	//		         return ;
	//		       }

    $id_type=exif_imagetype($fname) ;
    //echo 'Тип файла: '.$id_type.'<br>'  ;
    //echo 'Имя файла: '.$fname.'<br>'  ;


    /*if (stripos($fname,'.jpg')!==false) $type='jpg' ;
	if (stripos($fname,'.jpeg')!==false) $type='jpg' ;
	if (stripos($fname,'.gif')!==false) $type='GIF' ;
	if (stripos($fname,'.png')!==false) $type='PNG' ;
	if (stripos($fname,'.bmp')!==false) $type='BMP' ;
    */
    $func_name='' ; $source='' ;
    switch($id_type)
     { case 2 : $type='jpg' ; $func_name = 'imagecreatefromjpeg' ;  break ;
       case 1 : $type='gif' ; $func_name = 'imagecreatefromgif' ;   break ;
       case 3 : $type='png' ; $func_name = 'imagecreatefrompng' ;   break ;
       case 6 : $type='bmp' ; $func_name = 'imagecreatefromxbm' ;  break ;
       case 15: $type='bmp' ; $func_name = 'imagecreatefromwbmp' ;  break ;
     }
    if ($func_name!='') $source = $func_name($fname) ;
    if (!$source) echo '<br><span class="red">Не удалось открыть изображение средствами PHP ('.mime_content_type($fname).')</span> (exif_imagetype='.$id_type.',fucntion="'.$func_name.'").&nbsp;<span class="green bold">Сохраните изображение в другом формате и попробуйте еще раз.</span><br>' ;
    else return(array($source,$type)) ;
 }

 function put_image(&$image,$type,$options=array())
  { $gd_info=gd_info(); $trace=array() ;
    if ($options['output_fname']) 	{ $fname=$options['output_fname'] ;
    								  if (is_file($fname)) { unlink($fname) ; $trace[]='Удалили существующий файл '.$fname ; }
    								}
    if ($options['resize']['quality']) $quality=$options['resize']['quality'] ; else $quality=100 ;
    $trace[]='Качество изображения '.$quality ;

    //echo 'quality='.$options['quality'].'<br>' ;
    ob_start() ;  $trace[]='Тип файла на выходе: '.$type ;
    if (!$fname)
    { $content_type='' ;
      switch($type)
      { case 'jpg' : $content_type='image/jpeg';  break ;
        case 'gif' : $content_type='image/gif' ;  break ;
        case 'png' : $content_type='image/png' ;  break ;
        case 'bmp' : $content_type='image/wbmp' ;  break ;
      }
      if ($content_type)  { header('Content-type: '.$content_type); $trace[]='Отправлен заголовок Content-type: '.$content_type ; }
      else  $trace[]='Не опознан тип файла, заголовок не отправлен' ;
     }

    $res=0 ;
    switch($type)
     { case 'jpg' : $res=imagejpeg($image,$fname,$quality);  break ;
       case 'gif' : if ($gd_info['GIF Create Support']) $res=imagegif($image,$fname);  else { echo '<span class=red>Настройки сервера не поддерживают создание gif-файлов: '.$fname.'</span><br>' ; /*damp_array($gd_info) ; */} break ;
       case 'png' : $res=imagepng($image,$fname);  break ;
       case 'bmp' : $res=imagewbmp($image,$fname);  break ;
     }

    $img_cont=ob_get_clean() ;
    if (!$res) { echo 'Не удалось сохранить файл в '.$type.'<br>'  ;   $trace[]='Не удалось сохранить файл '.$type.' в '.$fname ; }
    else       { echo $img_cont ;                                      $trace[]='Файл сохранен: '.$fname ; }

    $size=(file_exists($fname))? filesize($fname):0 ;
    if ($size) $trace[]='Размер файла: '.$size ;
    //echo '_________________-name='.$fname ;
    return(($options['debug'])? $trace:$size) ;
  }


 function filter_image(&$source,$mode)
 {



 }

 // масштабирование изображения
 // по умолчанию - с полями, цвет задника - белый, без ресампла

 function resize_image(&$source,$options)
 { $trace=array() ;
   $options['fields']=($options['fields'])? 1:0 ;
   $options['back_color']=($options['back_color'])? $options['back_color']:'ffffff' ;
   $options['resampled']=($options['resampled'])? 1:0 ;

   $w1=imagesx($source)  ; $h1=imagesy($source) ;  $trace[]='Исходник: ширина: '.$w1.', высота: '.$h1 ;
   $w2=$options['width'] ; $h2=$options['height'] ;
   if ($w2) $trace[]='Клон: ширина: '.$w2 ;
   if ($h2) $trace[]='Клон: высота: '.$h2 ;
   if ($w2 or $h2) // масштабируем фото к размерам w2 и h2
 	{	$k_w=$w2/$w1 ; $k_h=$h2/$h1 ;
        $trace[]='Начальные коэфициенты масштабирования: k_w: '.$k_w.', k_h: '.$k_h ;

        // если не задана высота, то выбираем высоту пропорционально ширине
        if (!$h2) { $trace[]='Клон: высота не задана' ;
                    if ($w2<$w1) { $h2=$h1*$k_w ; $k_h=$k_w ; $trace[]='Ширина клона меньше исходника, ширина будет уменьшена на '.$k_w.', соответственно уменьшаем высоту: h='.$h2.', k_h='.$k_h ;  }
                    else         { $h2=$h1 ; $k_h=1 ; $trace[]='Ширина клона больше исходника, используем высоту источника: h='.$h2.', k_h='.$k_h ; }
                  }

 	    if (!$w2) { $trace[]='Клон: ширина не задана' ;  if ($h2<$h1) { $w2=$w1*$k_h ; $k_w=$k_h ; } else { $w2=$w1 ; $k_w=1 ; } }// если не задана ширина, то выбираем ширину пропорционально высоте

 	    $k1=$w1/$h1 ; $k2=$w2/$h2 ;

        $trace[]='Уточненные коэфициенты масштабирования: k_w: '.$k_w.', k_h: '.$k_h ;

	 	$dw=0 ; $dh=0 ; $w=0 ; $h=0 ;

        $im_convas  = imagecreatetruecolor($w2,$h2);
        imagefill($im_convas,0,0,hexdec($options['back_color']));

        if ($w1<=$w2 and $h1<=$h2) // если масштабиремое фото меньше конечного, то просто разместить его по центру изображения
        { $h1_new=$h1 ; $w1_new=$w1 ; $dw=($w2-$w1_new)/2 ;  $dh=($h2-$h1_new)/2 ; }
        else if (($options['fields'] and $k1<=$k2) or (!$options['fields'] and $k1>$k2)) // масштабируем с полями
        { $w1_new=$w1*$k_h ; $h1_new=$h2 ; $dw=($w2-$w1_new)/2 ; $dh=0 ; }
        else
        { $w1_new=$w2 ; $h1_new=$h1*$k_w ; $dw=0 ; $dh=($h2-$h1_new)/2 ; }

        if ($options['resampled'])
             imagecopyresampled($im_convas, $source,$dw, $dh, 0, 0,$w1_new,$h1_new,$w1,$h1);
        else imagecopyresized($im_convas, $source,$dw, $dh, 0, 0,$w1_new,$h1_new,$w1,$h1);

        $source=$im_convas ;
    }
    return($trace) ;
 }

 // добавление логотипа на рисунок
 // по умолчанию - непрозрачность - 100, x=0, y=0
 function image_add_logo(&$source,$logo_inf)
 {   //echo 'Добавляем лого<br>' ;
     //damp_array($logo_inf) ;
     if (!$logo_inf['x']) $logo_inf['x']=0 ;
     if (!$logo_inf['y']) $logo_inf['y']=0 ;
     if (!$logo_inf['transparent']) $logo_inf['transparent']=100 ;

	 if ($logo_inf['fname']) // накладываем логотип из файла
	   {
	      list($logo,$type)=open_image($logo_inf['fname']) ;
	      if (sizeof($logo))
	       { $w_l=imagesx($logo)  ; $h_l=imagesy($logo) ;
	         if (!imagecopymerge($source,$logo,$logo_inf['x'],$logo_inf['y'], 0, 0,$w_l,$h_l,$logo_inf['transparent'])) echo 'Не удалось наложить лого<br>';
	         ImageDestroy($logo);
	       }
	   }

	 if ($logo_inf['text']) // накладываем логотип из текста
	   {  // создаем фон изображения
  		  $w_l=$logo_inf['width']  ; $h_l=$logo_inf['height'] ; $angle=0 ; $font_name=$logo_inf['font_name'] ; $font_size=$logo_inf['font_size'] ;
  		  $x=$logo_inf['x'] ; $y=$logo_inf['y'] ;  $color=$logo_inf['color'] ;
  		  if (!file_exists(_DIR_TO_ROOT.'/'.$font_name)) { $msg='Не удалось открыть файл шрифта: '._DIR_TO_ROOT.'/'.$font_name ; trigger_error($msg) ; return ; }

  		  //$logo  = imagecreatetruecolor($w_l,$h_l);
  		  //imagefill($logo,0,0,hexdec($logo_inf['back']));

    	  imagefttext($source,$font_size,$angle,$x,$y,hexdec($color),_DIR_TO_ROOT.'/'.$font_name,$logo_inf['text']) ;

	      //if (!imagecopymerge($source,$logo,$logo_inf['x'],$logo_inf['y'], 0, 0,$w_l,$h_l,$logo_inf['transparent'])) echo 'Не удалось наложить лого<br>';
	      //ImageDestroy($logo);
	   }

	//return($source);
 }

 // добавляет к изображению рамку и информацию по изображению
 function image_add_info(&$source,$text,$options=array())
 { $w1=imagesx($source)  ; $h1=imagesy($source) ;
   $im_convas  = imagecreatetruecolor($w1+4,$h1+4+20);
   imagefill($im_convas,0,0,hexdec($options['back_color']));
   if (!imagecopymerge($im_convas,$source,2,2,0,0,$w1,$h1,100)) echo 'Не удалось наложить картинку<br>';
   ImageDestroy($source);
   $options['angle']=0 ;

   imagefttext($im_convas,$options['font_size'],$options['angle'],10,$h1+18,hexdec($options['color']),_DIR_TO_ROOT.'/'.$options['font_name'],$text) ;
   return($im_convas)  ;

 }



 function get_list_img_for_dir($dir_image)
 {
   $dir_list_images=scandir($dir_image) ;
   // чистим массив исходников от не-фоток
   if (sizeof($dir_list_images)) foreach ($dir_list_images as $indx=>$fname)
     if (!(stripos($fname,'.jpg')!==false or stripos($fname,'.gif')!==false or stripos($fname,'.png')!==false or stripos($fname,'.bmp')!==false)) unset($dir_list_images[$indx]) ;
   return($dir_list_images) ;
 }


  // проверить наличие каталога изображений для объектной таблицы
 function check_obj_table_image_dir($tkey)
 {
    // проверяем, существует ли каталог для хранения изображений
    // если нет, то создаем
    $show_img_pars=0 ;
    $cur_table=_DOT($tkey) ;
    if (sizeof($cur_table->list_clss)) foreach($cur_table->list_clss as $clss=>$clss_tkey)
      if (is_object(_CLSS($clss)) and _CLSS($clss)->has_parent_clss(3) and $cur_table->list_clss[$clss]==$tkey) $show_img_pars=1 ;

    if (_DOT($tkey)->dir_to_file and $show_img_pars)
    { echo "<br><strong>Проверка существования каталога изображений для '"._DOT($tkey)->table_name."'</strong><br>" ;
      $dir=_DOT($tkey)->dir_to_file ;
      $dir_small=$dir.'small/' ;
      $dir_source=$dir.'source/' ;
      echo $dir.": " ;       if (!is_dir($dir))          create_dir($dir) ;           else echo 'OK<br>'  ;
	  echo $dir_small.": " ; if (!is_dir($dir_small))    create_dir($dir_small) ;     else echo 'OK<br>'  ;
	  echo $dir_source.": " ;if (!is_dir($dir_source))   create_dir($dir_source) ;    else echo 'OK<br>'  ;
	      if (sizeof(_DOT($tkey)->list_clone_img())) foreach( _DOT($tkey)->list_clone_img() as $dir_clone=>$info)
	   { $dir_clone=$dir.$dir_clone.'/' ;
	     echo $dir_clone.': ' ; if (!is_dir($dir_clone)) create_dir($dir_clone) ;     else echo 'OK<br>'  ;
	          					 }
	  echo '<br>' ;
	}
 }

 function delete_clone_image($tkey,$fname)
 {

    $dir=_DOT($tkey)->dir_to_file ;
    //echo '<strong>Удаляем клоны изображения для '.$dir.$fname.'</strong><br>' ;
    if (sizeof(_DOT($tkey)->list_clone_img())) foreach(_DOT($tkey)->list_clone_img() as $rec)
    { if (is_file($dir.$rec['dir'].'/'.$fname))
 	  {  //echo 'Клон удален '.$dir.$rec['dir'].'/'.$fname.'<br>' ;
 	     unlink($dir.$rec['dir'].'/'.$fname) ;
 	  }
    }
 }

 // проверяем, возможно ли создание клонов для загруженного изображения
 function check_image_type($file_dir,$options=array())
 {
  	$file_type=get_image_type($file_dir) ;
    if ($file_type)
    { if ($options['view_upload']) echo 'Тип изображения: <span class="black bold">'.$file_type.'</span><br>' ;
	  if ($file_type=='GIF')
	  { if ($options['view_upload']) echo 'Изображение будет преобразовано в формать JPG<br>' ;
	    							 $file_dir=convert_image($file_dir,'jpg') ;
	  }
	  return($file_dir) ;
	}
	else echo $file_dir.' - <span class=red> - Неизвестный формат файла, создание клонов невозможно</span><br>' ;
 }

// генерация капчи
// вызываеттся только из /images/img_check_code.php
function generate_check_code_img($type=0)
{ session_start() ;
  if (!sizeof($_SESSION['check_code_img'])) // сессия умерла, надо по тихому перугрузить данные
   {  connect_database() ;   // законектиться к базе
       session_boot() ; // грузим сессию
       setting_setup() ; // настройка переменных после первой загрузки сесиии
   }
  if (!$type) $type=0;
  $img_params=$_SESSION['check_code_img'][$type] ; //damp_array($img_params) ;
  if (!sizeof($img_params)) { echo 'no_check_code_params' ; return ; }
  $_SESSION['check_code_number']='' ;
  // загружаем прозрачный фон изображения в качестве канвы
  if ($img_params['back_space'])
  { $im_convas = imagecreatefrompng(_DIR_TO_ROOT.$img_params['back_space']);
    imagealphablending( $im_convas, false );
    imagesavealpha($im_convas, true);
  }
  // или создаем канву стандартными свойствами - но прозрачность там будет черная независимо от imagealphablending и imagesavealpha
  else $im_convas  = imagecreatetruecolor($img_params['width'],$img_params['height']);

  if ($img_params['back']) imagefill($im_convas,0,0,hexdec($img_params['back']));
  elseif ($img_params['back_img'])
  {
      list($fon,$type)=open_image(_DIR_TO_ROOT.$img_params['back_img']) ;
      if ($type)
      { $w_l=imagesx($fon)  ; $h_l=imagesy($fon) ;
      	if (!imagecopymerge($im_convas,$fon,0,0,0,0,$w_l,$h_l,100)) echo 'Не удалось наложить лого<br>';
      	ImageDestroy($fon);
      }
  }
  $x=$img_params['x'] ;
  $y=$img_params['y'] ;
  $angle=0 ;
  $font_name=$img_params['font_name'] ; $font_size=$img_params['font_size'] ;
  if (strpos($font_name,'/')!=0) $font_name='/'.$font_name ;

  $step_x=$img_params['step_x'] ;

  $dev_angle=$img_params['dev_angle'] ;
  $dev_x=$img_params['dev_x'] ;
  $dev_y=$img_params['dev_y'] ;

  for ($i=0; $i<$img_params['digit_count']; $i++)
  { // получаем случайное число 0..9
    $numb=rand(0,9) ;
    $_SESSION['check_code_number'].=$numb ;
    // вносим девиацию в текущие координаты буквы
    if ($dev_x) $_x=round($x+rand(-$dev_x,$dev_x)) ; else $_x=$x ;
    if ($dev_y) $_y=round($y+rand(-$dev_y,$dev_y)) ; else $_y=$y ;
    if ($dev_angle) $_angle=round($angle+rand(-$dev_angle,$dev_angle)) ; else $_angle=$angle ;
    if ($_angle<0) $_angle=360+$_angle ;
    if (!$img_params['color']) { $color='' ; for($j=0;$j<6;$j++) $color.=rand(2,9) ; }
    else                         $color=$img_params['color'] ;
    if (!file_exists(_DIR_TO_ROOT.$font_name))
     { $msg='Не удалось открыть файл шрифта: '._DIR_TO_ROOT.'/'.$font_name ;
       trigger_error($msg) ;
       return ;
     }
    else imagefttext ($im_convas,$font_size,$_angle,$_x,$_y,hexdec($color),_DIR_TO_ROOT.'/'.$font_name,$numb) ;
    $x=$x+$step_x ; // расчет следующей точки
  }
  header('Content-type: image/png');
  imagepng($im_convas) ;
}

  // возвращает информацию по клонам изображения для изображения $obj_name из таблицы  tkey
  function get_clone_info($tkey,$fname)
  {
  	$text='' ;
  	if (sizeof(_DOT($tkey)->list_clone_img())) foreach (_DOT($tkey)->list_clone_img() as $clone_name=>$clone_info)
  	{  $dir=_DOT($tkey)->dir_to_file.$clone_name.'/' ;
  	   $text.=get_image_info($dir.$fname,$clone_info['resize']['width'].'x'.$clone_info['resize']['height']) ;
    }
    $text.=get_image_info(_DOT($tkey)->dir_to_file.'small/'.$fname,'100x100') ;
    return($text) ;
  }


  function get_img_reffers($tkey,$fname)
  {
    $patch=_DOT($tkey)->patch_to_file ;
    $dir=_DOT($tkey)->dir_to_file ;
  	$text='' ;
  	if (sizeof(_DOT($tkey)->list_clone_img())) foreach (_DOT($tkey)->list_clone_img() as $clone_name=>$clone_info)
  	{  //print_r($clone_info) ;
  	   if (file_exists($dir.$clone_name.'/'.$fname))
  	      $text.=$clone_info['resize']['width'].'x'.$clone_info['resize']['height'].' : '.$patch.$clone_name.'/'.$fname.'<br>' ;
  	   else
  	      $text.=$clone_info['resize']['width'].'x'.$clone_info['resize']['height'].' : файл не существует<br>' ;
    }
    if (file_exists($dir.'small/'.$fname)) $text.='100x100: '.$patch.'small/'.$fname.'<br>' ;
     else                                  $text.='100x100: файл не существует<br>' ;
    return($text) ;
  }

  function get_img_source($tkey,$fname)
  {
    $patch=_DOT($tkey)->patch_to_file ;
    $dir=_DOT($tkey)->dir_to_file ;
  	if (file_exists($dir.'/source/'.$fname))  $text='<a href="'.$patch.'/source/'.$fname.'">'.$patch.'/source/'.$fname.'</a><br>' ;
  	else $text='файл не существует<br>' ;
    return($text) ;
  }


?>