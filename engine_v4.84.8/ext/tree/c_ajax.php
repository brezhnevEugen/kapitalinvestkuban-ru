<?
include_once(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
    function get_panel_filter()
    {  $this->success='modal_window' ;
       $this->modal_panel_title='Фильтр' ;
       $this->modal_panel_width='150' ;
       $this->modal_panel_pos_x='"left"' ;
       $this->modal_panel_pos_y='["bottom", "outside"]' ;
       $this->modal_panel_target='go_filter' ;
       $this->no_close_button=1 ;
       include('panel_tree_filter.php') ;
       panel_tree_filter() ;
    }

    function set_filter()
    {   global $TM_goods ;
        $options=array() ;
        $tkey=$TM_goods ;

        if (!$options['on_click_script'])  $options['on_click_script']='fra_viewer.php' ;
        if (!$options['on_expand_script']) $options['on_expand_script']='ajax.php' ;
        if (!$options['on_expand_cmd'])    $options['on_expand_cmd']='upload_object_tree_items' ;
        if (!$options['usl_root']) $options['usl_root']='parent=0' ;
        $root=execSQL_van('select pkey,clss,obj_name,enabled,1 as cnt from '._DOT($tkey)->table_name.' where  '.$options['usl_root']) ;
        if (!$root['clss']) echo 'alert("В таблице не найден корень дерева")' ;

        $options['debug']=0 ;
        $options['order_by']=_DEFAULT_TREE_ORDER;
        //$options['use_page_view_sort_setting']=0 ; // используем настройки сортировки редактора объектов, если они таб быти определееы
        $options['no_image']=1 ; //информация по фото не нужна
        $options['fill_count_childs']=1 ; // получаем кол-во дочерних объектов
        $list_obj=select_db_obj_child_recs($root['_reffer'],$options) ;  $list_clss=array() ;
        ?><ul class="tree_root" on_click_script=<?echo $options['on_click_script']?> on_expand_script=<?echo $options['on_expand_script']?> on_expand_cmd=<?echo $options['on_expand_cmd']?> <?if ($options['no_show_cnt'])  echo 'no_show_cnt'?>>
             <li clss="<?echo $root['clss']?>" reffer="<?echo $root['_reffer']?>"><span class=name><?echo $root['obj_name']?></span><span class=cnt></span></li>
             <ul><? if (sizeof($list_obj)) foreach ($list_obj as $tkey_arr) if (sizeof($tkey_arr)) foreach ($tkey_arr as $clss=>$recs) { _CLSS($clss)->print_list_recs_to_tree($recs) ; $list_clss[]=$clss ; }?></ul>
          </ul>
        <?
        generate_clss_style_icon($list_clss) ;
    }

}


