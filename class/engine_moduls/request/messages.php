<?
// !!! внимание файл должен подключаеться отдельно в каждом расширении

function request_message_create_success($options=array())
{   ?>   <p>Уважаемый(ая) <strong><?echo $options['autor_name']?></strong>!<br><br>
		  Ваше сообщение получено и будет рассмотрено в ближайшее время.<br> В рабочее время это происходит обычно в течении трех часов.<p>
	      <p class='gold bold'>С уважением, администрация сайта <strong><?echo _MAIN_DOMAIN?></strong></p>
    <?
    return(1) ; // не показывать форму сообщения
}

function request_message($text)
{?><p class=alert><?echo $text?></p><br><?}

function request_message_error_field()
{?><p class=alert>Вы заполнили  не все необходмые поля. Пожалуйста, попробуйте еще раз.</p><br><?}

function request_message_db_error()
{?><p class=alert>Создание запроса временно заблокировано по техническим причинам. Попробуйте, пожалуйста, повторить попытку спустя некоторое время</p><br><?}

function request_message_error_check_code()
{?><p class=alert>Вы неправильно ввели цифровой проверочный код. Пожалуйста, попробуйте еще раз.</p><br><?}

function request_message_default()
{
 ?><p>Вы можете сделать запрос на представленные на нашем сайте услуги.</p>
   <p>Для того, чтобы мы могли помочь Вам, заполните приведенную ниже форму, и мы в кратчайшие сроки перезвоним Вам.</p>
 <?
}

?>