<?php
$__functions['init'][]		='_landing_admin_vars' ;
$__functions['install'][]	='_landing_install_modul' ;
$__functions['boot_admin'][]='_landing_admin_boot' ;


function _landing_admin_vars() //
{
    $_SESSION['descr_clss'][1]['list']['field']['code']				='ШАБЛОН'  ;

    // 7 - информация
    $_SESSION['descr_clss'][7]=array() ;
	$_SESSION['descr_clss'][7]['name']='Информация' ;
	$_SESSION['descr_clss'][7]['parent']=0 ;
	$_SESSION['descr_clss'][7]['fields']=array('value'=>'text','code'=>'varchar(32)','is_HTML'=>'int(1)') ;
	$_SESSION['descr_clss'][7]['parent_to']=array(3) ;

    $_SESSION['descr_clss'][7]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][7]['list']['field']['enabled']					='Сост.' ;
	$_SESSION['descr_clss'][7]['list']['field']['pkey']						='ID' ;
	$_SESSION['descr_clss'][7]['list']['field']['code']						='Код блока<br>в шаблоне' ;
	$_SESSION['descr_clss'][7]['list']['field']['obj_name']					='Наименование' ;
	$_SESSION['descr_clss'][7]['list']['field']['is_HTML']	                =array('title'=>'HTML')  ;
    $_SESSION['descr_clss'][7]['list']['field']['value']					=array('title'=>'Содержание','td_class'=>'justify','class'=>'big','use_HTML_editor'=>1) ;

	$_SESSION['descr_clss'][7]['details']=$_SESSION['descr_clss'][7]['list'] ;
	$_SESSION['descr_clss'][7]['details']['field']['value']					=array('title'=>'Текст','td_class'=>'justify','class'=>'big','no_generate_element'=>1) ;
    $_SESSION['descr_clss'][7]['details']['field'][]					    =array('title'=>'Код php','edit_element'=>'clss_7_code','td_class'=>'justify') ;
	




	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------

	$_SESSION['menu_admin_site']['Модули']['landing']=array('name'=>'Лендлинг','href'=>'editor_landing.php','icon'=>'menu/newspaper.jpg') ;
}

function _landing_admin_boot($options)
{   $options['no_create_model']=1 ;
	create_system_modul_obj('landing',$options) ;
}


function _landing_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Лендлинг сайт</strong></h2>' ;

    $file_items=array() ;
    $file_items['editor_landing.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_landing.php']['options']['system']='"landing_system"' ;
    $file_items['editor_landing.php']['options']['title']='"Лендлинг сайт"' ;
    $file_items['editor_landing.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;

    $pattern=array() ;
	$pattern['table_title']				= 	'Лендинг сайт' ;
	$pattern['use_clss']				= 	'1,7,21,20' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>1,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Лендлинг сайт') ;
   	$pattern['table_name']				= 	$_SESSION['TM_landing'] ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_parent']			= 	$DOT_root;
   	create_table_by_pattern($pattern) ;

    SETUP_get_img('/landing/admin_img_menu/newspaper.jpg',_PATH_TO_ADMIN.'/img/menu/newspaper.jpg') ;
}



?>