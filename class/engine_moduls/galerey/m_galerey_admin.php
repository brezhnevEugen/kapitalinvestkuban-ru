<?php
$__functions['init'][]		='_galerey_admin_vars' ;
$__functions['install'][]	='_galerey_install_modul' ;

function _galerey_admin_vars() //
{
	//-----------------------------------------------------------------------------------------------------------------------------
	// описание переменных модуля
	//-----------------------------------------------------------------------------------------------------------------------------

	//-----------------------------------------------------------------------------------------------------------------------------
	// описание классов модуля
	//-----------------------------------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------------------------------
	// описание пунктов меню админа сайта
	//-----------------------------------------------------------------------------------------------------------------------
	$_SESSION['menu_admin_site']['Модули']['galerey']=array('name'=>'Галерея','href'=>'editor_galerey.php') ;


  $_SESSION['descr_clss'][1]['list']['field']['pkey']				='Код' ;
  $_SESSION['descr_clss'][1]['list']['field']['enabled']			='Сост.' ;
  $_SESSION['descr_clss'][1]['list']['field']['indx']				='Позиция'  ;
  $_SESSION['descr_clss'][1]['list']['field']['obj_name']			=array('title'=>'Наименование','edit_element'=>'input','class'=>'big','td_class'=>'left') ;

}

function _galerey_install_modul($DOT_root)
{   echo '<h2>Инсталируем модуль <strong>Галерея изображений</strong></h2>' ;

    $file_items=array() ;
    // editor_galerey.php
    $file_items['editor_galerey.php']['include'][]='_DIR_TO_ENGINE."/admin/c_site.php"' ;
    $file_items['editor_galerey.php']['options']['use_table_code']='"TM_galerey"' ;
    $file_items['editor_galerey.php']['options']['title']='"Галереи изображений"' ;
    $file_items['editor_galerey.php']['options']['use_class']='"c_editor_obj"' ;
    create_menu_item($file_items) ;

    // таблица фоток
	global $TM_galerey ;
	$pattern['table_title']				= 	'Галерея' ;
	$pattern['use_clss']				=	'1' ;
	$pattern['def_recs'][]				=	array ('parent'=>0,	'clss'=>10,	'enabled'=>1,	'indx'=>0,	'obj_name'=>'Галерея') ;
   	$pattern['table_name']				= 	$TM_galerey ;
	$pattern['table_clss']				= 	101 ;
	$pattern['table_mode']				= 	'main' ;
	$pattern['table_parent']			= 	$DOT_root;
    $id_DOT=create_table_by_pattern($pattern) ;

    global $TM_galerey_image ;
    $pattern=array() ;
    $pattern['table_title']				= 	'Изображения' ;
    $pattern['use_clss']				= 	'3,4' ;
    $pattern['table_name']				= 	$TM_galerey_image ;
    $pattern['table_clss']				= 	101 ;
    $pattern['table_parent']			= 	$id_DOT;
    create_table_by_pattern($pattern) ;

    SETUP_get_img('/galerey/admin_img_menu/galerey.jpg',_DIR_TO_ADMIN.'img/menu/galerey.jpg') ;

    create_dir('/galerey/') ;

    $file_items=array() ;
    $file_items['index.php']['options']['system']='"galerey_system"' ;
    $file_items['.htaccess']['text'][]='RewriteEngine On' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*)/(.*).html$ index.php[L]' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*).html$ index.php [L]' ;
    $file_items['.htaccess']['text'][]='RewriteRule ^(.*)/$ index.php [L]' ;
    create_script_to_page($file_items,_DIR_TO_ROOT.'/galerey/','../ini/patch.php') ;

    SETUP_get_file('/galerey/class/c_galerey.php','/class/c_galerey.php');
    $list_templates_file=analize_class_file_to_templates('/class/c_galerey.php') ;
    SETUP_get_templates('/galerey/',$list_templates_file) ;
}
?>