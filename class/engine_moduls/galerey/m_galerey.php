<?php

$__functions['init'][]		='_galerey_site_vars' ;
$__functions['boot_site'][]	='_galerey_site_boot' ;

function _galerey_site_vars() //
{
  $_SESSION['TM_galerey']='obj_'.SITE_CODE.'_galerey' ;
  $_SESSION['TM_galerey_image']='obj_'.SITE_CODE.'_galerey_image' ;
  $GLOBALS['TM_galerey_image']=&$_SESSION['TM_galerey_image'] ;

    // описание системы  -------------------------------------------------------------------------------------------------------------------
	$_SESSION['init_options']['galerey']['debug']=0 ;
	$_SESSION['init_options']['galerey']['patch_mode']='TREE_NAME' ;
	$_SESSION['init_options']['galerey']['root_dir']	='galerey' ;
  $_SESSION['init_options']['galerey']['usl_show_items']='clss in(3,4) and enabled=1' ;

    // определяем условия для формирование дерева
    $_SESSION['init_options']['galerey']['tree']['debug']=0 ;
    $_SESSION['init_options']['galerey']['tree']['usl_include']='clss in (1,10) and enabled=1' ;
	$_SESSION['init_options']['galerey']['tree']['include_space_section']=1 ;
    $_SESSION['init_options']['galerey']['tree']['order_by']='indx' ;
    $_SESSION['init_options']['galerey']['tree']['get_count_by_clss']=array('debug'=>0,'include_out_tables'=>1) ; // по умолчанию get_count_by_clss подсчитывает объекты только в основной таблице

    // набор шаблонов для резайса и наложения логотипа ---------------------------------------------------------------------------------
   	//global $img_pattern ; 	$img_pattern['Галерея'][250]['resize']=array('width'=>250,'fields'=>1,'resampled'=>1,'quality'=>100,'back_color'=>'ffffff');
}

function _galerey_site_boot($options)
{
	create_system_modul_obj('galerey',$options) ; // создание объекта galerey_system
}

class c_galerey_system extends c_system_catalog
{
 function c_galerey_system(&$create_options=array())
  {
	// устанавливаем значения опций по умолчанию, если они не были заданы ранее
    if (!$create_options['table_name']) 			$create_options['table_name']=$_SESSION['TM_galerey'];
    if (!$create_options['section_page_name']) 	    $create_options['section_page_name']='galerey' ; // названия для префиксной системы страниц
    if (!$create_options['item_page_name']) 	    $create_options['item_page_name']='galerey' ; // названия для префиксной системы страниц
    if (!$create_options['root_dir'])				$create_options['root_dir']='galerey' ;
    if (!$create_options['usl_show_items'])         $create_options['usl_show_items']='clss in (3,4) and enabled=1' ;
    if (!$create_options['item_table'])             $create_options['item_table']=$_SESSION['TM_galerey_image'] ;

    if (!$create_options['tree']['order_by'])	    $create_options['tree']['order_by']='indx' ;
    if (!$create_options['tree']['class_name'])		$create_options['tree']['class_name']='c_obj';

    parent::c_system_catalog($create_options) ;
  }
}

?>