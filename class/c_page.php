<?php
include (_DIR_EXT."/HTML/c_HTML.php") ;
class c_page extends c_HTML
{
  public $body_class ;

 function set_head_tags()
 { // настройки, определяющие HEADER страниц сайта
   $_SESSION['meta_info'][]=array('name'=>"mailru-domain",'content'=>"KctgpTx30hG0aSuV") ;
   $this->HEAD['favicon']='/favicon.ico'  ;

   // стандартный JQuery
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-1.8.3.min.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.timers-1.2.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery.maskedinput.min.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/jquery/jquery-ui-1.9.1.custom.min.js' ;

   // google maps
   $this->HEAD['js'][]='http://maps.google.com/maps/api/js?sensor=false' ;

   // mootools
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-core-1.3.1.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mootools/mootools-more-1.3.1.1.js' ;

   // библитека jBox + замена mBox
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/jBox-master/Source/jBox.min.js' ;
   $this->HEAD['css'][]='/assets/jBox.css' ;

   $this->HEAD['css'][]='/assets/mBoxModal.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Core.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Element.Select.js' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/mForm.Submit.js' ;
   $this->HEAD['css'][]='/assets/mForm.css' ;
   $this->HEAD['css'][]='/assets/mFormElement-Select.css' ; // _PATH_TO_EXT.'/mBox/StephanWagner-mForm-0.2.6/assets/mFormElement-Select.css' ;
   $this->HEAD['js'][]=_PATH_TO_EXT.'/mBox/modal_window.jBox.js' ;

   $this->HEAD['js'][]=_PATH_TO_EXT.'/highslide/highslide-4.1.13/highslide.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/highslide/highslide-4.1.13/highslide.css' ;


   //$this->HEAD['css'][]=_PATH_TO_EXT.'/request_price/style.css' ;

   //<link href='http://fonts.googleapis.com/css?family=Kelly+Slab&subset=latin,cyrillic' rel='stylesheet' type='text/css'>



//   калькулятор
   $this->HEAD['css'][]=_PATH_TO_EXT.'/calc/style.css';
   $this->HEAD['js'][]=_PATH_TO_EXT.'/calc/script.js';


   // файлы шаблона, потом необходимо перенести

   $this->HEAD['css'][]='/template/font/fonts.css';
   $this->HEAD['css'][]='/template/css/global.css';
//   $this->HEAD['css'][]='/template/js/libs/modernizr-2.6.1.min.js';
//   $this->HEAD['js'][]='/templates/jquery.maskedinput.min.js';
//   $this->HEAD['js'][]='/template/main.js';
   $this->HEAD['css'][]='http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css';


   //расширение - карусель банеров
   $this->HEAD['js'][]=_PATH_TO_EXT.'/carusel_item/script.js' ;
   $this->HEAD['css'][]=_PATH_TO_EXT.'/carusel_item/style.css' ;
   // стандартные файлы - должны подключаться последними

   $this->HEAD['css'][]='/images3/gothapro/style.css' ;
   $this->HEAD['css'][]='/images3/museo/style.css' ;
   $this->HEAD['css'][]='/images3/gotham-pro/style.css' ;
   //$this->HEAD['css'][]='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' ;

   $this->HEAD['js'][]='/script.js' ;
   //$this->HEAD['css'][]='http://fonts.googleapis.com/css?family=Kelly+Slab&subset=latin,cyrillic' ;
//   $this->HEAD['css'][]='/style.css' ;
//   $this->HEAD['LPtracker'][]='<noindex><script async src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTY4NjE7bHN0YXRzLnJlZmVyZXIoKX07dmFyIGpxdWVyeV9sc3RhdHM9ZnVuY3Rpb24oKXtqUXN0YXQubm9Db25mbGljdCgpO2xvYWRzY3JpcHQoInN0YXRzX2F1dG8uanMiLGluaXRfbHN0YXRzKX07bG9hZHNjcmlwdCgianF1ZXJ5LTEuMTAuMi5taW4uanMiLGpxdWVyeV9sc3RhdHMp"></script></noindex>';
   $this->HEAD['LPtracker'][]='<noindex><script async src="//track.soctracker.ru/?id=MTA3ZGVmMjAxZDYwYzA1YWE0ZDE2MzJlMDYxNDIzMjN8ODE0"></script></noindex>';


 }

// function local_page_head()
// {
//
//
// }


 // ----------------------------------------------------------------------------------------------------------------------------
 // структура сайта
 // ----------------------------------------------------------------------------------------------------------------------------

  function body()
    { global $landing_system ;
      $recs=execSQL('select * from obj_site_landing where enabled=1 and parent>0') ;
      if (sizeof($recs)) foreach($recs as $i=>$rec)
      {  $section_code=$landing_system->tree[$rec['parent']]->rec['code'] ;
         if ($rec['clss']==7) $data[$section_code][$rec['code']]=$rec['value'] ;
         if ($rec['clss']==1) $data[$section_code][$rec['code']]=$rec['pkey'] ;
      }
      ?><body class="<?echo $this->body_class?>"><?
      $this->{'block_header'}($data['header']) ;
      $this->block_main_menu() ; // Главное меню

      ?><div id="block_main">
        <div class="inner">
          <?$this->block_main() ; ?></div>
      </div><?
      $this->{'block_12'}($data['12']) ;
      $this->{'block_footer'}($data['footer']) ;
      ?>
      <link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
      <script type="text/javascript" src="https://cdn.callbackkiller.com/widget/cbk.js?cbk_code=20256" charset="UTF-8" async></script>

      </body><?
       ;
    }


 // ----------------------------------------------------------------------------------------------------------------------------
 // СТРУКТУРА БЛОКОВ САЙТА
 // ----------------------------------------------------------------------------------------------------------------------------

  function block_main()
  { global $galerey_system;
    

    $this->page_title() ; // стандартный заголовок страницы
    $_SESSION['pages_system']->show_page_content($GLOBALS['obj_info']) ;
    ?><div id="baner_galerey">
    <div id="project_content"><?$galerey_system->show_list_items('parent='.$GLOBALS['obj_info']['template'].' and enabled=1' ,'galerey/list_images',array('id'=>'galerey_carusel','clone'=>190,'count'=>12));?> </div>
    <script type="text/javascript">
      $j('div#project_content div#galerey_carusel').carusel_item({'speed' : '500','step' : '197','interval' : 2500,'autostart' : 1}) ;
    </script>


  </div><?
  }

  function panel_path() {}
  function block_main_menu()
  { $recs=execSQL('select obj_name,href from obj_site_pages where clss=26 and enabled=1 and on_use_menu=1 order by indx') ;?>
    <div id="block_main_menu" class="default">
      <div class="inner"><?
        include_once (_DIR_EXT.'/menu/list_items_menu_a_name_top.php');
        list_items_menu_a_name_top ($recs);?>
      </div>
    </div>
    <script type="text/javascript">
      $j(document).ready(function(){
        var $menu = $j("#block_main_menu");
        $j(window).scroll(function(){
            if ( $j(this).scrollTop() > 540 && $menu.hasClass("default") ){
                $menu.removeClass("default").addClass("fixed");
            } else if($j(this).scrollTop() <= 540 && $menu.hasClass("fixed")) {
                $menu.removeClass("fixed").addClass("default");
            }
        });
      });
    </script> <?
  }



 // ----------------------------------------------------------------------------------------------------------------------------
 // ШАПКА САЙТА
 // ----------------------------------------------------------------------------------------------------------------------------
  function block_header($data)
    { global $landing_system ;

      ?>

      <div id="block_header" class="page_section" style="background:url('<?echo img_clone($landing_system->tree[93],'source');?>') no-repeat center -190px">
        <div class="contain">
          <p class="rectangle1"><a href="/"><?echo $data['rectangle1']?></a> </p>
          <p class="action"><?echo $data['action']?></p>
          <span class="snoska"><?echo $data['snoska']?></span>
             <div class="call-him"><p class="p-register1"> <?echo $data['p-register1']?></p>
               <p class="number"> <span class="lptracker_phone"> <?echo $data['number']?></span></p>
               <p class="number_2"><span>  <?echo $data['number_2']?></span></p>
               <p class="p-register"> <?echo $data['p-register2']?></p>
<!--               <span class="link-to-modal">--><?//echo $data['link-to-modal']?><!--</span>-->
               <a href="<?echo $data['vk']?>"><img src="/public/landing/source/151_vk_logo_small_blue.png"/></a>
             </div>

             <div class="fform">
               <p>ОСТАВЬТЕ ЗАЯВКУ</p>
               <p>НА КОНСУЛЬТАЦИЮ</p>
               <div class="panel_request_header">
                 <form class="s_form">
                   <input class="s_text nameinp sinp" type="text" name="name" placeholder="Имя">
                   <input class="s_text nameinp sinp" type="text" name="phone" data-required placeholder="Напишите ваш телефон" mask="+7 (999) 999-99-99">
                   <input class="s_text adress" type="text" name="email" data-validate="email" placeholder="Напишите ваш e-mail">
                   <input type="submit" class="v2 s_submit" value="ОСТАВИТЬ ЗАЯВКУ" cmd="request/request_service" validate="form" theme="Заявка на консультацию" validate="form">

               </form>
               </div>
             </div>
           </div><?

        ?>
      </div>
      <?
    }

 // ----------------------------------------------------------------------------------------------------------------------------
 // ПОДВАЛ САЙТА
 // ----------------------------------------------------------------------------------------------------------------------------

  function block_12($data)
  { ?>
    <a name="contact"  class="anchor"></a>
    <div id="block_12" class="page_section">
        <div id="map_canvas"></div>
        <div class="inner">
            <div id="panel_12_01">
                <h2><?echo $data['h2']?></h2>
                <div><?echo $data['contact']?></div>
            </div>
      </div>
    </div>
      <style type="text/css"></style>
      <script type="text/javascript">
          $j(document).ready(function()
          {   var myLatlng = new google.maps.LatLng(<?echo $data['map_center']?>);
              var myOptions = {
                  zoom: 16,
                  center: myLatlng,
                  panControl: false,
                  zoomControl: false,
                  scaleControl: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              var map = new google.maps.Map($j("#map_canvas")[0], myOptions);
              var image = 'beachflag.png';
              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  title:"<?echo _MAIN_DOMAIN?>"
              });

          }) ;
      </script>
      <?
  }

  function block_footer($data)
   { ?>
 		<div id="block_footer" class="page_section">
 			<div class=inner>
 				<div id="panel_14_01"><?echo $data['inn']?></div>
 				<div id="panel_14_02"><?echo $data['copy']?></div>
 				<div id="panel_14_03"><?echo $data['studio']?></div>
 				<div id="ya" class="counter "><?echo $data['ya']?>
 				<div id="ya_1" class="counter "><?echo $data['ya_1']?>
        </div>
 				<div id="gg" class="counter "><?echo $data['gg']?></div>
 			</div>
 		</div>
    <?
   }

}