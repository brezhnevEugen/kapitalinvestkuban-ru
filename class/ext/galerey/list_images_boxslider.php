<?
  // шаблон - список статей: наименование-ссылка
  //
  function list_images_boxslider(&$list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;?>
    <script type="text/javascript">
          $j(document).ready(function(){
            $j('#<?echo $options['id'];?>').bxSlider({
          		mode: 'horizontal',
          		randomStart: false,
          		minSlides: 1,
          		maxSlides: 1,
          		slideWidth: 0,
          		slideMargin: 0,
          		adaptiveHeight: false,
          		adaptiveHeightSpeed: 500,
          		//ticker: ,
          		//tickerHover: ,
          		speed: 500,
          		controls: true,
          		auto: false,
          		autoControls: false,
          		pause: 3000,
          		autoDelay: 0,
          		autoHover: false,
          		pager: true,
          		pagerType: 'full',
          		pagerShortSeparator: '/'
          	});
          });
          </script>
    <ul class="list_images_bxslider" <?echo $id?>><?
      if (sizeof($list_recs)>0) foreach($list_recs as $rec)
      { ?><li class="Slideshow"><img class=main  width="930" height="623" src="<?echo img_clone($rec,930)?>" alt="" border="0" title=""  ></li><?
      }?>
    </ul>
    <div class=clear></div>

    <?
  }
?>