<?
  // шаблон - список статей: наименование-ссылка
  //
  function list_images_flexslider(&$list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
    $pos_row=$options['position'];
    $group_id   =($options['group_id'])?    $options['group_id']    :rand(1,10000) ;
    $clone_big  =($options['clone_big'])?   $options['clone_big']   :'source' ;
    $clone_small=($options['clone_small'])? $options['clone_small'] :'tumb_rewiew' ;
    $clone_main =($options['clone_main'])?  $options['clone_main']  :'main_review' ;?>
    <div class="list_images_flexslider flexslider" <?echo $id?>>
      <ul class="slides"><?
        list($id,$rec)=each($list_recs);
        $i=0;
        if (sizeof($list_recs)>0) foreach($list_recs as $rec)
        { $i++;
          $class=array() ;
          if ($i==1) $class[]='first' ;
          if ($i==sizeof($list_recs)) $class[]='last' ;
          if (sizeof($class)) $class='class="'.implode(' ',$class).'"' ; else $class="" ; ?>
          <li data-thumb="<?echo img_clone($rec,'small')?>" class="Slideshow">     <?
            if ($pos_row==1 & $i==1)
            { ?><img class=main src="<?echo img_clone($rec,$clone_small)?>" width="930" height="623" alt="" border="0" title=""  onload="slideLoaded(this)"> <?}
            else { ?><img class=main  width="930" height="623" src="<?echo img_clone($rec,$clone_small)?>" alt="" border="0" title=""  onload="slideLoaded(this)"> <?}?>
          </li><?
        }?>
      </ul>
      <div class=clear></div>
      <script type="text/javascript">

          function slideLoaded(img){
              var $img = $j(img),
                  $slideWrapper = $img.parent(),
                  total = $slideWrapper.find('img').length,
                  percentLoaded = null;

              $img.addClass('loaded');

              var loaded = $slideWrapper.find('.loaded').length;

              if(loaded == total){
                  percentLoaded = 100;
                  // INSTANTIATE PLUGIN
                $j(window).load(function() {
                        $j('.flexslider').flexslider(
                        {
                          animation: "fade",              //Выбор типа анимации (fade/slide)
                          slideshow: false,                //Включание автопроигрывания слайдшоу (true/false)
                          slideshowSpeed: 7000,           //Установка скорости переключения слайдов в слайдшоу, в миллисекундах
                          animationDuration: 500,         //Скорость выполнения анимации, в миллисекундах
                          directionNav: true,             //Включение навигации предыдущий/следующий (true/false)
                          controlNav: true,               //Включение постраничной навигации (true/false)
                          keyboardNav: true,              //Включение навигации с использованием клавиатуры(true/false)
                          prevText: "",           //Текст для пункта "предыдущий" directionNav
                          nextText: "",               //Текст для пункта "следующий" directionNav
                          slideToStart: 0,                //Слайд, с которого начинается слайдшоу. Нумерация слайдов по правилу массива(0 = первый слайд)
                          pauseOnAction: true,            //Включение паузы сладшоу при взаимоодействии с управляющим элементом (true/false)
                          pauseOnHover: false,            //Включение паузы слайдшоу при наведении курсора мыши на слайд, затем слайдшоу продолжается (true/false)
                          controlsContainer: ""           //Дополнительное свойство: можно указать контейнер для элементов навигации. По умолчанию используется контейнер слайдера. Если указанный элемент не будет найден, используется значение по умолчанию.
                        });
                      });
              } else {
                  // TRACK PROGRESS
                  percentLoaded = loaded/total * 100;
              };
          };

        </script>
      <script type="text/javascript" charset="utf-8">

      </script>
    </div>
    <?
  }
?>