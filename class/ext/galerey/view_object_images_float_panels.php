<?
  // алгоритм показа фото объекта:
  // клик по большому фото показывает исходник фото в всплывающем окне
  // клик по малому фото показывает большой клон фото на месте большого фото
  // дальшейний клик по большому фото приводит к показу нового исходника фото
  // что важно для работы этого алгоритма:
  //  - клон, который будет показываться на месте большого фото задается в переменной $big_clone
  //  - названия классов и id ссылок, в которые завернуты большое изображение и превьюшики менять нельзя!!!
  //    у большого фото class=main_img
  //	у превьюшек 	class=small_img

  function view_object_images_float_panels(&$rec,$options=array())
  {
	if (!$rec['_image_name']) return ;
	$big_clone=$options['big_img_clone'] ;
    ?><div class="panel_images"><?
	 if (sizeof($rec['obj_clss_3'])>1)
  	  	{ ?><div class="panel_small_img "><?
  	  	 	  foreach($rec['obj_clss_3'] as $rec_img)
  	  	  	  {  $source_info=img_source_info($rec_img) ;
  	  	  	     $big_clone_info=img_source_info($rec_img,$big_clone) ;
  	  	  	     //damp_array($source_info) ;
  	  	  	  	 ?><a class='small_img' id=<?echo $source_info['id']?> href='<?echo img_clone($rec_img,'source')?>'>
  	  	  	  	 	<img src='<?echo img_clone($rec_img,'small')?>' " alt=''>
  	  	  	  	 	<input type="hidden" id="<?echo $big_clone_info['id']?>" class=inf value="<?echo img_clone($rec_img,$big_clone)?>">
  	  	  	  	   </a>
  	  	  	  	 <?
  	  	  	  }
		  ?></div><?
		}
	$source_info=img_source_info($rec) ;
	?><a id=<?echo $source_info['id']?> class='main_img' href='<?echo img_clone($rec)?>'><img class='main_img' src='<?echo img_clone($rec,$big_clone)?>'  alt=''/></a>
	  <div class=clear></div>
    </div><?

    // код JS + JQery для обработки кликов по изображению
	?><div id="dialog_img"></div>
          <script>
            // назначаем обработчики событий
            $j('a.main_img').click(on_click_main_img) ;
            $j('a.small_img').click(on_click_small_img) ;

			// задаем параметры всплывающего окна
 			$j("#dialog_img").dialog({autoOpen: false,bgiframe: true,closeOnEscape: true,title: "Просмотр фото",position: ["center", "center"],draggable: true,resizable: true,modal: true,show: null,hide: null,buttons: {"Закрыть": function() {$j(this).dialog("close");}}});

            // клик по большому фото - должно показатся исходник изображения
            function on_click_main_img()
            {   var img_info=$j(this).attr('id') ;
                var arr_img_info=img_info.split('_') ;
                $j('div#dialog_img').html('<img src="'+this+'" alt="" width='+arr_img_info[1]+' height='+arr_img_info[2]+' border="0">') ;
            	$j('div#dialog_img').dialog('option','width',parseInt(arr_img_info[1])+30) ;
            	$j('div#dialog_img').dialog('option','height',parseInt(arr_img_info[2])+30) ;
                var title=$j('h1').html() ;
                $j('div#dialog_img').dialog('option','title',title) ;

            	$j("#dialog_img").dialog("open"); // Открыть окно
            	return(false);
            }

            // клик по малому фоту - должно заменить собой большое фото
            function on_click_small_img()
            { var img_id=$j(this).attr('id') ;
              var inp_name='a#'+img_id+' input.inf' ; // в этом скрытом элементе содержится адрес клона фото для большого изображения
              var big_img_src=$j(inp_name).attr('value') ; // получаем адрес клона для большого изображения
              var big_img_info=$j(inp_name).attr('id') ; // получаем размеры клона для большого изображения
              var arr_big_img_info=big_img_info.split('_') ;
              $j('a.main_img').attr('id',img_id) ; // замещаем в главной ссылке инфу по исх. изображению (код_ширина_высота)
              $j('a.main_img').attr('href',this)  ; // замещаем в главной ссылке адрес исх.изображения
              $j('a.main_img img').attr('src',big_img_src)  ; // замещаем в главной ссылке адрес исх.изображения
              $j('a.main_img img').attr('width',arr_big_img_info[1])  ; // устанавливаем новую ширину
              $j('a.main_img img').attr('height',arr_big_img_info[2])  ; // устанавливаем новую высоту
              return(false) ;
            }
          </script>
    <?
  }
?>