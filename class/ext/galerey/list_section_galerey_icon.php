<?
  // шаблон - список подразделов с наименованиями и кратким перечнем последних новостей каждого подраздела
  function list_section_galerey_icon(&$list_recs,$options=array())
  {
    $id=($options['id'])? 'id='.$options['id']:'' ;
    $clone=($options['clone'])? $options['clone']:'small' ;
    $go_text=($options['go_text'])? $options['go_text']:'Перейти в раздел' ;
    ?><div class=list_section_galerey_icon <?echo $id?>><?
      $i=1;
      if (sizeof($list_recs)) foreach($list_recs as $rec)
      {   $class='';
          if ($i==1) {$class='first' ;}
          if ($i % 2 == 0) {$class='last' ; $i=0;}
          ?><div class="item <?echo $class?>">
            <a href="<? echo $rec['__href'];?>">
              <img src="<? echo img_clone($rec,$clone) ?>" alt="<?echo $rec['__name']?>" />
              <span class=name><? echo $rec['__name'];?></span>
            </a>
          </div><?   $i++;
      }
  	?><div class=clear></div></div><?
  }

?>