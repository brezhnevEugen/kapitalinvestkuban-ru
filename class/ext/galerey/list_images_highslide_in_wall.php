<?
  // шаблон - список статей: наименование-ссылка
  function list_images_highslide_in_wall(&$list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
    $clone=($options['clone'])? $options['clone']:'source' ;
//      damp_array($list_recs,1,-1);
    if(sizeof($list_recs)>=5)
    {?>
        <script type="text/javascript">
              $(function(){
                $('#<?echo $options['id'];?>').bxSlider({
                  displaySlideQty: 5,
                  moveSlideQty: 1,
                  auto: true,
                  prevText:'назад',
                  nextText:'вперед',
                  hideControlOnEnd:true,
                  pager:true,
                  speed:1000,
                  pause:5000


                });
              });
        </script><?
    }?>
    <div class=galerey_wrap >
        <ul class="highslide-gallery" <?echo $id?>><?
        $ii=1;
  	    if (sizeof($list_recs)) foreach($list_recs as $i=>$rec)
  		{ $class=array() ;
  		  if (!$i) $class[]='first' ;
  		  if ($ii % 4 == 0) $class[]='last_in_line' ;
  		  if ($i==sizeof($list_recs)) $class[]='last' ;
  		  if (sizeof($class)) $class='class="'.implode(' ',$class).'"' ; else $class="" ;
  		  if (!$options['no_float_big_img']) {?><li <?echo $class?>><a href="<?echo img_clone($rec,800)?>" class="highslide" title="<?echo $rec['manual']?>" onclick="return hs.expand(this)"><img src="<?echo img_clone($rec,$clone)?>" alt="Highslide" border="0" title="Кликните мышкой для увеличения"></a></li><?}
  		  else {?><li <?echo $class?>><img  src="<?echo img_clone($rec,$clone)?>" class="highslide" border="0"></li><?}
            $ii++;
  		}?>
        <div class="clear"></div>
        </ul>
    </div><?
  }
?>