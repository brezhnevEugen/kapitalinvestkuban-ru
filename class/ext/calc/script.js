$j(document).ready(function () {
	
	$j(".onlynumber").live("keydown", function (e) {
		if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 13 ||
			(e.keyCode == 65 && e.ctrlKey === true) ||
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		} else {
			if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		}
	});
	
	$j(".parameter ul li").click(function(){
		$j(this).parent().find("li").removeClass("active");
		$j(this).addClass("active");
		calculate();
	});
	
	$j("#calk_meter").blur(function(){
		calculate();
	});
	
	if($j("#calk_meter").size() > 0)	{
		_msg = "";
		$j(".parameter").each(function(){
			_msg += $j(this).attr("data-msg") + "<br />";
		});
		$j("#msg div").html(_msg);
	}	
	
	function calculate(){
		nodata = false;
		_msg = "";
		if( $j("#calk_meter").val() < 80 )	{
			$j("#calk_meter").addClass("error")
			_msg += $j(".parameter:eq(0)").attr("data-msg") + "<br />";
			nodata = true;
		} else	{
			$j("#calk_meter").removeClass("error")
		}
		$j(".parameter:gt(0)").each(function(){
			num = $j(this).find("li.active").size();
			if(num == 0)	{
				_msg += $j(this).attr("data-msg") + "<br />";
				nodata = true;
			}
		});
		if(nodata)	{
			$j("#msg").css("display", "block");
			$j("#totalinfo").css("display", "none");
			$j("#msg div").html(_msg);
			$j("#total").html(0);
		}	else	{
			$j("#msg").css("display", "none");
			$j("#totalinfo").css("display", "block");
			
			_sum = 0;
			_text = "";
			$j(".parameter:gt(0)").each(function(){
				_sum += parseInt($j(this).find("li.active").attr("data-price"));
				_text += $j(this).find("li.active").find("div:last-child").html();
			});
			_sum *= parseInt($j("#calk_meter").val());
			_sum_metr=_sum/$j("#calk_meter").val();
			
			$j("#total").html( _sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") );
			$j("#total_metr").html( _sum_metr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") );
			$j("#totalinfo").html(_text);
		}
	}
	
	
});

