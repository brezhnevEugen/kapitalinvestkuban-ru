<?
include('messages.php') ;
include(_DIR_TO_ENGINE.'/c_page_XML_AJAX.php') ;
class c_page_ajax extends c_page_XML_AJAX
{
 function get_panel_request()
 {
   $options=array() ;
   switch($_POST['mode'])
   { case 'back_call':   $options=array('text_button'=>'Заказать звонок','comment'=>'Ваша заявка принята. В Ближайшее время менеджер с Вами свяжеться','theme'=>'Запрос обратного звонка') ;
                   $panel_name='panel_request' ;
                   break ;
     case 'sales': $options=array('text_button'=>'ПОЛУЧИТЬ СКИДКУ','comment'=>'Прайс будет отправлен на указанный вами E-MAIL','theme'=>'Запрос скидки') ;
                   $panel_name='panel_request_sales' ;
                   break ;
     case 'order': $options=array('text_button'=>'ОСТАВИТЬ ЗАЯВКУ','comment'=>'Менеджер перезвонит в течение 7 минут','theme'=>'Покупка','reffer'=>$_POST['reffer']) ;
                   $panel_name='panel_request' ;
                   break ;

   }
   if (sizeof($options))
   { include_once($panel_name.'.php') ;
     $panel_name($options) ;
     $this->add_element('show_modal_window','HTML',array('width'=>342)) ;
     //$this->add_element('title','Запрос коммерческого предложения') ;
   }

 }

 function request_service()
 { global $request_system,$goods_system ;
   save_form_values_to_session($_POST) ;
   //damp_array($_POST) ;
   //$rec=get_obj_info($_POST['reffer']) ;
   //$goods_system->prepare_public_info($rec) ;
   $request['autor_name']=$_POST['name'] ;
   $request['autor_email']=$_POST['email'] ;
   $request['autor_phone']=$_POST['phone'] ;
   $request['theme']=$_POST['theme'] ;

   if ($_POST['reffer'])
   {  $rec=get_obj_info($_POST['reffer']) ;
      //$request['value']['Товар']=$rec['obj_name'] ;
      $request['obj_reffer']=$_POST['reffer'] ;
      $request['obj_reffer_name']=$rec['obj_name'] ;
      //$request['obj_reffer_href']=$rec['__href'] ;
   }

   //$request['value']['Компания']=$_POST['company'] ;
   //$request['value']['Адрес']=$_POST['adres'] ;
   //$request['value']['Оборудование']='<a href="'.$rec['__href'].'">'.$rec['obj_name'].'</a>' ;
   //$request['value']['Комплектация товара']=$_POST['model'] ;
   //damp_array($request) ;
   $result=$request_system->create_message($request) ;
   $this->add_element('result_type',$result['type']) ;
   show_message_by_id('request_message',$result) ;

   // закрываем текущее модальное окно
   if ($result['type']!='error') $this->add_element('close_mBox_window','all') ;
   // формируем модальное окно
   //$this->add_element('title',$request['theme']) ;
   $this->add_element('show_modal_window','HTML',array('width'=>342)) ;

   if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

 function request_sales()
 { global $request_system,$goods_system ;
   save_form_values_to_session($_POST) ;
   //damp_array($_POST) ;
   //$rec=get_obj_info($_POST['reffer']) ;
   //$goods_system->prepare_public_info($rec) ;
   $request['autor_name']=$_POST['name'] ;
   $request['autor_email']=$_POST['email'] ;
   $request['autor_phone']=$_POST['phone'] ;
   $request['theme']='Запрос скидки' ;
   //$request['obj_reffer']=$_POST['reffer'] ;
   //$request['obj_reffer_name']=$rec['obj_name'] ;
   //$request['obj_reffer_href']=$rec['__href'] ;
   //$request['value']['Компания']=$_POST['company'] ;
   //$request['value']['Адрес']=$_POST['adres'] ;
   //$request['value']['Оборудование']='<a href="'.$rec['__href'].'">'.$rec['obj_name'].'</a>' ;
   //$request['value']['Комплектация товара']=$_POST['model'] ;
   //damp_array($request) ;
   $result=$request_system->create_message($request) ;
   $this->add_element('result_type',$result['type']) ;
   show_message_by_id('request_message',$result) ;

   // закрываем текущее модальное окно
   if ($result['type']!='error') $this->add_element('close_mBox_window','all') ;
   // формируем модальное окно
   //$this->add_element('title',$request['theme']) ;
   $this->add_element('show_modal_window','HTML',array('width'=>342)) ;

   if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

 // принимающая функция - вызывается в c_ajax.php
 function request_usluga()
 { global $request_system,$goods_system ;
   save_form_values_to_session($_POST) ;
   //damp_array($_POST) ;
   //$rec=get_obj_info($_POST['reffer']) ;
   //$goods_system->prepare_public_info($rec) ;
   $request['autor_name']=$_POST['name'] ;
   $request['autor_email']=$_POST['email'] ;
   $request['autor_phone']=$_POST['phone'] ;
   $request['theme']='Запрос продажи' ;
   //$request['obj_reffer']=$_POST['reffer'] ;
   //$request['obj_reffer_name']=$rec['obj_name'] ;
   //$request['obj_reffer_href']=$rec['__href'] ;
   //$request['value']['Компания']=$_POST['company'] ;
   $request['value']['Адрес']=$_POST['adres'] ;
   //$request['value']['Оборудование']='<a href="'.$rec['__href'].'">'.$rec['obj_name'].'</a>' ;
   $request['value']['Комплектация товара']=$_POST['model'] ;
   //damp_array($request) ;
   $result=$request_system->create_message($request) ;
   $this->add_element('result_type',$result['type']) ;
   show_message_by_id('request_message',$result) ;

   // закрываем текущее модальное окно
   if ($result['type']!='error') $this->add_element('close_mBox_window','get_panel_request_price') ;
   // формируем модальное окно
   $this->add_element('title','Заказ велоколяски') ;
   $this->add_element('success','modal_window') ;

   if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

 function get_panel_request_demo()
 { global $member ;
   $this->add_element('success','modal_window') ;
   $this->add_element('title','Запрос демонстрации оборудования') ;
   $this->add_element('no_close_button',1) ;
   include_once('panel_request_demo.php') ;
   panel_request_demo(array('reffer'=>$_POST['reffer'])) ;
 }

 // принимающая функция - вызывается в c_ajax.php
 function request_demo()
 { global $request_system,$goods_system ;
   save_form_values_to_session($_POST) ;
   $request['autor_name']=$_POST['name'] ;
   $request['autor_email']=$_POST['email'] ;
   $request['autor_phone']=$_POST['phone'] ;
   $request['theme']='Запрос демонстрации оборудования' ;
   $request['value']['Компания']=$_POST['company'] ;
   $request['value']['Адрес']=$_POST['adres'] ;
   //damp_array($request) ;
   $result=$request_system->create_message($request) ;
   $this->add_element('result_type',$result['type']) ;
   if ($result['type']!='error') $result['code']='request_demo_success' ;
   show_message_by_id('request_message',$result) ;

   // закрываем текущее модальное окно
   if ($result['type']!='error') $this->add_element('close_mBox_window','get_panel_request_demo') ;
   // формируем модальное окно
   $this->add_element('title','Запрос демонстрации оборудования') ;
   $this->add_element('success','modal_window') ;

   //if ($result['type']!='error') $this->add_element('after_close_update_page',1) ;
 }

}
