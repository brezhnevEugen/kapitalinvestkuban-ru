<?
  // шаблон - вывод ссылками без разделитея
  function list_items_img_a_name(&$list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
    $clone=($options['clone'])? $options['clone']:'source' ;
    $i=0;
    ?>
    <div class="list_items_img_a_name" <?echo $id?>><?
      if (sizeof($list_recs))
      { foreach($list_recs as $rec)
        { $i++;
          $class='';
          if ($i==1) $class='first';
          if ($i==sizeof($list_recs)) $class='last';?>
          <div class="item <? echo $class;?>">
            <a href="<? echo $rec['__href']?>">
              <div class="image"><img class=logo src="<? echo img_clone($rec,'source')?>"></div>
              <h4><? echo $rec['__name']?></h4>
              <div class="title"><?echo $rec['title']?></div>
            </a>
          </div><?
        }
      }?>
      <div class=clear></div>
    </div><?
  }

?>