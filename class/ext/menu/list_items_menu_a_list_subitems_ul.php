<?
  //шаблон - список подразделов для верхнего меню - разделы и всплывающие подразделы
  function list_items_menu_a_list_subitems_ul(&$list_recs,$options = array())
  {
    global $menu_system;
    $id = ($options['id'])? 'id=' . $options['id']:'';?>
    <ul class = "list_items_menu_a_list_subitems_ul" <? echo $id; ?> ><?
      if (sizeof($list_recs))
      {
        $i = 0;
        $j = sizeof($list_recs)-1;
        foreach($list_recs as $rec)
        {
          $class = array();
          $class[] = 'item';
          $class[] = ($rec['__href']==_CUR_PAGE_DIR . _CUR_PAGE_NAME)? 'selected':'';
          if ($options['item_class']) $class[] = $options['item_class'];
          if (!$i) $class[] = 'first ';
          if ($i==$j) $class[] = 'last ';
          $_class = (sizeof($class))? 'class="' . implode(' ',$class) . '"':'';?>
          <li <?echo $_class;?> id=sect_<?echo  $rec['pkey']?>>
            <a class=top href="<?echo $rec['href']?>"> <?echo $rec['obj_name']?></a><?
            if (sizeof($rec['list_obj']))
            {
              $ii = 0;
              $jj = sizeof($rec['list_obj'])-1;?>
              <ul class = "list_subitems hidden"><?
                foreach($rec['list_obj'] as $child_id)
                {
                  $class_subsection   = array();
                  $class_subsection[] = 'subitem';
                  if ($options['item_class']) $class_subsection[] = $options['item_class'];
                  if (!$ii) $class_subsection[] = 'first';
                  if ($ii==$jj) $class_subsection[] = 'last ';
                  $_class_subsection = (sizeof($class_subsection))? 'class="' . implode(' ',$class_subsection) . '"':'';
                  echo '<li ' . $_class_subsection . '><a class=isTextShadow href="' . $menu_system->tree[$child_id]->href . '">' . $menu_system->tree[$child_id]->name . '</a></li>';
                  $ii++;
                }?>
              </ul><?
            }?>
          </li><?
          $i++;
        }
      }?>
    </ul>


    <script type = "text/javascript">
        $j(document).ready(function(){
          $j('#ddmenu a').on('click', function(e){
            e.preventDefault();
          });

          $j('#ddmenu li').hover(function () {
             clearTimeout($j.data(this,'timer'));
             $j('ul',this).stop(true,true).slideDown(200);
          }, function () {
            $j.data(this,'timer', setTimeout($j.proxy(function() {
              $j('ul',this).stop(true,true).slideUp(200);
            }, this), 100));
          });

        });
    </script>

      <script type="text/javascript">
            $j('div#top_menu div.item').hover(
             function(){$j(this).children('div.list_subitems_1').removeClass('hidden') ;},
             function(){$j(this).children('div.list_subitems_1').addClass('hidden') ;}
            );
            $j('div.list_subitems_1 div.subitem').hover(
             function(){$j(this).children('a').children('div').addClass('selected') ;},
             function(){$j(this).children('a').children('div').removeClass('selected') ;}
            );
            $j('div.list_subitems_1 div.subitem').hover(
             function(){$j(this).children('div.list_subitems_next').removeClass('hidden') ;},
             function(){$j(this).children('div.list_subitems_next').addClass('hidden') ;}
            );

         </script>

  <?
  }

?>