<?php
$options=array() ;
include_once($_SERVER['DOCUMENT_ROOT']."/ini/patch.php")  ;
include_once(_DIR_TO_ENGINE."/i_system.php") ;
include_once(_DIR_TO_ENGINE."/c_HELP.php") ;
class c_page_man extends c_HELP
{
  public $h1='Обратный звонок' ;
  public $title='Обратный звонок' ;
  public $path=array('/'=>'Главная') ;

  function block_main()
  {	$this->page_title() ;
    ?><h2>Запрос обратного звонка через модальное окно</h2>
      <ul><li>В контексте страницы создаем <strong>button</strong> или <strong>div</strong>, атрибутами которого будет параметры для открытия модального окна:<br><br>
              <div>
                   <button class=v2 cmd=get_backcall_panel ext=request_phone_callback>Заказать обратный звонок</button>
                   <div class="v2 demo1"  cmd=get_backcall_panel2 ext=request_phone_callback>Заказать обратный звонок</div>
              </div>
              <textarea class=source_code></textarea>
          </li>
          <li>В файле ajax.php имеется функция <strong>get_backcall_panel</strong>. Она задает параметры для вывода модального окна, подключает скрипт с панелью запроса и выводим саму панель запроса.</li>
          <li>То, как будет отработана функия обратного звонка, зависит от параметра <strong>cmd</strong> кнопки отправки формы</li>
          <li>Значение cmd=<strong>request_phone_to_mail</strong> отправит результат запроса на email</li>
          <li>Значение cmd=<strong>request_phone_register</strong> зарегистрирует запрос в <strong>request_system</strong></li>
          <li>Для работы расширения необходим шаблон письма <strong>request_phone_sending</strong> и настройка сайта <strong>LS_request_phone_sending_email_alert</strong></li>

      </ul>
      <style type="text/css">
        div.demo1{border:1px solid green;background:#1392e9;color:white;padding:5px;cursor:pointer;text-align:center;width:200px;margin:15px 0;}
      </style>
    <?
  }


}

$options['class_file']='this' ;
show_page($options) ;
?>