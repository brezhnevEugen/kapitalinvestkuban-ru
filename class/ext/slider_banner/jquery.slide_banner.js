(function($j){
$j.fn.slide_banner = function(options)
{  var banner=this ;
   var banner_status=0 ; // 0 - ����������, 1 - �������

   // ��������� �������� ���������� ��������
   $j(this).append('<div class=status></div>') ;
   $j(this).children('div.item').each(function(){$j(banner).children('div.status').append('<div class=item></div>')}) ;

   // ������������� �������� ��� ������
   //$j(this).children('div.item').hide() ;

   // ���������� �������
   var status_bar=$j(banner).children('div.status') ;
   var cnt_item=$j(this).children('div.item').length ;
   var settings = $j.extend(
       {  'interval' : 3000,
          'speed'    : 500
       }, options);

   // ������ �������� ������ ����� � ������������ ��� ������
   $j(this).children('div.item:first-child').addClass('sel') ;
   $j(status_bar).children('div.item:nth-child(1)').addClass('sel') ;

   // ���������� ��������� �����
  function next(i,set_pos)
   { var cur_pos=$j(banner).children('div.item.sel').index()+1 ;

     // �������� ������� �����
     $j(banner).children('div.item.sel').animate({opacity:0},settings['speed'],'linear',function(){$j(this).removeClass('sel');}) ;

     // �������� ������� ���������� ������
     var next_pos=(cur_pos<cnt_item)?  cur_pos+1:1 ;
     if (set_pos!=undefined) next_pos=set_pos ;

     // ���������� ��e������ �����
     $j(banner).children('div.item:nth-child('+next_pos+')').css('display','block').animate({opacity:1},settings['speed'],'linear',function(){$j(this).addClass('sel');}) ;

     // ��������� ���� �� �������� ������
     $j(status_bar).children('div.item:nth-child('+cur_pos+')').removeClass('sel') ;
     $j(status_bar).children('div.item:nth-child('+next_pos+')').addClass('sel') ;
   }

  function start() { $j(banner).everyTime(settings['interval'],'ban_timer',next); banner_status=1 ; }
  function stop() { $j(banner).stopTime('ban_timer'); banner_status=0 ; }

  function status_bar_item_on_click()
  { // ������������� ��������� �������

    // �������� ������� ���������� ������ � �������� ������
    var set_pos=$j(this).index()+1 ;
    var cur_pos=$j(banner).children('div.item.sel').index()+1 ;
    if (set_pos!=cur_pos)
     { if (banner_status) stop() ; // ���� ����� ������� - �������������
       // ���������� ������ �����
       next(0,set_pos) ;
     }
    else if (!banner_status) start() ; // ����� ���� ����� ���������� - ������
  }

  $j(status_bar).children('div.item').click(status_bar_item_on_click) ;

  start() ;
}  ;
})(jQuery) ;