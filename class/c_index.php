<?php
include('c_page.php') ;
class c_page_index extends c_page
{ var $body_class='landing' ;

	function set_head_tags()
	  {
	    parent::set_head_tags() ;
	    //Jssor.Slider.FullPack
	  $this->HEAD['js'][]=_PATH_TO_EXT.'/Jssor.Slider.FullPack/js/jssor.core.js' ;
	  $this->HEAD['js'][]=_PATH_TO_EXT.'/Jssor.Slider.FullPack/js/jssor.utils.js' ;
	  $this->HEAD['js'][]=_PATH_TO_EXT.'/Jssor.Slider.FullPack/js/jssor.slider.js' ;
	  $this->HEAD['js'][]=_PATH_TO_EXT.'/Jssor.Slider.FullPack/panel_banner_rotator_2.js' ;
	  $this->HEAD['css'][]=_PATH_TO_EXT.'/Jssor.Slider.FullPack/panel_banner_rotator_2.css' ;

	  }

  function body()
  { global $landing_system ;
    $recs=execSQL('select * from obj_site_landing where enabled=1 and parent>0') ;
    if (sizeof($recs)) foreach($recs as $i=>$rec)
    {  $section_code=$landing_system->tree[$rec['parent']]->rec['code'] ;
       if ($rec['clss']==7) $data[$section_code][$rec['code']]=$rec['value'] ;
       if ($rec['clss']==1) $data[$section_code][$rec['code']]=$rec['pkey'] ;
    }
    ?><body class="<?echo $this->body_class?>"><?
       if (sizeof($landing_system->tree['root']->list_obj)) foreach($landing_system->tree['root']->list_obj as $section_id)
       { $template_code=$landing_system->tree[$section_id]->rec['code'] ;
         if (method_exists($this,'block_'.$template_code)) $this->{'block_'.$template_code}($data[$template_code]) ;
       }
    ?>
		<link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
		      <script type="text/javascript" src="https://cdn.callbackkiller.com/widget/cbk.js?cbk_code=20256" charset="UTF-8" async></script>
		</body><?
     ;
  }



  function block_01($data)
  {  global $landing_system ;?>
    <div id="block_01" class="page_section how-we-make" >
      <div class="inner contain cont-we-make"  >
<!--        <div class="text-we-make">-->
<!--          <h2>--><?//echo $data['h2']?><!--</h2>-->
<!--          <h3>--><?//echo $data['h3']?><!--</h3>-->
<!--          --><?//$landing_system->show_list_section($data['table'],'create_table') ;?>
<!--          <hr/>-->
<!--          <div class="star">--><?//echo $data['star']?><!--</div>-->
<!--        </div> -->

				<?
	
				global $ban_system ;?>
				     <div id=section_banner>
				       <div class="inner"><?
				         $ban_system->show_list_items(81,'Jssor.Slider.FullPack/panel_banner_rotator_2') ;?>
				       </div>
				     </div>

      </div>

    </div><?
  }

  function block_02($data)
    {  global $landing_system ;?>
      <a name="about"  class="anchor"></a>
      <div id="block_02" class="page_section">
        <h2><?echo $data['h2']?></h2>

        <div class="inner contain">
          <div id="panel_11_01" class="h3 bold"><?echo $data['text']?></div>
  <!--        --><?//damp_array($landing_system->tree[12]);?>
  <!--        --><?//$landing_system->show_list_items($data['list'],'list_art_img',array()) ;?>
          <div class="image certificates"><?$landing_system->show_list_items($data['list'],'list_images_highslide_ul',array('clone'=>201)) ;?>
          <div class="clear"></div> </div>

        </div>
      </div>
      <?
    }

  function block_03_form($data)
  { ?>
    <div id="block_03_form" class="page_section">
      <div class="inner">
        <div class="form">
          <div class="fform-inline">
            <p><?echo $data['name']?></p>
            <form class="s_form">
              <input class="s_text nameinp sinp copy-form" type="text" name="name" placeholder="Имя">
              <input class="s_text nameinp sinp" type="text" name="phone" data-required placeholder="Напишите ваш телефон" mask="+7 (999) 999-99-99">
              <input class="s_text adress" type="text" name="email" data-validate="email" placeholder="Напишите ваш e-mail">
              <input type="submit" class="v2 s_submit" value="ОСТАВИТЬ ЗАЯВКУ" cmd="request/request_service" validate="form" theme="Заявка на консультацию" validate="form">
            </form>
          </div>
        </div>
      </div>
    </div><?
  }

  function block_04($data)
  { global $landing_system ;?>
    <div id="block_04" class="page_section" >
      <div class="inner contain priminenie">
        <h2><?echo $data['h2']?></h2>
      </div>
      <div class="inner  contain yes-no">

        <div class="yes">
          <p class="title2 blue"><?echo $data['h3_2']?></p>
          <?$landing_system->show_list_items($data['list_YES'],'list_item_ul',array('debug'=>0)) ; ?>
        </div>
				<div class="no">
				          <p class="title2"><?echo $data['h3_1']?></p>
				          <?$landing_system->show_list_items($data['list_NO'],'list_item_ul',array('debug'=>0)) ; ?>
				        </div>
      </div>
    </div><?
  }

  function block_05($data)
  { global $landing_system ;?>
    <div id="block_05" class="page_section" >
      <div class="inner contain">
        <h2 class="my-title"><?echo $data['h2']?></h2>
        <div class="table"><?echo $data['table']?></div>
      </div>
    </div>
     <?
  }

  function block_06($data)
  {   global $landing_system ;?>
      <div id="block_06" class="page_section" >
        <div class="inner contain">
          <h2 class="my-title"><?echo $data['h2']?></h2>
          <div class="table"><?echo $data['table']?></div>
        </div>
      </div>
       <?
  }

  function block_07($data)
  {
  }

  function block_08($data)
  {

  }

  function block_09($data)
  {  global $landing_system ;?>
    <div id="block_09" class="page_section">
      <div class="inner contain">
        <h2  class="my-title"><?echo $data['h2']?></h2>
         <?$landing_system->show_list_items($data['list'],'list_item_img_value_plan',array('id'=>'panel_09_01')) ;?>

         <div class="clear"></div>
      </div>
    </div>
    <?
  }

  function block_10($data)
  {  global $landing_system ;?>
    <div id="block_10" class="page_section">
      <div class="inner contain">
        <h2  class="my-title"><?echo $data['h2']?></h2>
<!--         --><?//$landing_system->show_list_items($data['list'],'list_item_img_value_plan',array('id'=>'panel_09_01')) ;?>

         <div class="clear"></div>
      </div>
    </div>
    <?
  }

  function block_11($data)
  {  global $galerey_system ;?>
    <div id="block_11" class="page_section">
      <div class="inner contain">
        <h2  class="my-title"><?echo $data['h2']?></h2>
        <div id="panel_11_01" class="h3 bold"><?echo $data['text']?></div>
        <div id="project_content"><?$galerey_system->show_list_items('enabled=1','galerey/list_images',array('id'=>'galerey_carusel','clone'=>190,'count'=>12));?> </div>
        <script type="text/javascript">$j('div#project_content div#galerey_carusel').carusel_item({'speed' : '500','step' : '197','interval' : 2500,'autostart' : 1}) ;</script>
        <div class="clear"></div>
<!--        <div id="panel_11_02">-->
<!--          <div class="panel_request">-->
<!--            <form>-->
<!--              <div id="ind_email">ВАШ E-MAIL</div>-->
<!--              <input type="text" id="email" name="email" data-validate="email" placeholder="Напишите ваш e-mail">-->
<!--              <div id="ind_phone">ВАШ КОНТАКТНЫЙ ТЕЛЕФОН</div>-->
<!--              <input type="text" id="phone" name="phone" data-required placeholder="Напишите ваш телефон" mask="+7 (999) 999-99-99">-->
<!--              <input type="submit" class="v2 button_green" value="ОСТАВИТЬ ЗАЯВКУ" cmd="request/request_service" validate="form" theme="Запрос каталога" validate="form">-->
<!--            </form>-->
<!--          </div>-->
<!--        </div>-->
        <div class="clear"></div>
      </div>
    </div><?
  }

  function block_13($data)
  {  global $landing_system ;?>
    <div id="block_13" class="page_section">
      <div class="inner contain" id="text">
        <h2  class="my-title"><?echo $data['h2']?></h2>
        <div  class="text"><?echo $data['text_before']?></div>
				<hr>
				<div id="calculator">
					<div class="parameter" data-msg="- укажите метраж дома">
						<h4>Укажите общую площадь дома:<input type="text" id="calk_meter" class="onlynumber" value="" placeholder="0" /> м<sup>2</sup><small>(Минимальная площадь - 80 м<sup>2</sup>)</small></h4>
					</div>
					<?$landing_system->show_list_section($data['data_calc'],'list_section_calc',array('debug'=>0)) ; ?>
					<div id="result">
						<h3>Ориентировочная стоимость строительства дома: <strong id="total">0</strong> руб.</h3>
						<h3>Стоимость за квадратный метр: <strong id="total_metr">0</strong> руб.</h3>
						<div id="msg">
							<p>Для того, чтобы рассчитать стоимость, вам осталось указать следующие данные:</p>
							<div></div>
						</div>
						<div id="totalinfo"></div>
					</div>
				</div>
				<hr>
        <div  class="text"><?echo $data['text_after']?></div>
         <div class="clear"></div>
      </div>
    </div>
    <?
  }


  function block_12($data)
  { ?>
    <a name="contact"  class="anchor"></a>
    <div id="block_12" class="page_section">
			<div id="map_canvas"></div>
			<div class="inner">
				<div id="panel_12_01">
					<h2><?echo $data['h2']?></h2>
					<div><?echo $data['contact']?></div>
				</div>
      </div>
    </div>
      <style type="text/css"></style>
      <script type="text/javascript">
          $j(document).ready(function()
          {   var myLatlng = new google.maps.LatLng(<?echo $data['map_center']?>);
              var myOptions = {
                  zoom: 16,
                  center: myLatlng,
                  panControl: false,
                  zoomControl: false,
                  scaleControl: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              var map = new google.maps.Map($j("#map_canvas")[0], myOptions);
              var image = 'beachflag.png';
              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  title:"<?echo _MAIN_DOMAIN?>"
              });

          }) ;
      </script>
      <?
  }


}
?>
