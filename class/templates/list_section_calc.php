<?
  // шаблон - список подразделов с наименованиями и кратким перечнем последних новостей каждого подраздела
  function list_section_calc(&$list_recs,$options=array())
  { global $landing_system ;
    $id=($options['id'])? 'id='.$options['id']:'' ;
    if (sizeof($list_recs)) foreach($list_recs as $rec)
    { ?>
      <div class="parameter list_section_calc" data-msg="- <?echo $rec['name']?>">
   			<h4><?echo $rec['name']?>:</h4>
   			<ul>
          <? $landing_system->show_list_items($rec['pkey'],'list_item_ul_calc',array('debug'=>0));?>
   			</ul>
   		</div><?
    }
  }

?>