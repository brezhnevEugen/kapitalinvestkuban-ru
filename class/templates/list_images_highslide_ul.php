<?
  // шаблон - список статей: наименование-ссылка
  function list_images_highslide_ul(&$list_recs,$options=array())
  { $id=($options['id'])? 'id='.$options['id']:'' ;
    $clone=($options['clone'])? $options['clone']:'source' ;
    $slideshowGroup=($options['slideshowGroup'])? 'slideshowGroup:'.$options['slideshowGroup']:'888888' ;

//      damp_array($list_recs,1,-1);
    ?>
    <div class=galerey_wrap>
        <ul class="highslide-gallery" <?echo $id?>><?
  	    if (sizeof($list_recs)) foreach($list_recs as $i=>$rec)
  		{ ?>
        <?
          $class=array() ;
  		  if (!$i) $class[]='first' ;
  		  if ($i==sizeof($list_recs)) $class[]='last' ;
  		  if (sizeof($class)) $class='class="'.implode(' ',$class).'"' ; else $class="" ;
  		  if (!$options['no_float_big_img']) {?><li><a href="<?echo img_clone($rec,'source')?>" class="highslide" title="<?echo $rec['manual']?>" onclick="return hs.expand(this,'<?echo $slideshowGroup;?>')"><img <?echo $class?> src="<?echo img_clone($rec,$clone)?>" alt="Highslide" border="0" title="Кликните мышкой для увеличения"></a></li><?}
  		  else {?><li><img <?echo $class?> src="<?echo img_clone($rec,$clone)?>" class="highslide" border="0"></li><?}
  		}?>
        <div class="clear"></div>
        </ul>
    </div><?
  }
?>