<?php

function init_local_setting()
{   // настройки, определяющее поведение и вид страниц сайта
    $_SESSION['check_code_img'][0]=array('digit_count'=>4,'width'=>118,'height'=>30,'back'=>'F1F1F1','color'=>'272727','font_name'=>'/images/check_font.ttf','font_size'=>'18','x'=>10,'y'=>25,'step_x'=>30,'dev_x'=>0,'dev_y'=>5,'dev_angle'=>35) ;
    $_SESSION['init_options']['request']['use_capcha_code']=0 ;


}

function init_admin_setting()
{
//    unset($_SESSION['menu_admin']['Контент']) ;

//  _DOT('obj_site_landing')->rules[] = array('to_pkey' => 200,'only_clss' => 711);  //в исходные данные можно добавлять только "Элемент расчета"

  // страница сайта
 	$_SESSION['descr_clss'][26]['name']='Страница сайта' ;
 	$_SESSION['descr_clss'][26]['parent']=0 ;
 	$_SESSION['descr_clss'][26]['fields']=array('value'=>'longtext','href'=>'varchar(255)','obj_reffer'=>'any_object','type'=>'int(11)','url_name'=>'varchar(255)','template'=>'int(11)','on_use_menu'=>'int(1)') ;
 	$_SESSION['descr_clss'][26]['parent_to']=array(70,71,3) ;
  $_SESSION['descr_clss'][26]['SEO_tab']=1 ;

  $_SESSION['descr_clss'][26]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

 	$_SESSION['descr_clss'][26]['list']['field']['pkey']				='id' ;
 	$_SESSION['descr_clss'][26]['list']['field']['enabled']				='Сост.' ;
 	$_SESSION['descr_clss'][26]['list']['field']['obj_name']			=array('title'=>'Наименование','td_class'=>'left','edit_element'=>'memo','class'=>'small') ;
 	$_SESSION['descr_clss'][26]['list']['field']['template']			=array('title'=>'Набор Фото','td_class'=>'center','class'=>'small') ;
 	$_SESSION['descr_clss'][26]['list']['field']['href']					=array('title'=>'Имя страницы','td_class'=>'left','edit_element'=>'memo','class'=>'small') ;
 	$_SESSION['descr_clss'][26]['list']['field'][]								=array('title'=>'Ссылка на сайт','on_view'=>'clss_26_url_view','td_class'=>'left') ;
 	$_SESSION['descr_clss'][26]['list']['field']['on_use_menu']		=array('title'=>'Показать в навигации') ;

  $_SESSION['descr_clss'][26]['details']['field']=$_SESSION['descr_clss'][26]['list']['field'] ;
  $_SESSION['descr_clss'][26]['details']['field']['value']=array('title'=>'Текст','class'=>'big','use_HTML_editor'=>1) ;


      // 711 - информация
  $_SESSION['descr_clss'][711]=array() ;
	$_SESSION['descr_clss'][711]['name']='Расчетный элемент калькулятора' ;
	$_SESSION['descr_clss'][711]['parent']=0 ;
	$_SESSION['descr_clss'][711]['fields']=array('value'=>'text','code'=>'varchar(32)','is_HTML'=>'int(1)','price'=>'int(11)') ;
	$_SESSION['descr_clss'][711]['parent_to']=array(3) ;

  $_SESSION['descr_clss'][711]['icons']=_PATH_TO_BASED_CLSS_IMG.'/7.png' ;

	$_SESSION['descr_clss'][711]['list']['field']['enabled']					='Сост.' ;
	$_SESSION['descr_clss'][711]['list']['field']['pkey']						='ID' ;
	$_SESSION['descr_clss'][711]['list']['field']['code']						='Код блока<br>в шаблоне' ;
	$_SESSION['descr_clss'][711]['list']['field']['obj_name']					='Наименование' ;
	$_SESSION['descr_clss'][711]['list']['field']['price']					='цена' ;
	$_SESSION['descr_clss'][711]['list']['field']['is_HTML']	                =array('title'=>'HTML')  ;
    $_SESSION['descr_clss'][711]['list']['field']['value']					=array('title'=>'Содержание','td_class'=>'justify','class'=>'big','use_HTML_editor'=>1) ;

	$_SESSION['descr_clss'][711]['details']=$_SESSION['descr_clss'][711]['list'] ;
	$_SESSION['descr_clss'][711]['details']['field']['value']					=array('title'=>'Текст','td_class'=>'justify','class'=>'big','no_generate_element'=>1) ;
    $_SESSION['descr_clss'][711]['details']['field'][]					    =array('title'=>'Код php','edit_element'=>'clss_7_code','td_class'=>'justify') ;



  $_SESSION['img_pattern']['Статьи']=array() ;
    $_SESSION['img_pattern']['Статьи'][100]['resize']=array('width'=>100,'height'=>100,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');
    $_SESSION['img_pattern']['Статьи'][200]['resize']=array('width'=>200,'height'=>150,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');
    $_SESSION['img_pattern']['Статьи'][201]['resize']=array('width'=>200,'height'=>300,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');
    $_SESSION['img_pattern']['Статьи'][270]['resize']=array('width'=>270,'height'=>381,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');
    $_SESSION['img_pattern']['Статьи'][300]['resize']=array('width'=>300,'height'=>185,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');
    $_SESSION['img_pattern']['Статьи'][724]['resize']=array('width'=>724,'height'=>480,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');

  $_SESSION['img_pattern']['Галерея']=array() ;
    $_SESSION['img_pattern']['Галерея'][190]['resize']=array('width'=>190,'height'=>142,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');
    $_SESSION['img_pattern']['Галерея'][800]['resize']=array('width'=>800,'height'=>600,'fields'=>0,'resampled'=>1,'quality'=>100,'back_color'=>'FFFFFF');

}
?>